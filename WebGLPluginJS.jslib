mergeInto(LibraryManager.library, {

   RequestBlueprints: function () {
      window.dispatchReactUnityEvent("Blueprints");
   },
   RequestUserItems: function () {
      window.dispatchReactUnityEvent("UserItems");
   },
   RequestSpaceMeta: function () {
      window.dispatchReactUnityEvent("SpaceMeta");
   },
   RequestBlueprintByID: function (blueprintId) {
      window.dispatchReactUnityEvent("BlueprintByID", blueprintId);
   },
   RequestInterpretationsByBlueprintID: function (blueprintId) {
      window.dispatchReactUnityEvent("InterpretationsByBlueprintID", blueprintId);
   },
   RequestInterpretationsByItemID: function (blueprintId, itemId) {
      window.dispatchReactUnityEvent("InterpretationsByItemID", blueprintId, itemId);
   },
   RequestSpaceClose: function () {
      window.dispatchReactUnityEvent("SpaceClose");
   },
   RequestOpenMarketPlace: function () {
      window.dispatchReactUnityEvent("OpenMarketPlace");
   },
   RequestUserInfo: function(){
      window.dispatchReactUnityEvent("UserInfo");
   },
   RequestMintItem: function(blueprintId,metadata,description) {
      window.dispatchReactUnityEvent("RequestMintItem", Pointer_stringify(blueprintId) , Pointer_stringify(metadata), Pointer_stringify(description) );
      console.log("BlueprintID = "+Pointer_stringify(blueprintId));
      console.log("Name = "+Pointer_stringify(metadata));
      console.log("Description = "+Pointer_stringify(description));
   },
   OnControllerLoaded: function () {
      window.dispatchReactUnityEvent("ControllerLoaded");
   },
   OnControllerUnloaded: function () {
      window.dispatchReactUnityEvent("ControllerUnloaded");
   },
});
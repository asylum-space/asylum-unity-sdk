# Asylum Unity SDK

> Unity version supported: **2021.3.1f1**

Provides plugins, editor scripts, and a set of utilities to integrate with the Asylum ecosystem.

The plugin is designed to work in **Unity Play Mode**, **WebGL build**, and **Windows, Mac, Linux Standalone builds**. Check [Build & Run inside Unity project](#build-run-inside-unity-project) section.

## Quick Start

For the Quick Start, you can download [Asylum Unity SDK example](https://gitlab.com/asylum-space/asylum-unity-sdk-example) and run it in [Play Mode](Docs/PlayMode.md)


## Plugin inwards

The plugin is designed to work within WebGL build and in Standalone mode (in Play mode or Desktop builds). For Standalone mode, it utilizes [Ajuna.NetApi](https://github.com/ajuna-network/Ajuna.NetApi) as a Substrate Client, whereas for WebGL build, it uses `jslib` approach. 

Both methods implements the same [IAsylumDigitalObjectsClient](AsylumSDK/IAsylumDigitalObjectsClient.cs) API. Check the UML classes diagram below:

![](Docs/img/UML.png)

- `jslib` - responsible for the connection between Unity and JS code, which runs the build
- `AsylumDigitalObjectsController.cs` - the script that defines the environment in which the Unity build is running, whether it's WebGL or Standalone. And defines the mechanism of work, either using ReactClient or StandaloneClient
- `AsylumEntities.cs` - contains types and entities you need to initialize Asylum NFT Items
- `ReactClient.cs` - implements [IAsylumDigitalObjectsClient](AsylumSDK/IAsylumDigitalObjectsClient.cs) and responsible for connection between Unity life circle and `jslib`(WebGL), parsing and downloading on-chain data or send requests
- `StandaloneClient.cs` - implements [IAsylumDigitalObjectsClient](AsylumSDK/IAsylumDigitalObjectsClient.cs) and responsible for standalone connection between Unity life circle and chain, parsing and downloading on-chain data or send requests
- `AsylumPlugin.cs` - responsible for the standalone connection between Unity Editor and chain, parsing and downloading on-chain data, or send requests
- `AsylumItem.cs` - contains a low-level object that stores various item parameters and references to its metadata, which is not loaded. The serialized class is created for JSON parsing. 
- `AsylumItemAsyncHandler.cs` - describes an item handler that works with [AsylumItem](AsylumSDK/AsylumEntities.cs) and turns it into a high-level object, loading all the necessary metadata and turning it from bytes to complex objects.
- `AsylumInterpretation.cs` - contains an id and a list of tags corresponding to the interpretation, and [InterpretationInfo](AsylumSDK/AsylumEntities.cs). The serialized class is created for JSON parsing.
- `InterpretationInfo.cs` - contains links to the metadata and source interpretation for further download. The serialized class is created for JSON parsing.


## Build & Run inside Unity Project

 - Quick installation with [Asylum Unity SDK Example](https://gitlab.com/asylum-space/asylum-unity-sdk-example)

 - Manual installation in existing Unity project:

    1. Create or open a Unity project (supports version **2021.3.1f1**)

    2. Import [Newtonsoft Json Unity Package](https://docs.unity3d.com/Packages/com.unity.nuget.newtonsoft-json@2.0/manual/index.html) using [AssetPackagesImport](https://docs.unity3d.com/Manual/AssetPackagesImport.html). This is the dependency for Asylum Unity SDK.

    3. Import [UniTask](https://github.com/Cysharp/UniTask.git?path=src/UniTask/Assets/Plugins/UniTask)

    4. Import [GLFTUtility](https://github.com/Siccity/GLTFUtility).
        > Note: You need to follow [these steps](https://github.com/Siccity/GLTFUtility/pull/203) to use GLTFUtility in the WebGL build 

    5. Put Asylum Unity SDK inside Unity `Assets/Plugins` folder

    6. You can test the plugin in [Play Mode](Docs/PlayMode.md)

    7. When you're ready to build, select one of the options: 

        - [Run Unity WebGL build inside Creator Studio](Docs/WEBGL.md)
        
        - [Run Unity Standalone build for Windows, Mac, Linux](Docs/StandaloneWindowsBuild.md)

## How to work with the Asylum Unity SDK Plugin

#### Using a plugin Editor Extension to interact with blockchain directly from Unity

There is a specially developed component called [AsylumPlugin.cs.](AsylumSDK/AsylumPlugin.cs) 
It is designed to interact with the blockchain without leaving Unity to simplify your development.

With the visual plugin that you can use in the Unity inspector, you can query all available blueprints or tags and see all information about them.
You can mint an item from any blueprint you choose.
It is also possible to query Items of a certain user with the help of a secret phrase, which you will need to specify in the component field.

[Here you can see a detailed tutorial](Docs/AsylumPluginEditor.md) on how to use [AsylumPlugin.cs.](AsylumSDK/AsylumPlugin.cs) 


#### API Usage

- Create empty `GameObject` named `DigitalObjectController` in the scene, set `GameObject` tag to `DigitalObjectController` and add `AsylumDigitalObjectsController.cs` as its component

- Create new C# script(e.g. `ItemsController`) and link `AsylumDigitalObjectsController` component to it. You can do it via inspector, using public/serializable fields or [FindObjectOfType method](https://docs.unity3d.com/ScriptReference/Object.FindObjectOfType.html) or with the help of dependency injection (like Zenject)

- Remember to set the user's wallet passphrase before start working with [AsylumDigitalObjectsController](AsylumSDK/AsylumDigitalObjectsController.cs). Simply initialize the property `AccountSecretPhrase` with a string with the secret phrase. You can set it from inspector in [AsylumDigitalObjectsController](AsylumSDK/AsylumDigitalObjectsController.cs) component. 

    ```cs
        //ItemsController.cs

        void Start
        {
            AsylumDigitalObjectsController asylumClient = FindObjectOfType<AsylumDigitalObjectsController>();
        }
    ```
- Get user items list in the `ItemsController`. Simply subscribe on the event `OnUserItemsRecieved` and call `RequestAsylumUserItems`
    ```cs
        // ItemsController.cs

        AsylumDigitalObjectsController asylumClient;

        void Start()
        {
            //Connect to the chain.
            asylumClient.ConnectToChain();

            // Subsribe on event
            asylumClient.OnItemWasLoaded += OnItemLoaded;

            //Send request
            asylumClient.RequestAsylumUserItems();

            // Manual call
            List<AsylumItemAsyncHandler> items = asylumClient.ItemHandlers;

        }
        
        void OnItemLoaded(List<AsylumItemAsyncHandler> items)
        {
            ...
        }
    ```
- Get user info in the `ItemsController`. Simply subscribe on the event `OnUserInfoRecieved` and calling `RequestAsylumUserInfo`
    ```cs
        //ItemsController.cs

        AsylumDigitalObjectsController asylumClient;

        void Start()
        {    
            //Connect to chain.
            asylumClient.OnConnected();

            // Subsribe on event
            asylumClient.OnUserInfoRecieved += UserInfoRecieved;

            //Send request
            asylumClient.RequestAsylumUserInfo();

            // Manual call
            UserInfo userInfo = asylumClient.UserInfo;

        }

        void UserInfoRecieved(UserInfo info)
        {
            ...
        }
    ```
- Get user info in the `ItemsController`. Simply subscribe on the event `OnItemWasLoaded` and calling `RequestMintAsylumItem`
    ```cs
        //ItemsController.cs

        AsylumDigitalObjectsController asylumClient;

        void Start()
        {    
            //Connect to chain.
            asylumClient.OnConnected();

            // Subsribe on event
            asylumClient.OnItemWasLoaded += MintedItemsRecieved;

            // Set blueprintId for minted item
            int blueprintId = 0;

            // Set metadata for the minted item
            AsylumItemMetadata metadata = new AsylumItemMetadata("description of minted item","name of minted item");

            //Send a request with 2 params
            reactClientInstance.RequestMintAsylumItem(blueprintId,metadata);
        }

        void MintedItemsRecieved(List<AsylumItemAsyncHandler> mintedItems)
        {
            ...
        }
    ```

#### AsylumDigitalObjectsController API

The controller works using the [IAsylumDigitalObjectsClient](AsylumSDK/IAsylumDigitalObjectsClient.cs) interface. 
The interface is implemented in StandaloneClient and in ReactClient. 
When you run a build or playmode, the [AsylumDigitalObjectsController](AsylumSDK/AsylumDigitalObjectsController.cs) script independently determines which approach to use.

> Note: If You want to use WebGL build, you have also to add empty `GameObject` named `ReactController` in the scene and add `ReactClient.cs` as its component 

Actions:
- `OnItemWasLoaded` - when all user items was parsed with metadata and initialized in the [AsylumDigitalObjectsController](AsylumSDK/AsylumDigitalObjectsController.cs)
- `OnUserInfoRecieved` - when all user info was parsed and initialized in the [AsylumDigitalObjectsController](AsylumSDK/AsylumDigitalObjectsController.cs)
- `OnSpaceMetadataReceived` - when all space info was parsed and initialized in the [AsylumDigitalObjectsController](AsylumSDK/AsylumDigitalObjectsController.cs)
- `OnPauseRequestedAction` - when pause requested out of game.

Methods: 
- `RequestAsylumUserItems` - send user item request to chain. Subscribe to the event `OnItemWasLoaded` to get the items
- `RequestAsylumUserInfo` - send user info request to chain. Subscribe to the event `OnUserInfoRecieved` to get the info
- `RequestAsylumSpaceMetadata` - send space metadata request to chain. Subscribe to the event `OnSpaceMetadataRecieved` to get the metadata
- `RequestMintAsylumItem` - send minting item request to chain. Subscribe to the event `OnItemWasLoaded` to get the minted item. The method has 2 parameters. The blueprint number and the item's metadata
- `ConnectToChain` - connect to chain.
- `DisconnectFromChain` - disconnect from chain.
- `OnExitRequested` - send request to close space.
- `GetItemHandler` - outputs a specific object, depending on the request, from the ItemHandlers array.

Properties:
- `AccountSecretPhrase` - to save secret phrase from the user's wallet.
- `ItemHandlers` - a list of all downloaded items from the chain. An object of type AsylumItemAsyncHandler is a high-level object with fully loaded metadata.
- `UserInfo` - contains complete information about the user downloaded from the chain.
- `SpaceMetadata` - contains complete information about the space downloaded from the chain.

#if UNITY_EDITOR

using Asylum;
using UnityEngine;
using System.Threading.Tasks;
using Cysharp.Threading.Tasks;
using UnityEngine.Networking;
using System.Text;
using System.Collections.Generic;

using Ajuna.NetApi;
using Ajuna.NetApi.Model.Types;
using AjunaExample.NetApiExt.Generated.Model.sp_core.crypto;
using System;
using System.Threading;

/// <summary>
/// Class for editor functionality of the SDK
/// </summary>
[System.Serializable]
public class AsylumPlugin : MonoBehaviour
{
    /// <summary>
    /// Asylum tags array
    /// </summary>
    public List<AsylumAssembledTag> _asylumTags;

    /// <summary>
    /// Asylum blueprints array
    /// </summary>
    public List<AsylumAssembledBlueprint> _asylumBlueprints = null;

    //TODO: consider creation of assembled Asylum Item entities.
    /// <summary>
    /// Asylum user items array
    /// </summary>
    public List<AsylumItem> _asylumItems = null;

    /// <summary>
    /// User account from secret phrase
    /// </summary>
    public Account Account { get => _account; }
    private Account _account;

    /// <summary>
    /// Secret phrase from inspector
    /// </summary>
    public string AccountSecretKey { get => secretPhrase; set => secretPhrase = value; }
    private string secretPhrase;

    /// <summary>
    /// Client for connecting and interacting with the chain.
    /// </summary>
    private IAsylumEditorClient _standaloneEditorClient = new StandaloneClient();

    /// <summary>
    /// Path for download data from ipfs storage
    /// </summary>
    private string _mainPath = @"http://127.0.0.1:8080/ipfs/";

    /// <summary>
    /// Set account secret phrase. Get string from input in inspector.
    /// </summary>
    public void SetAccountSecretKey()
    {
        this._account = Mnemonic.GetAccountFromMnemonic(this.secretPhrase, "", KeyType.Sr25519);
        if (this._account != null)
        {
            this._standaloneEditorClient.SetSecretPhrase(this.secretPhrase);
        }
        else
        {
            Debug.LogError("In AsylumPlugin:SetAccountSecretPhrase - can`t set account. The secret phrase is wrong.");
        }
    }

    /// <summary>
    /// Get all Asylum tags from chain in this._asylumTags. After the data is received, download the tags metadata.
    /// </summary>
    public void GetAllAsylumTags()
    {
        Task.Run(() => this._asylumTags = AsylumAssembledTag.ConvertToAssembledTagsList(this._standaloneEditorClient.RequestTagsEditorAsync().Result))
            .ContinueWith((state) => AsylumAssembledTag.LoadTagsMetaAsync(this._asylumTags,this._mainPath));
    }

    /// <summary>
    /// Get all Asylum user items from chain in this._asylumItems. Uses a secret phrase entered by the user.
    /// </summary>
    public void GetAllAsylumUserItems()
    {
        Task.Run(() => this._asylumItems = new List<AsylumItem>(this._standaloneEditorClient.RequestUserItemsEditorAsync(Account.Value).Result));
    }

    /// <summary>
    /// Get all Asylum blueprints from chain in this._asylumBlueprints.
    /// </summary>
    public void GetAllAsylumBlueprints()
    {
        Task.Run(() => this._asylumBlueprints = AsylumAssembledBlueprint.ConvertToAssembledBlueprintsList(this._standaloneEditorClient.RequestUserBlueprintsEditorAsync().Result))
            .ContinueWith((state) => AsylumAssembledBlueprint.LoadBlueprintsMetaAsync(this._asylumBlueprints, this._mainPath));
        //Task.Run(() => this._asylumBlueprints = new List<AsylumBlueprint>(this._standaloneEditorClient.RequestUserBlueprintsEditorAsync().Result)).ContinueWith((state) => LoadBlueprintsMetaAsync(), GetTaskScheduler());
    }

    /// <summary>
    /// Reconnect to chain. Clear all data and set secret key.
    /// </summary>
    public void Reconnect()
    {
        Task.Run(() =>  _standaloneEditorClient.ReconnectEditorAsync(
                        () => { ClearAllData(); SetAccountSecretKey(); }
                        ));
    }

    /// <summary>
    /// Clear all data in AsylumPlugin.
    /// </summary>
    public void ClearAllData()
    {
        this._account = null;
        this._asylumBlueprints = null;
        this._asylumItems = null;
        this._asylumTags = null;
    }

    /// <summary>
    /// Clear account data. Set null.
    /// </summary>
    public void ClearAccountData()
    {
        this._account = null;
    }

    /// <summary>
    /// Send mint request to chain.
    /// </summary>
    /// <param name="id">Blueprint id for minted item.</param>
    /// <param name="itemMetadata">Metadata for minted item.</param>
    public void MintItemFromBlueprint(int id, AsylumItemMetadata itemMetadata)
    {
        this._standaloneEditorClient.OnMintedItemsRecieved += ItemMinted;
        Task.Run(() => this._standaloneEditorClient.RequestMintItemEditorAsync(this.Account,id,itemMetadata));
    }

    /// <summary>
    /// Send mint request to chain.
    /// </summary>
    /// <param name="id">Blueprint id for minted item.</param>
    /// <param name="itemMetadata">Metadata for minted item.</param>
    public void MintItemFromBlueprint(string id, AsylumItemMetadata itemMetadata)
    {
        this._standaloneEditorClient.OnMintedItemsRecieved += ItemMinted;
        Task.Run(()=>this._standaloneEditorClient.RequestMintItemEditorAsync(this.Account, Convert.ToInt32(id), itemMetadata));
    }

    private void ItemMinted(AsylumItem[] items)
    {
        this._standaloneEditorClient.OnMintedItemsRecieved -= ItemMinted;

        GetAllAsylumUserItems(); 
        GetAllAsylumBlueprints();

    }

    private TaskScheduler GetTaskScheduler()
    {
        TaskScheduler syncContextScheduler;
        if (SynchronizationContext.Current != null)
        {
            syncContextScheduler = TaskScheduler.FromCurrentSynchronizationContext();
        }
        else
        {
            // If there is no SyncContext for this thread (e.g. we are in a unit test
            // or console scenario instead of running in an app), then just use the
            // default scheduler because there is no UI thread to sync with.
            syncContextScheduler = TaskScheduler.Current;
        }

        return syncContextScheduler;
    }
}

#endif
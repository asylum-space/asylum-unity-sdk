using Asylum;
using Cysharp.Threading.Tasks;
using Newtonsoft.Json;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

/// <summary>
/// Class for async loading and handling of the <see cref="Asylum.AsylumInterpretation"/> entity
/// </summary>
public class AsylumInterpretationAsyncHandler
{
    //All tag that define this interpretation
    private Dictionary<string, TypedData> _loadedTypedData = new();

    protected string _loadingMainPath;
    protected AsylumInterpretation _asylumInterpretation;
    protected Dictionary<string, object> _metadataDictionary = new();

    protected bool _isInterpretationValidLoaded = false;

    private byte[] _loadedDataByteArray;

    public string ID => _asylumInterpretation?.id ?? "NO_ID";

    public AsylumInterpretationAsyncHandler(AsylumInterpretation asylumInterpretation, string dataPath)
    {
        _asylumInterpretation = asylumInterpretation;
        _loadingMainPath = dataPath;
    }

    public bool IsTagInterpretated(string tagString)
    {
        return _loadedTypedData.ContainsKey(tagString);
    }

    //TODO : Implement 
    /*
    public bool IsTagInterpretated(TagBase tagBaseObject)
    {
        return _tagCollection.Contains(tagBaseObject);
    }
    */


    public object GetAdapted(string tagString)
    {
        if (_isInterpretationValidLoaded)
        {
            if (_loadedTypedData.TryGetValue(tagString, out TypedData founded))
            {
                //MB return (type,object)?

                return founded.dataObject;
            }
            else
            {
                UnityEngine.Debug.LogWarning($"AsylumInterpretationAsyncHandler GetAdapted by tagString {tagString} doesn't in the dictionary! Return null!");

                return null;
            }
        }
        else
        {
            UnityEngine.Debug.LogWarning($"AsylumInterpretationAsyncHandler GetAdapted {_asylumInterpretation.id} is not fully loaded! Return null!");

            return null;
        }
    }

    public object[] GetAdaptedArray(Type type, string tagString = null)
    {
        if (_isInterpretationValidLoaded)
        {
            if (!string.IsNullOrEmpty(tagString))
            {
                if (_loadedTypedData.TryGetValue(tagString, out TypedData founded))
                {
                    if (founded != null && founded.dataType == type)
                    {
                        return new object[] { founded.dataObject };
                    }
                    else
                    {
                        UnityEngine.Debug.LogWarning($"AsylumInterpretationAsyncHandler GetAdapted {_asylumInterpretation.id} tag {tagString} has not adapted {type}! Return null!");

                        return null;
                    }
                }
                else
                {
                    UnityEngine.Debug.LogWarning($"AsylumInterpretationAsyncHandler GetAdapted Can't get {tagString} from {_asylumInterpretation.id}! Return null!");

                    return null;
                }
            }
            else
            {
                var founded = _loadedTypedData.Values.Where(typedData => typedData.dataType == type);

                if (founded != null && founded.Count() > 0)
                {
                    var selected = founded.Select(typed => typed.dataObject);

                    if (selected != null && selected.Count() > 0)
                    {
                        return selected.ToArray();
                    }
                }

                UnityEngine.Debug.LogWarning($"AsylumInterpretationAsyncHandler GetAdapted {_asylumInterpretation.id} can't find the adapted {type}! Return null!");

                return null;
            }

        }
        else
        {
            UnityEngine.Debug.LogWarning($"AsylumInterpretationAsyncHandler GetAdapted {_asylumInterpretation.id} is not fully loaded! Return null!");

            return null;
        }
    }

    //MB return Dictionary<tagString, TypedData>?
    public TypedData[] GetAllAdapted()
    {
        List<TypedData> result = new();

        foreach (var typed in _loadedTypedData)
        {
            if (typed.Value != null && typed.Value.dataObject != null)
            {
                result.Add(typed.Value);
            }
        }

        return result.ToArray();
    }

    /// <summary>
    /// Async loading of the interpretation data
    /// </summary>
    /// <returns>Result of the loading Return true only if at least one of its tag was loaded succesfully</returns>
    public async UniTask<bool> LoadInterpretationAsync()
    {
        string srcPath = $"{_loadingMainPath}{_asylumInterpretation.interpretationInfo.src}";
        string metadataPath = $"{_loadingMainPath}{_asylumInterpretation.interpretationInfo.metadata}";

        var srcLoadingTask = DataLoader.instance.GetDataAsync(srcPath);
        var metadataLoadingTask = DataLoader.instance.GetDataAsync(metadataPath);

        var loadedInterpretationData = await UniTask.WhenAll(srcLoadingTask, metadataLoadingTask);

        //TODO : MB more detail statements?
        var srcLoadResult = loadedInterpretationData.Item1;
        var metadataLoadResult = loadedInterpretationData.Item2;
        
        if (srcLoadResult != null
            && metadataLoadResult != null)
        {
            if (srcLoadResult.state && metadataLoadResult.state)
            {
                _loadedDataByteArray = srcLoadResult.data;

                string jsonInterpretationMeta = Encoding.UTF8.GetString(metadataLoadResult.data);

                var parseTaskResult = await ParseInterpretationMetadataAsync(jsonInterpretationMeta);

                if(parseTaskResult)
                {
                    var adaptedDataInitResult = await InitAdaptedData();

                    if(adaptedDataInitResult)
                    {
                        //UnityEngine.Debug.LogWarning($"Interpretation id : {_asylumInterpretation.id} is loaded valid!");

                        _isInterpretationValidLoaded = true;
                    }

                    return adaptedDataInitResult;
                }
            }
        }

        UnityEngine.Debug.LogError($"Interpretation id : {_asylumInterpretation.id} loading error => return false");

        return false;
    }

    /// <summary>
    /// Init and check data of the tag
    /// </summary>
    /// /// <returns>Result of the loading Return true only if at least one of its tag was loaded succesfully</returns>
    private async UniTask<bool> InitAdaptedData()
    {
        int addedCount = 0;

        if(_loadedTypedData == null)
        {
            _loadedTypedData = new();
        }

        foreach (var tag in _asylumInterpretation.tags)
        {
            //MB cache this?
            var tagBase = TagController.Instance.GetTagBaseByString(tag);

            if(tagBase != null)
            {
                Type tagType = tagBase.DataType;

                var tagAdaptedData = tagBase.AdaptData(_loadedDataByteArray, _metadataDictionary);

                if (tagAdaptedData != null
                    && tagAdaptedData.GetType() == tagType)
                {
                    //Our tag is loaded!

                    if (!_loadedTypedData.TryAdd(tagBase.TagString, new TypedData(tagType, tagAdaptedData)))
                    {
                        UnityEngine.Debug.LogError($"Can't add the adapted data of tagString : {tagBase.TagString} with type {tagType} in the interpretation {_asylumInterpretation.id}");
                    }
                    else
                    {
                        //Increase count only if we add tag interpretation
                        addedCount++;

                        await UniTask.Yield();
                    }
                }
                else
                {
                    UnityEngine.Debug.LogError($"Tag {tagBase.TagString} in the interpretation {_asylumInterpretation.id} doesn't adapt as {tagType} type");
                }
            }
        }

        if(addedCount > 0)
        {
            //UnityEngine.Debug.LogError($"{_asylumInterpretation.id} got {addedCount} adapted tags!");

            return true;
        }
        else
        {
            UnityEngine.Debug.LogError($"We can't adapt any data in this {_asylumInterpretation.id}");

            return false;
        }
    }

    //Is it really need to be async?
    private async UniTask<bool> ParseInterpretationMetadataAsync(string jsonString)
    {
        Dictionary<string, object> metadataDictionary = new Dictionary<string, object>();

        metadataDictionary = JsonConvert.DeserializeObject<Dictionary<string, object>>(jsonString);

        await UniTask.Yield();

        if (metadataDictionary != null && metadataDictionary.Count > 0)
        {
            if(_metadataDictionary == null)
            {
                _metadataDictionary = new();
            }

            _metadataDictionary = metadataDictionary;

            return true;
        }
        else
        {
            UnityEngine.Debug.LogWarning($"ParseInterpretationMetadataAsync ERROR : {_asylumInterpretation.id} ");
            return false;
        }
    }
    
}

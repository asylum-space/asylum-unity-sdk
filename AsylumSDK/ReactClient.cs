using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Runtime.InteropServices;

namespace Asylum
{
    /// <summary>
    /// Implementation of the <see cref="IAsylumDigitalObjectsClient"/> interface for the chain-unity connection by React
    /// </summary>
    public class ReactClient : MonoBehaviour, IAsylumDigitalObjectsClient
    {
        #region Imported JsLib methods for interactions with React

#if UNITY_WEBGL && !UNITY_EDITOR

        [DllImport("__Internal")]
        public static extern void RequestUserItems();

        [DllImport("__Internal")]
        public static extern void RequestBlueprints(); 

        [DllImport("__Internal")]
        public static extern void RequestSpaceMeta(); 

        [DllImport("__Internal")]
        public static extern void RequestBlueprintByID(int blueprintId);

        [DllImport("__Internal")]
        public static extern void RequestInterpretationsByBlueprintID(string blueprintId);

        [DllImport("__Internal")]
        public static extern void RequestInterpretationsByItemID(string blueprintId, string itemId);

        [DllImport("__Internal")]
        public static extern void RequestSpaceClose();

        [DllImport("__Internal")]
        public static extern void RequestOpenMarketPlace();

        [DllImport("__Internal")]
        public static extern void RequestMintItem(string blueprintId,string metadata,string description);
        
        [DllImport("__Internal")]
        public static extern void OnControllerLoaded();

        [DllImport("__Internal")]
        public static extern void OnControllerUnloaded();

        [DllImport("__Internal")]
        public static extern void RequestUserInfo();
#endif
        #endregion

        public Action<AsylumItem[]> OnUserItemsRecieved { get; set; }
        public Action<UserInfo> OnUserInfoRecieved { get; set; }
        public Action<SpaceMetadata> OnSpaceMetadataRecieved { get; set; }
        public Action<AsylumItem[]> OnMintedItemsRecieved { get; set; }

        public Action<AsylumBlueprint[]> OnBlueprintsReceived { get; set; }
        public Action<AsylumInterpretation[]> OnInterpretationReceived { get; set; }

        public Action OnPauseRequestedAction;

        private void Awake()
        {
            DontDestroyOnLoad(this);
        }

        public void SendUserItemsRequest()
        {
#if UNITY_WEBGL && !UNITY_EDITOR
            RequestUserItems();
#endif
        }
        
        public void SendUserInfoRequest()
        {
#if UNITY_WEBGL && !UNITY_EDITOR
            RequestUserInfo();
#endif
        }
        
        public void SendSpaceMetadataRequest()
        {
#if UNITY_WEBGL && !UNITY_EDITOR
            RequestSpaceMeta();
#endif
        }

        public void SendMintItemRequest(int blueprintId, AsylumItemMetadata metadata)
        {
#if UNITY_WEBGL && !UNITY_EDITOR
            RequestMintItem(blueprintId.ToString(),metadata.name,metadata.description);
#endif
        }
        public void OnConnected()
        {
#if UNITY_WEBGL && !UNITY_EDITOR

            OnControllerLoaded();
#endif
        }

        public void OnDisconnected()
        {
#if UNITY_WEBGL && !UNITY_EDITOR
            OnControllerUnloaded();
#endif
        }

        public void SendSpaceCloseRequest()
        {
#if UNITY_WEBGL && !UNITY_EDITOR
            RequestSpaceClose();
#endif
        }

        #region Calling from React

        //MB put whole region to the #if Unity_web statement?

        public void ParseUserInfo(string jsonString)
        {
            UserInfo userInfo = JsonUtility.FromJson<UserInfo>(jsonString);

            OnUserInfoRecieved?.Invoke(userInfo);
        }

        public void ParseItems(string jsonString)
        {
            AsylumItem[] items = JsonHelper.FromJson<AsylumItem>(jsonString);

            OnUserItemsRecieved?.Invoke(items);
        }

        public void ParseSpaceMetadata(string jsonString)
        {
            SpaceMetadata spaceMetadata = JsonUtility.FromJson<SpaceMetadata>(jsonString);

            OnSpaceMetadataRecieved?.Invoke(spaceMetadata);
        }

        public void ItemsMinted(string jsonString)
        {
            AsylumItem[] mintedItems = JsonHelper.FromJson<AsylumItem>(jsonString);

            if (mintedItems != null && mintedItems.Length > 0)
            {
                OnMintedItemsRecieved?.Invoke(mintedItems);
            }
            else
            {
                Debug.LogError("ReactClient:ItemsMinted items array is null or empty. Can`t invoke action.");
            }
        }

        public void PauseSpace()
        {
            this.OnPauseRequestedAction?.Invoke();
        }

        public void ParseBlueprints(string jsonString)
        {
            AsylumBlueprint[] blueprints = JsonHelper.FromJson<AsylumBlueprint>(jsonString);
            //TODO : Implement

            OnBlueprintsReceived?.Invoke(blueprints);
        }

        public void ParseInterpretations(string jsonString)
        {
            AsylumInterpretation[] interpretations = JsonHelper.FromJson<AsylumInterpretation>(jsonString);
            //TODO : Implement

            OnInterpretationReceived?.Invoke(interpretations);
        }

#endregion
    }
}

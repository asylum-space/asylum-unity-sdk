using System;
using System.Collections.Generic;
using UnityEngine;
using Cysharp.Threading.Tasks;
using Newtonsoft.Json.Linq;
using System.Text;
using UnityEngine.Networking;

#if UNITY_EDITOR || UNITY_STANDALONE && !UNITY_WEBGL

using Ajuna.NetApi;
using Ajuna.NetApi.Model.Meta;
using Ajuna.NetApi.Model.Types;
using Ajuna.NetApi.Model.Types.Primitive;
using AjunaExample.NetApiExt.Generated.Model.asylum_traits.types;
using AjunaExample.NetApiExt.Generated.Model.frame_support.storage.bounded_vec;
using AjunaExample.NetApiExt.Generated.Model.rmrk_traits.collection;
using AjunaExample.NetApiExt.Generated.Model.rmrk_traits.nft;
using AjunaExample.NetApiExt.Generated.Model.rmrk_traits.resource;
using AjunaExample.NetApiExt.Generated.Model.sp_core.crypto;

#endif

namespace Asylum
{
    public static class JsonHelper
    {
        public static T[] FromJson<T>(string json)
        {
            WrappedAsylumItems<T> wrapper = JsonUtility.FromJson<WrappedAsylumItems<T>>(json);
            return wrapper.Items;
        }

        public static string ToJson<T>(T[] array, bool prettyPrint = false)
        {
            WrappedAsylumItems<T> wrapper = new WrappedAsylumItems<T>();
            wrapper.Items = array;
            return JsonUtility.ToJson(wrapper, prettyPrint);
        }

        [Serializable]
        private class WrappedAsylumItems<T>
        {
            public T[] Items;
        }
    }

    public static class AsylumAssemblerUtils
    {
        /// <summary>
        /// Load metadata for one tag.
        /// </summary>
        /// <param name="metadataPath">Path for downloading.</param>
        /// <returns></returns>
        public static async UniTask<T> LoadMetaAsync<T>(string metadataPath) 
            where T : class
        {
            var result = await AsylumAssemblerUtils.LoadDataAsync(metadataPath);

            T tagMetadata = JsonUtility.FromJson<T>(Encoding.Default.GetString(result.Item2));

            if (tagMetadata == null)
            {
                return null;
            }

            return tagMetadata;
        }

        /// <summary>
        /// Load data from path async.
        /// </summary>
        /// <param name="path">Path for downloading various data</param>
        /// <returns></returns>
        public static async UniTask<(bool, byte[])> LoadDataAsync(string path)
        {
            await UniTask.SwitchToMainThread();

            UnityWebRequest request = UnityWebRequest.Get($"{path}");

            UnityWebRequest sendedRequest = await request.SendWebRequest();

            if (sendedRequest.result != UnityWebRequest.Result.Success)
            {
                UnityEngine.Debug.LogError($"Can't load from {sendedRequest.url}! Got the {sendedRequest.result} state with {sendedRequest.error}! Return null by path {path}");

                return (false, null);
            }
            else
            {
                byte[] data = sendedRequest.downloadHandler.data;

                return (true, data);
            }
        }
    }

    //TODO : Optimize \/
#if UNITY_EDITOR || UNITY_STANDALONE && !UNITY_WEBGL
    /// <summary>
    /// Class have utils for interact with blockchaine without React. Doesn`t work in WebGL.
    /// </summary>
    public static class AsylumStandaloneUtils
    {
        public static T1 DecodeMapKeys<T1>(byte[] bytes, Storage.Hasher[] hashers)
            where T1 : IType, new()
        {
            if (hashers.Length != 1)
            {
                throw new Exception("This method can decode keys only for SingleMap");
            }
            var key = new T1();
            var shift = 32;
            switch (hashers[0])
            {
                case Storage.Hasher.BlakeTwo128Concat:
                    shift += 16;
                    break;
                case Storage.Hasher.Twox64Concat:
                    shift += 8;
                    break;
                default:
                    throw new NotSupportedException();
            }
            if (key.TypeSize > 0)
            {
                key.Create(bytes[shift..]);
            }
            else
            {
                var p = 0;
                CompactInteger ci = CompactInteger.Decode(bytes[shift..], ref p);
                key.Create(bytes[shift..(int)(shift + ci + p)]);
            }

            return key;
        }

        public static Tuple<T1, T2> DecodeMapKeys<T1, T2>(byte[] bytes, Storage.Hasher[] hashers)
            where T1 : IType, new()
            where T2 : IType, new()
        {
            if (hashers.Length != 2)
            {
                throw new Exception("This method can decode keys only for DoubleMap");
            }
            var result = new Tuple<T1, T2>(new T1(), new T2());
            var shift = 32;
            for (int i = 0; i < hashers.Length; ++i)
            {
                // God won't forgive me for this
                IType key;
                switch (i)
                {
                    case 0:
                        key = result.Item1;
                        break;
                    case 1:
                        key = result.Item2;
                        break;
                    default:
                        throw new Exception("Can't reach this");
                }
                switch (hashers[i])
                {
                    case Storage.Hasher.BlakeTwo128Concat:
                        shift += 16;
                        break;
                    case Storage.Hasher.Twox64Concat:
                        shift += 8;
                        break;
                    default:
                        throw new NotSupportedException();
                }

                if (key.TypeSize > 0)
                {
                    key.Create(bytes[shift..(shift + key.TypeSize)]);
                }
                else
                {
                    var p = 0;
                    CompactInteger ci = CompactInteger.Decode(bytes[shift..], ref p);
                    key.Create(bytes[shift..(int)(shift + ci + p)]);
                }
                shift += key.TypeSize;
            }

            return result;
        }

        public static Tuple<T1, T2, T3> DecodeMapKeys<T1, T2, T3>(byte[] bytes, Storage.Hasher[] hashers)
            where T1 : IType, new()
            where T2 : IType, new()
            where T3 : IType, new()
        {
            if (hashers.Length != 3)
            {
                throw new Exception("This method can decode keys only for NMap, where N == 3");
            }
            var result = new Tuple<T1, T2, T3>(new T1(), new T2(), new T3());
            var shift = 32;
            for (int i = 0; i < hashers.Length; ++i)
            {
                // God won't forgive me for this
                IType key;
                switch (i)
                {
                    case 0:
                        key = result.Item1;
                        break;
                    case 1:
                        key = result.Item2;
                        break;
                    case 2:
                        key = result.Item3;
                        break;
                    default:
                        throw new Exception("Can't reach this");
                }
                switch (hashers[i])
                {
                    case Storage.Hasher.BlakeTwo128Concat:
                        shift += 16;
                        break;
                    case Storage.Hasher.Twox64Concat:
                        shift += 8;
                        break;
                    default:
                        throw new NotSupportedException();
                }
                if (key.TypeSize > 0)
                {
                    key.Create(bytes[shift..(shift + key.TypeSize)]);
                }
                else
                {
                    var p = 0;
                    CompactInteger ci = CompactInteger.Decode(bytes[shift..], ref p);
                    key.Create(bytes[shift..(int)(shift + ci + p)]);
                }
                shift += key.TypeSize;
            }

            return result;
        }

        public static T DecodeValue<T>(string str) where T : IType, new()
        {
            T value = new T();
            if (str != null && str.Length > 0)
            {
                value.Create(str);
            }
            return value;
        }

        public static string GetString(IType value, bool isOptional = false)
        {
            int shift = 1; // TypeSize byte
            if (isOptional)
            {
                shift += 1; // OptionFlag byte
            }
            return System.Text.Encoding.Default.GetString(value.Encode()[shift..]);
        }

        public static BoundedVec ToBoundedVec<BoundedVec>(string str) where BoundedVec: IType, new()
        {
            var boundedVec = new BoundedVec();
            var bytes = new List<byte>();

            bytes.AddRange(new CompactInteger(str.Length).Encode());

            bytes.AddRange(System.Text.Encoding.Default.GetBytes(str));

            boundedVec.Create(bytes.ToArray());

            return boundedVec;
        }

        public static AsylumInterpretation ToAsylumInterpretation((U32 id, Interpretation info) interpretation)
        {
            string id = interpretation.id.Value.ToString();
            string src = "";
            if (interpretation.info._Interpretation.Src.OptionFlag)
            {
                src = GetString(interpretation.info._Interpretation.Src, true);
            }
            string metadata = "";
            if (interpretation.info._Interpretation.Metadata.OptionFlag)
            {
                metadata = GetString(interpretation.info._Interpretation.Metadata, true);
            }
            List<string> tags = new List<string>();
            foreach (var tag in interpretation.info.Tags.Value.Value.Value)
            {
                tags.Add(GetString(tag));
            }
            return new AsylumInterpretation(id, tags.ToArray(), new InterpretationInfo(src, metadata));
        }

        public static AsylumBlueprint ToAsylumBlueprint((U32 id, CollectionInfo info, List<(U32 id, Interpretation info)> interpretations) template)
        {
            string id = template.id.Value.ToString();
            string name = GetString(template.info.Symbol);
            string max = "";
            if (template.info.Max.OptionFlag)
            {
                max = template.info.Max.Value.Value.ToString();
            }
            string metadata = GetString(template.info.Metadata);
            string issuer = Ajuna.NetApi.Utils.GetAddressFrom(template.info.Issuer.Value.Encode());
            int nftCount = (int)template.info.NftsCount.Value;

            List<AsylumInterpretation> interpretations = new List<AsylumInterpretation>();
            foreach (var interpretation in template.interpretations)
            {
                interpretations.Add(ToAsylumInterpretation(interpretation));
            }

            return new AsylumBlueprint(id, name, max, metadata, issuer, nftCount, interpretations.ToArray());
        }

        public static AsylumItem ToAsylumItem((U32 templateId, U32 itemId, NftInfo info, List<(U32 id, TagsOfInfo tags, ResourceInfo interpretation)> interpretations) item)
        {
            string templateId = item.templateId.Value.ToString();
            string itemId = item.itemId.Value.ToString();
            var accountId = (AccountId32)item.info.Owner.Value2;
            string owner = Ajuna.NetApi.Utils.GetAddressFrom(accountId.Value.Encode());
            string royalty = "";
            if (item.info.Royalty.OptionFlag)
            {
                royalty = GetString(item.info.Royalty);
            }
            string metadata = GetString(item.info.Metadata);
            bool equipped = item.info.Equipped.Value;

            List<AsylumInterpretation> interpretations = new List<AsylumInterpretation>();
            foreach (var interpretation in item.interpretations)
            {
                string id = interpretation.id.Value.ToString();

                List<string> tags = new List<string>();
                foreach (var tag in interpretation.tags.Tags.Value.Value.Value)
                {
                    tags.Add(GetString(tag));
                }

                var basicResource = (BasicResource)interpretation.interpretation.Resource.Value2;
                string src = "";
                if (basicResource.Src.OptionFlag)
                {
                    src = GetString(basicResource.Src, true);
                }
                string interpretationMetadata = "";
                if (basicResource.Metadata.OptionFlag)
                {
                    interpretationMetadata = GetString(basicResource.Metadata, true);
                }
                interpretations.Add(new AsylumInterpretation(id, tags.ToArray(), new InterpretationInfo(src, interpretationMetadata)));
            }

            return new AsylumItem(templateId, itemId, owner, royalty, metadata, equipped, interpretations.ToArray());
        }

        public static AsylumTag ToAsylumTag((BoundedVecT4 tag, TagInfo info) tag)
        {
            string tagName = GetString(tag.tag);
            string tagMetadata = GetString(tag.info.Metadata);
            return new AsylumTag(tagName, tagMetadata);
        }

        public static List<AsylumBlueprint> DecodeBlueprints(JArray blueprintsInfos, JArray blueprintsInterpretations)
        {
            List<(U32, CollectionInfo, List<(U32, Interpretation)>)> resultBlueprints = new List<(U32, CollectionInfo, List<(U32, Interpretation)>)>();

            foreach (var pair in blueprintsInfos)
            {
                U32 blueprintId = AsylumStandaloneUtils.DecodeMapKeys<U32>(Ajuna.NetApi.Utils.HexToByteArray(pair[0].ToString(), true), new Storage.Hasher[] { Storage.Hasher.Twox64Concat });
                CollectionInfo blueprintInfo = AsylumStandaloneUtils.DecodeValue<CollectionInfo>(pair[1].ToString());
                resultBlueprints.Add((blueprintId, blueprintInfo, new List<(U32, Interpretation)>()));
            }

            foreach (var pair in blueprintsInterpretations)
            {
                Tuple<U32, U32> keys = AsylumStandaloneUtils.DecodeMapKeys<U32, U32>(Ajuna.NetApi.Utils.HexToByteArray(pair[0].ToString(), true), new Storage.Hasher[] { Storage.Hasher.BlakeTwo128Concat, Storage.Hasher.BlakeTwo128Concat });
                Interpretation blueprintInterpretation = AsylumStandaloneUtils.DecodeValue<Interpretation>(pair[1].ToString());
                var blueprint = resultBlueprints.Find(x => x.Item1.Value == keys.Item1.Value);
                blueprint.Item3.Add((keys.Item2, blueprintInterpretation));
            }

            return resultBlueprints.ConvertAll(element => AsylumStandaloneUtils.ToAsylumBlueprint(element));
        }

        public static List<AsylumItem> DecodeItems(JArray items, JArray itemsInterpretations, JArray itemsInterpretationsTags)
        {
            //Use type for that tuple => less mess
            List<(U32 blueprintId, U32 itemId, NftInfo info, List<(U32 id, TagsOfInfo tags, ResourceInfo interpretation)> interpretations)> 
                resultItems = new ();

            foreach (var pair in items) //12 items
            {
                Tuple<U32,U32> keys = AsylumStandaloneUtils.DecodeMapKeys<U32, U32>(Ajuna.NetApi.Utils.HexToByteArray(pair[0].ToString(), true), 
                                                                        new Storage.Hasher[] { Storage.Hasher.Twox64Concat, Storage.Hasher.Twox64Concat });

                NftInfo info = AsylumStandaloneUtils.DecodeValue<NftInfo>(pair[1].ToString());

                resultItems.Add((keys.Item1, keys.Item2, info, new List<(U32 id, TagsOfInfo tags, ResourceInfo interpretation)>()));
            }

            foreach (var pair in itemsInterpretations) //3 interpretation
            {
                Tuple<U32,U32,U32> keys = AsylumStandaloneUtils.DecodeMapKeys<U32, U32, U32>(Ajuna.NetApi.Utils.HexToByteArray(pair[0].ToString(), true), 
                                                                            new Storage.Hasher[] { Storage.Hasher.BlakeTwo128Concat, Storage.Hasher.BlakeTwo128Concat, Storage.Hasher.BlakeTwo128Concat });
                ResourceInfo interpretation = AsylumStandaloneUtils.DecodeValue<ResourceInfo>(pair[1].ToString());
                var item = resultItems.Find(x => x.blueprintId.Value == keys.Item1.Value && x.itemId.Value == keys.Item2.Value); //Not sure about Find()
                item.interpretations.Add((keys.Item3, new TagsOfInfo(), interpretation));
            }

            foreach (var pair in itemsInterpretationsTags) //3 tags
            {
                Tuple<U32,U32,U32> keys = AsylumStandaloneUtils.DecodeMapKeys<U32, U32, U32>(Ajuna.NetApi.Utils.HexToByteArray(pair[0].ToString(), true), 
                                                                                new Storage.Hasher[] { Storage.Hasher.Twox64Concat, Storage.Hasher.Twox64Concat, Storage.Hasher.Twox64Concat });

                TagsOfInfo tags = AsylumStandaloneUtils.DecodeValue<TagsOfInfo>(pair[1].ToString());
                var item = resultItems.Find(x => x.blueprintId.Value == keys.Item1.Value && x.itemId.Value == keys.Item2.Value); //Not sure about Find()
                int index = item.interpretations.FindIndex(x => x.id.Value == keys.Item3.Value);//and also here
                item.interpretations[index].tags.Tags = tags.Tags;
            }

            return resultItems.ConvertAll(element => AsylumStandaloneUtils.ToAsylumItem(element));
        }
        
        public static List<AsylumTag> DecodeTags(JArray tags)
        {
            List<(BoundedVecT4 tag, TagInfo info)> resultTags = new List<(BoundedVecT4 tag, TagInfo info)>();

            foreach (var pair in tags)
            {
                BoundedVecT4 tag = AsylumStandaloneUtils.DecodeMapKeys<BoundedVecT4>(Ajuna.NetApi.Utils.HexToByteArray(pair[0].ToString(), true), new Storage.Hasher[] { Storage.Hasher.BlakeTwo128Concat });
                TagInfo tagInfo = AsylumStandaloneUtils.DecodeValue<TagInfo>(pair[1].ToString());
                resultTags.Add((tag, tagInfo));
            }

            return resultTags.ConvertAll(element => AsylumStandaloneUtils.ToAsylumTag(element));
        }
    }
#endif
}

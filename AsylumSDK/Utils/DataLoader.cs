using Cysharp.Threading.Tasks;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;

namespace Asylum
{
    //MB Asylum.Utils namespace?

    /// <summary>
    /// Service singletone class for data loading and caching
    /// </summary>
    public class DataLoader : MonoBehaviour
    {
        public static DataLoader instance;

        //Basically it's already loaded data in the byte[] format with string hash|path as key
        //TODO : cash this data to the files for saving between sessions
        private Dictionary<string, byte[]> _savedData = new(); //mb use hashset<DataWithID>?

        /// <summary>
        /// Data loading in this moment
        /// Key - path to loading
        /// Value - callbacks on the data loading end
        /// </summary>
        private Dictionary<string, List<Action<byte[]>>> _loadingDataWithCallbacks = new();

        private void Awake()
        {
            if (instance == null)
            {
                instance = this;

                DontDestroyOnLoad(this.gameObject);
            }
            else if (instance != this)
            {
                Destroy(this.gameObject);
            }
        }

        public void LoadDataByCallback(string path, Action<byte[]> callback)
        {
            if(_savedData == null)
            {
                UnityEngine.Debug.LogWarning($"GetAsync Loaded data dictionary is null! Init");
                _savedData = new();
            }

            if(_savedData.TryGetValue(path, out byte[] savedData))
            {
                //Data is already loaded and in the cache. Don't need to load again - return cached
                if(savedData != null && savedData.Length > 0)
                {
                    UnityEngine.Debug.LogWarning($"LoadDataByCallback {path} returning from SAVED");

                    callback?.Invoke(savedData);

                    return;
                }
                else if(_savedData.ContainsKey(path))
                {
                    UnityEngine.Debug.LogError($"GetAsync Loaded Dictionary CONTAINS key {path} but value is null or empty! Remove this path from dictionary!");
                    _savedData.Remove(path);
                }
            }

            if(_loadingDataWithCallbacks == null)
            {
                UnityEngine.Debug.LogWarning($"GetAsync Loading data dictionary is null! Init");
                _loadingDataWithCallbacks = new();
            }    

            if(_loadingDataWithCallbacks.TryGetValue(path, out List<Action<byte[]>> onLoadedCallbacksList))
            {
                //Data is already loading

                _loadingDataWithCallbacks.Remove(path);

                onLoadedCallbacksList.Add(callback);

                _loadingDataWithCallbacks.Add(path, onLoadedCallbacksList);
            }
            else
            {
                _loadingDataWithCallbacks.Add(path, new List<Action<byte[]>>() { callback });

                LoadAsyncReturn(path).Forget();
            }
        }

        public async UniTask<StatedLoadedData<byte[]>> GetDataAsync(string path)
        {
            if (_savedData == null)
            {
                //UnityEngine.Debug.LogWarning($"GetAsync Loaded data dictionary is null! Init");
                _savedData = new();
            }

            if (_savedData.TryGetValue(path, out byte[] savedData))
            {
                //Data is already loaded and in the cache. Don't need to load again - return cached
                if (savedData != null && savedData.Length > 0)
                {
                    UnityEngine.Debug.LogWarning($"GetDataAsync by {path} returning from SAVED ");

                    return new StatedLoadedData<byte[]>(true, savedData);
                }
                else if (_savedData.ContainsKey(path))
                {
                    UnityEngine.Debug.LogError($"GetAsync Loaded Dictionary CONTAINS key {path} but value is null or empty! Remove this path from dictionary!");
                    _savedData.Remove(path);
                }
            }

            if(_loadingDataWithCallbacks == null)
            {
                _loadingDataWithCallbacks = new();
            }

            if (_loadingDataWithCallbacks.TryGetValue(path, out List<Action<byte[]>> onLoadedCallbacksList))
            {
                //Data is loading => w8 for load end
                while (!_savedData.ContainsKey(path))
                {
                    await UniTask.Yield();
                }

                if (_savedData.TryGetValue(path, out byte[] loadedData))
                {
                    if (loadedData != null && loadedData.Length > 0)
                    {
                        return new StatedLoadedData<byte[]>(true, loadedData);
                    }
                }

                UnityEngine.Debug.LogError($"Something wrong with data => Return null!");
                return new StatedLoadedData<byte[]>(false, null);
            }
            else
            {
                //Data is not loading rn => Start loading

                _loadingDataWithCallbacks.Add(path, new List<Action<byte[]>>());

                var result = await LoadAsyncReturn(path);

                return result;
            }
        }


        private async UniTask<StatedLoadedData<byte[]>> LoadAsyncReturn(string path)
        {
            await UniTask.SwitchToMainThread();

            UnityWebRequest request = UnityWebRequest.Get($"{path}");

            var sendedRequest = await request.SendWebRequest();

            if (sendedRequest.result != UnityWebRequest.Result.Success)
            {
                UnityEngine.Debug.LogError($"Can't load from {sendedRequest.url}! Got the {sendedRequest.result} state with {sendedRequest.error} by path {path}! Return null!");

                return new StatedLoadedData<byte[]>(false, null);
            }
            else
            {
                //Data is loaded successfully!

                await UniTask.SwitchToMainThread(); //Just to be sure

                var data = sendedRequest.downloadHandler.data;

                //Call this for invoking callbacks if we subscribe from non-async function and add loaded data to the saved
                OnDataWasLoaded(path, data); 

                return new StatedLoadedData<byte[]>(true, data);
            }
        }

        private void OnDataWasLoaded(string path, byte[] rawLoadedData)
        {
            if(_loadingDataWithCallbacks.TryGetValue(path, out List<Action<byte[]>> loadedCallbacksList))
            {
                List<Action<byte[]>> callbackList = new(loadedCallbacksList.ToArray());
                
                //Anyway return data from loading 
                _loadingDataWithCallbacks.Remove(path);

                if (_savedData.TryAdd(path, rawLoadedData))
                {
                    //Data succesfully loaded =>  call all the callbacks

                    foreach(var singleCallback in callbackList)
                    {
                        singleCallback?.Invoke(rawLoadedData);
                    }
                }
                else
                {
                    //Looks like we alredy have that data in our cache! => check are they equal

                    if(_savedData.TryGetValue(path, out byte[] loadedDataFromDictionary))
                    {
                        if(rawLoadedData.Length != loadedDataFromDictionary.Length)
                        {
                            UnityEngine.Debug.LogError($"DataLoader:OnDataWasLoaded path : {path} data is not equal by lenght!");

                            //Our fresh loaded data IS NOT equal of already saved.
                            //TODO : Implement one of the few options:
                            //- re-init data in dictionary and callback with fresh data
                            //or
                            //- forget about new one and trust only old one - callback with old one as well
                        }
                        else
                        {
                            for(int i = 0; i < rawLoadedData.Length; ++i)
                            {
                                if(rawLoadedData[i] != loadedDataFromDictionary[i])
                                {
                                    UnityEngine.Debug.LogError($"DataLoader:OnDataWasLoaded path : {path} data is not equal by {i} index!");
                                }
                            }
                        }

                        //Anyway return the old one by callback

                        foreach (var singleCallback in callbackList)
                        {
                            singleCallback?.Invoke(rawLoadedData);
                        }
                    }
                    else
                    {
                        UnityEngine.Debug.LogError($"DataLoader:OnDataWasLoaded ERROR path : {path} can't get data from cached!");
                        //Throw exception?
                    }
                }
            }
            else
            {
                UnityEngine.Debug.LogError($"DataLoader:OnDataWasLoaded path : {path} data is not loading actually!");
            }
        }

        /// <summary>
        /// Generic class representing the loaded data with loading state
        /// </summary>
        /// <typeparam name="T"></typeparam>
        public class StatedLoadedData<T>
        {
            /// <summary>
            /// State of the data loading
            /// False if data was not loaded of any reason
            /// </summary>
            public bool state;
            public T data;

            public StatedLoadedData(bool n_state, T n_data)
            {
                state = n_state;
                data = n_data;
            }
        }
    }
}

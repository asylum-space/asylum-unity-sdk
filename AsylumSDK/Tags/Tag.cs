using System;

namespace Asylum
{
    //TODO : Create nice dropdown for choosing tags

    /// <summary>
    /// Base generic class represents Tag
    /// </summary>
    /// <typeparam name="T">Type of the tag data</typeparam>
    public abstract class Tag<T> : TagBase
    {
        //TODO : mb few generic type for returning?
        public override Type DataType => typeof(T);
    }
}

using System;
using System.Collections.Generic;
using UnityEngine;

namespace Asylum
{
    /// <summary>
    /// Service class for tags controlling
    /// </summary>
    [CreateAssetMenu(fileName = "TagController", menuName = "Asylum/Tag Controller instance", order = 0)]
    public class TagController : ScriptableObject
    {
        private static TagController _instance;
        public static TagController Instance
        {
            get
            {
                if (_instance == null)
                {
                    var assets = Resources.LoadAll<TagController>("");
                    if (assets == null || assets.Length == 0)
                    {
                        //Mb debug log and try to create instance?
                        throw new Exception("Can't find any TagController instance!");
                    }
                    else if (assets.Length > 1)
                    {
                        UnityEngine.Debug.LogWarning($"Have found {assets.Length} instances of the TagController! Return the 1st one!");
                    }

                    _instance = assets[0];
                }

                return _instance;
            }
        }

        //TODO : Mb change it for the hashset or dictionary?
        //TODO : Allow to execute in editor and find the SO instances in realtime
        [SerializeField] private List<TagBase> _implementedTags = new();

        //TODO : same function TagBase GetTagBase() with diff parameters
        public TagBase GetTagBaseByType(Type tagType)
        {
            var found = _implementedTags.FindAll(tag => tag.DataType == tagType);
            if (found != null
                && found.Count > 0)
            {
                //TODO optimize for more than 1 founded tag
                if (found.Count != 1)
                {
                    //Look's like there're more than one tag implemented 
                    UnityEngine.Debug.LogWarning($"There are {found.Count} tag with data type of {tagType}");
                }

                return found[0]; //Just return first one founded
            }
            else
            {
                UnityEngine.Debug.LogWarning($"There're no tag with type {tagType}! Return null!");

                return null;
            }
        }

        public TagBase GetTagBaseByString(string tagString)
        {
            var found = _implementedTags.FindAll(tag => tag.TagString == tagString);

            if (found != null
                && found.Count > 0)
            {
                //TODO optimize for more than 1 founded tag
                if (found.Count != 1)
                {
                    //Look's like there're more than one tag implemented 
                    UnityEngine.Debug.LogWarning($"There are {found.Count} tag string {tagString}");
                }

                return found[0]; //Just return first one founded
            }
            else
            {
                UnityEngine.Debug.LogWarning($"There're no tag string {tagString}! Return null!");

                return null;
            }
        }
    }
}

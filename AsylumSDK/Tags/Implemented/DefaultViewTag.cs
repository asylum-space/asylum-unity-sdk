using System.Collections.Generic;
using UnityEngine;

namespace Asylum
{
    //Un-comment this for creating instance
    //[CreateAssetMenu(fileName = "New DefaultViewTag", menuName = "Asylum/Asylum DefaultViewTag ScriptableObject", order = 1)]

    /// <summary>
    /// Implementation of the "default-view" tag
    /// </summary>
    public class DefaultViewTag : Tag<Sprite>
    {
        public override string TagString => "default-view";

        /// <summary>
        /// Create the sprite from interpretation raw data
        /// </summary>
        public override object AdaptData(byte[] rawData, Dictionary<string, object> metaDataDictionary)
        {
            if(rawData == null || rawData.Length <= 0)
            {
                UnityEngine.Debug.LogWarning($"DefaultViewTag : raw data is corrupted! Return null!");
                return null;
            }

            Sprite sprite = null;

            Texture2D texture = new Texture2D(1, 1);
            if (texture.LoadImage(rawData))
            {

                sprite = Sprite.Create(texture, new Rect(0, 0, texture.width, texture.height),
                                                        new Vector2(texture.width / 2, texture.height / 2));

                //UnityEngine.Debug.LogError($"Sprite created! id:{result.Item2}");
            }
            else
            {
                UnityEngine.Debug.LogError($"DefaultViewInterpretation Sprite cant be loaded! Something wrong with texture! Return NULL!");
            }

            return sprite;
        }
    }
}

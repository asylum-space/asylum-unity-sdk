using System.Collections;
using System.Collections.Generic;
using UnityEngine;
namespace Asylum
{
    /// <summary>
    /// Represents tag "2d-sprite-atlas" as array of the UnityEngine.Sprite type array
    /// </summary>
    [CreateAssetMenu(fileName = "New SpriteAtlas2DTag", menuName = "Asylum/Asylum SpriteAtlas2DTag ScriptableObject", order = 1)]
    public class SpriteAtlas2DTag : Tag<Sprite[]>
    {
        public override string TagString => "2d-sprite-atlas";

        /// <summary>
        /// Create sprite from interpretation data and split as sprite atlas by setup from metadata  
        /// </summary>
        /// <param name="rawData">Byte array represents a sprite as raw data</param>
        /// <param name="metaDataDictionary">Interpretation's metadata with settings of the atlas (f.e. row count)</param>
        /// <returns></returns>
        public override object AdaptData(byte[] rawData, Dictionary<string, object> metaDataDictionary)
        {
            if (rawData == null || rawData.Length <= 0)
            {
                UnityEngine.Debug.LogWarning($"InventoryViewTag : raw data is corrupted! Return null!");
                return null;
            }

            if(metaDataDictionary == null || metaDataDictionary.Count == 0)
            {
                //TODO: optimaze default values
                UnityEngine.Debug.LogWarning($"InventoryViewTag : MetaDataDictionary is corrupted! Return null!");
                return null;
            }

            Texture2D texture = new Texture2D(1, 1);
            if (texture.LoadImage(rawData))
            {
                ParseSpriteAtlasMetadata(metaDataDictionary, out int rowCount, out int columnCount, out Vector2 pivot, out int tileWidth, out int tileHeight);

                var itemSpriteAtlas = new List<Sprite>();

                int spriteIterator = 0;
                for (int i = rowCount - 1; i >= 0; i--)
                {
                    for (int j = 0; j < columnCount; j++)
                    {
                        Sprite newSprite = Sprite.Create(texture,
                            new Rect(j * tileWidth, i * tileHeight, tileWidth, tileHeight),
                            pivot);
                        newSprite.name = $"{spriteIterator++}";

                        itemSpriteAtlas.Add(newSprite);
                    }
                }

                if (spriteIterator > 0)
                {
                    return itemSpriteAtlas.ToArray();
                }
            }

            UnityEngine.Debug.LogError($"SpriteAtlas2DTag sprite isn't an atlas or rawData cannot be adapt as sprite-atlas! Return null!");
            return null;
        }

        //TODO: optimaze default values
        private void ParseSpriteAtlasMetadata(Dictionary<string, object> metaDataDictionary,out int rowCount,out int columnCount,out Vector2 pivot,out int tileWidth,out int tileHeight)
        {
            rowCount = 0;
            columnCount = 0;
            pivot = new Vector2();
            tileWidth = 0;
            tileHeight = 0;

            if (metaDataDictionary.TryGetValue("rowCount", out object rowCountObject))
            {
                if (!int.TryParse(rowCountObject.ToString(), out rowCount))
                    Debug.LogError("SpriteAtlas2DTag: In AdaptData can`t convert metadata for variable 'rowCount'.");
            }
            else
            {
                Debug.LogError("SpriteAtlas2DTag: In ParseSpriteAtlasMetadata - Can`t find value by key 'rowCount'.");
            }
            if (metaDataDictionary.TryGetValue("columnCount", out object columnCountObject))
            {
                if (!int.TryParse(columnCountObject.ToString(), out columnCount))
                    Debug.LogError("SpriteAtlas2DTag: In AdaptData can`t convert metadata for variable 'columnCount'.");
            }
            else
            {
                Debug.LogError("SpriteAtlas2DTag: In ParseSpriteAtlasMetadata - Can`t find value by key 'columnCount'.");
            }
            if (metaDataDictionary.TryGetValue("pivotX", out object pivotXObject))
            {
                if (!float.TryParse(pivotXObject.ToString(), out pivot.x))
                    Debug.LogError("SpriteAtlas2DTag: In AdaptData can`t convert metadata for variable 'pivotX'.");
            }
            else
            {
                Debug.LogError("SpriteAtlas2DTag: In ParseSpriteAtlasMetadata - Can`t find value by key 'pivotX'.");
            }
            if (metaDataDictionary.TryGetValue("pivotY", out object pivotYObject))
            {
                if (!float.TryParse(pivotYObject.ToString(), out pivot.y))
                    Debug.LogError("SpriteAtlas2DTag: In AdaptData can`t convert metadata for variable 'pivotY'.");
            }
            else
            {
                Debug.LogError("SpriteAtlas2DTag: In ParseSpriteAtlasMetadata - Can`t find value by key 'pivotY'.");
            }
            if (metaDataDictionary.TryGetValue("tileWidth", out object tileWidthObject))
            {
                if (!int.TryParse(tileWidthObject.ToString(), out tileWidth))
                    Debug.LogError("SpriteAtlas2DTag: In AdaptData can`t convert metadata for variable 'tileWidth'.");
            }
            else
            {
                Debug.LogError("SpriteAtlas2DTag: In ParseSpriteAtlasMetadata - Can`t find value by key 'tileWidth'.");
            }
            if (metaDataDictionary.TryGetValue("tileHeight", out object tileHeightObject))
            {
                if (!int.TryParse(tileHeightObject.ToString(), out tileHeight))
                    Debug.LogError("SpriteAtlas2DTag: In AdaptData can`t convert metadata for variable 'tileHeight'.");
            }
            else
            {
                Debug.LogError("SpriteAtlas2DTag: In ParseSpriteAtlasMetadata - Can`t find value by key 'tileHeight'.");
            }
        }
    }
}

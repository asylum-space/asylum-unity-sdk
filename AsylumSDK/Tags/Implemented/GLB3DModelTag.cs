using Siccity.GLTFUtility;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Asylum
{
    /// <summary>
    /// This tag return GLB model as GameObject which should be cached in un-active GameObject in the scene if using inside the build
    /// </summary>
  

    [CreateAssetMenu(fileName = "New GLB3DModelTag", menuName = "Asylum/Asylum GLB3DModelTag ScriptableObject", order = 1)]
    public class GLB3DModelTag : Tag<GameObject>
    {
        public override string TagString => "3d-model";

        [Space]
        [SerializeField] private string _parentGOUnityTag = "Parent3D";

        private Transform _deactivatedTransform;

        public override object AdaptData(byte[] rawData, Dictionary<string, object> metaDataDictionary)
        {
            /*
             * Here is HACK:
                - You can't create prefab in run-time, so GLB|GLTF importer import run-time 3D models as GameObjects
                - We don't need that item cause it will be active in the scene, so we create deactivated "wrapper" - _deactivatedTransform
                - Which will be hold all our ACTIVE 3DModels represented as GameObjects
                Also it's HIGHLY NECESSARY have our tag {_parentGOUnityTag} in the Unity's TagManager
            */


            if (_deactivatedTransform == null)
            {
                GameObject parentGO = GameObject.FindGameObjectWithTag(_parentGOUnityTag);

                if (parentGO == null)
                {
                    parentGO = new GameObject();
                    parentGO.tag = _parentGOUnityTag;

                    parentGO.name = _parentGOUnityTag;
                    parentGO.SetActive(false);

                    DontDestroyOnLoad(parentGO);
                }

                _deactivatedTransform = parentGO.transform;
            }

            GameObject imported = Importer.LoadFromBytes(rawData);

            //TODO : Cache all the new gameobjects for avoid multiply creation, fe in List<(byte{], GameObject)>
            GameObject savedGO = GameObject.Instantiate(imported, _deactivatedTransform);

            Destroy(imported);

            return savedGO;
        }
    }
}

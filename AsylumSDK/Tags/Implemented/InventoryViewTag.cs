using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Asylum
{
    /// <summary>
    /// Represent tag "inventory-view"
    /// </summary>
    [CreateAssetMenu(fileName = "New InventoryViewTag", menuName = "Asylum/Asylum InventoryViewTag ScriptableObject", order = 1)]
    public class InventoryViewTag : Tag<Sprite>
    {
        public override string TagString => "inventory-view";

        public override object AdaptData(byte[] rawData, Dictionary<string, object> metaDataDictionary)
        {
            if (rawData == null || rawData.Length <= 0)
            {
                UnityEngine.Debug.LogWarning($"InventoryViewTag : raw data is corrupted! Return null!");
                return null;
            }

            Sprite sprite = null;

            Texture2D texture = new Texture2D(1, 1);
            if (texture.LoadImage(rawData))
            {

                sprite = Sprite.Create(texture, new Rect(0, 0, texture.width, texture.height),
                                                        new Vector2(texture.width / 2, texture.height / 2));

                //UnityEngine.Debug.LogError($"Sprite created! id:{result.Item2}");
            }
            else
            {
                UnityEngine.Debug.LogError($"InventoryViewTag Sprite cant be loaded! Something wrong with texture! Return NULL!");
            }

            return sprite;
        }
    }
}

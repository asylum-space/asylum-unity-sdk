using System;
using System.Collections.Generic;
using UnityEngine;

namespace Asylum
{
    /// <summary>
    /// Base abstract class represents Tag 
    /// </summary>
    public abstract class TagBase : ScriptableObject
    {
        /// <summary>
        /// Tag string identificator
        /// </summary>
        public abstract string TagString { get; }

        public abstract Type DataType { get; }

        //https://stackoverflow.com/a/3142058
        //The highly promising way ||
        //How to resolve this mess \/
        public abstract object AdaptData(byte[] rawData, Dictionary<string, object> metaDataDictionary);

        //-------------------
    }
}

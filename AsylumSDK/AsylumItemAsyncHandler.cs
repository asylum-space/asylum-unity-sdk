using Asylum;
using Cysharp.Threading.Tasks;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

/// <summary>
/// Class for async loading and handling <see cref="Asylum.AsylumItem"/> entity
/// Provide functional for interaction with already loaded <see cref="Asylum.AsylumItem"/> as well
/// </summary>
public class AsylumItemAsyncHandler
{
    private AsylumItem _item;
    private string _mainPath;

    //Holds LOADED successfully handlers
    private List<AsylumInterpretationAsyncHandler> _interpretationsHandlers = new ();
    private Dictionary<string, string> _itemMetadata = new();

    private string _itemName = "NO_NAME_LOADED";
    private string _itemDescription = "NO_DESCRIPTION_LOADED";

    public string Name => _itemName;
    public string Description => _itemDescription;
    public AsylumItem AsylumItem => _item;

    public List<AsylumInterpretationAsyncHandler> InterpretationHandlers => _interpretationsHandlers;

    //Holds handles which loading rn
    private List<AsylumInterpretationAsyncHandler> _loadingHandlers = new();

    public AsylumItemAsyncHandler(AsylumItem item, string dataPath)
    {
        _item = item;
        _mainPath = dataPath;
        
        if(_interpretationsHandlers == null)
        {
            _interpretationsHandlers = new();
        }

        if(_loadingHandlers == null)
        {
            _loadingHandlers = new();
        }

        foreach(var inter in _item.interpretations)
        {
            _loadingHandlers.Add(new(inter, _mainPath));
        }
    }

    /// <summary>
    /// Async function for loading all data of the item including all of its interpretations
    /// </summary>
    /// <returns>Result of the loading Return true only if at least one of its interpretation was loaded succesfully</returns>
    public async UniTask<bool> LoadAllItemDataAsync()
    {
        var itemLoadingData = await LoadAndParseItemMetaAsync();

        if(itemLoadingData)
        {
            List<UniTask<bool>> interpretationsLoadingTasks = new();

            foreach (var loadingInterHandler in _loadingHandlers)
            {
                interpretationsLoadingTasks.Add(loadingInterHandler.LoadInterpretationAsync());
            }

            //MB .WhenAny?
            var loadingInterpretationsResults = await UniTask.WhenAll(interpretationsLoadingTasks);

            int lenghtForCheck = _loadingHandlers.Count;

            if(loadingInterpretationsResults.Length != _loadingHandlers.Count)
            {
                UnityEngine.Debug.LogWarning($"AsylumItemAsyncHandler item:{Name} id:{_item.id}|bp:{_item.blueprintId} results lenght != handlers lenght! Use min of them!");

                lenghtForCheck = Mathf.Min(loadingInterpretationsResults.Length, _loadingHandlers.Count);
            }

            if(_interpretationsHandlers == null)
            {
                _interpretationsHandlers = new();
            }

            int handlersCount = _interpretationsHandlers.Count;

            for (int i = 0; i < lenghtForCheck; ++i)
            {
                if(loadingInterpretationsResults[i])
                {
                    _interpretationsHandlers.Add(_loadingHandlers[i]);
                }
                else
                {
                    UnityEngine.Debug.LogError($"Item {Name} id:{_item.id}|bp:{_item.blueprintId} Interpretation {_loadingHandlers[i].ID} not loaded correctly!");
                }
            }

            if(_interpretationsHandlers.Count != handlersCount)
            {
                //Array was changed => interpretation was added => return true
                return true;
            }
            else
            {
                UnityEngine.Debug.LogError($"Item {Name} id:{_item.id}|bp:{_item.blueprintId} does not load any interpretation. Return false!");

                return false;
            }
        }
        else
        {
            UnityEngine.Debug.LogError($"AsylumItemAsyncHandler Item ID: {_item.id} meta was not loaded!");

            return false;
        }
    }

    /// <summary>
    /// Async loading of the <see cref="Asylum.AsylumItem"/> metadata and parsing it as dictionary
    /// </summary>
    /// <returns>Result of the loading and parsing Return true only if loading and parsing was succesfull</returns>
    private async UniTask<bool> LoadAndParseItemMetaAsync()
    {
        string metadataPath = $"{_mainPath}{_item.metadata}";

        var metadataLoadingTask = DataLoader.instance.GetDataAsync(metadataPath);

        var result = await metadataLoadingTask;

        if(result.state)
        {
            await UniTask.Yield();

            string jsonReturned = Encoding.UTF8.GetString(result.data);
            Dictionary<string, string> metadataDictionary = new Dictionary<string, string>();

            metadataDictionary = JsonConvert.DeserializeObject<Dictionary<string, string>>(jsonReturned);

            if (metadataDictionary != null && metadataDictionary.Count > 0)
            {
                if (metadataDictionary.TryGetValue("name", out string itemLoadedName))
                {
                    _itemName = itemLoadedName;
                }

                if (metadataDictionary.TryGetValue("description", out string loadedDescription))
                {
                    _itemDescription = loadedDescription;
                }

                //TODO : Add possibility get custom item metadata fields
                //Save for later or for any other custom field
                _itemMetadata = metadataDictionary;

                return true;
            }
            else
            {
                UnityEngine.Debug.LogWarning($"AsylumItemAsyncHandler ERROR Item.metadata parsing ID : {_item.id} ");

                return false;
            }
        }
        else
        {   
            UnityEngine.Debug.LogError($"AsylumItemAsyncHandler LoadAndParseItemMetaAsync can't load data from {metadataPath}");

            return false;
        }
    }

    public object[] GetAdapted(string tagString)
    {
        List<object> founded = new();

        _interpretationsHandlers.ForEach(
            inter =>
                {
                    var interAdapted = inter.GetAdapted(tagString);
                    
                    if(interAdapted != null)
                    {
                        founded.Add(interAdapted);
                    }
                }
            );

        if(founded.Count > 1)
        {
            UnityEngine.Debug.LogWarning($"More that one {tagString} adapted!");
        }

        return founded.ToArray();
    }

    public object[] GetAdapted(Type matchingType, string tagString = null)
    {
        List<object> founded = new();

        _interpretationsHandlers.ForEach(
            inter =>
                {
                    var interAdapted = inter.GetAdaptedArray(matchingType, tagString);

                    if (interAdapted != null
                        && interAdapted.Length > 0)
                    {
                        founded.AddRange(interAdapted);
                    }
                }
            );

        if(founded.Count == 0)
        {
            UnityEngine.Debug.LogWarning($"AsylumItemAsyncHandler GetAdapted({matchingType},{tagString}) found 0!");
        }

        return founded.ToArray();
    }

    //mb use hash-set for NOT having dublicates?
    public TypedData[] GetAllAdapted()
    {
        List<TypedData> result = new();

        foreach(var inter in _interpretationsHandlers)
        {
            var dataArray = inter.GetAllAdapted();

            if(dataArray != null && dataArray.Length > 0)
            {
                result.AddRange(dataArray);
            }
        }

        return result.ToArray();
    }

    public bool IsTagInterpretated(string tagString)
    {
        return _interpretationsHandlers.Any(inter => inter.IsTagInterpretated(tagString));
    }
}

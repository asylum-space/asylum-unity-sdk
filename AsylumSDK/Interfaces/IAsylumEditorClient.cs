#if !UNITY_WEBGL || UNITY_EDITOR

using Ajuna.NetApi.Model.Types;
using Asylum;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Threading.Tasks;
using UnityEngine;

/// <summary>
/// Interface for editor purpose which define client-side of the chain-unity connection
/// </summary>
public interface IAsylumEditorClient
{
    /// <summary>
    /// On receive Asylum minted items.
    /// </summary>
    public abstract Action<AsylumItem[]> OnMintedItemsRecieved { get; set; }

    /// <summary>
    /// Connect to chain from Unity Editor. Or reconnects if it was connected.
    /// </summary>
    public abstract Task ReconnectEditorAsync();

    /// <summary>
    /// Connect to chain from Unity Editor. Or reconnects if it was connected.
    /// </summary>
    public abstract Task ReconnectEditorAsync(Action refreshLogicCallback);

    /// <summary>
    /// Disconnect from chain from Unity Editor.
    /// </summary>
    public abstract Task CloseConnectionEditorAsync();

    /// <summary>
    /// Send request to chain for get Asylum User Items from Unity Editor.
    /// </summary>
    public abstract Task<AsylumItem[]> RequestUserItemsEditorAsync(string accountString);

    /// <summary>
    /// Send request to chain for get Asylum Blueprints from Unity Editor.
    /// </summary>
    public abstract Task<AsylumBlueprint[]> RequestUserBlueprintsEditorAsync();

    /// <summary>
    /// Send request to chain for get Asylum Tags from Unity Editor.
    /// </summary>
    public abstract Task<AsylumTag[]> RequestTagsEditorAsync();

    /// <summary>
    /// Send request to chain for mint Asylum Item for user from Unity Editor.
    /// </summary>
    public abstract Task<bool> RequestMintItemEditorAsync(Account owner, int id, AsylumItemMetadata metadata);

    /// <summary>
    /// Set account secret phrase.
    /// </summary>
    public abstract void SetSecretPhrase(string secretPhrase);

}
#endif
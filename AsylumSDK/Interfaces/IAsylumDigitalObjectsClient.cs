using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Asylum
{
    /// <summary>
    /// Interface which define chain-unity connection
    /// </summary>
    public interface IAsylumDigitalObjectsClient 
    {
        /// <summary>
        /// On receive Asylum user items.
        /// </summary>
        public abstract Action<AsylumItem[]> OnUserItemsRecieved { get; set; }
        /// <summary>
        /// On receive Asylum user info.
        /// </summary>
        public abstract Action<UserInfo> OnUserInfoRecieved { get; set; }
        /// <summary>
        /// On receive Asylum space metadata.
        /// </summary>
        public abstract Action<SpaceMetadata> OnSpaceMetadataRecieved { get; set; }
        /// <summary>
        /// On receive Asylum minted items.
        /// </summary>
        public abstract Action<AsylumItem[]> OnMintedItemsRecieved { get; set; }

        /// <summary>
        /// Connect to chain.
        /// </summary>
        public abstract void OnConnected();
        /// <summary>
        /// Disconnect from chain.
        /// </summary>
        public abstract void OnDisconnected();
        /// <summary>
        /// Send request to chain for get Asylum user items.
        /// </summary>
        public abstract void SendUserItemsRequest();
        /// <summary>
        /// Send request to chain for get Asylum user info.
        /// </summary>
        public abstract void SendUserInfoRequest();
        /// <summary>
        /// Send request to chain for get Asylum space metadata.
        /// </summary>
        public abstract void SendSpaceMetadataRequest();
        public abstract void SendMintItemRequest(int blueprintId, AsylumItemMetadata metadata);
        
        //TODO : Implement - should this be in interface? We use this only in the editor mode
        //public abstract Action<AsylumBlueprint[]> OnBlueprintsRecieved { get; set; }
        //public abstract Action<AsylumInterpretation[]> OnInterpretationsRecieved { get; set; }
    }
}

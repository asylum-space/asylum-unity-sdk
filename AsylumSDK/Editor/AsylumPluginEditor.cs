#if UNITY_EDITOR

using Asylum;
using Cysharp.Threading.Tasks;
using System.Threading.Tasks;
using UnityEditor;
using UnityEditorInternal;
using UnityEngine;

/// <summary>
/// Service class for Unity editor inspector view
/// </summary>
[CustomEditor(typeof(AsylumPlugin))]
public class AsylumPluginEditor : Editor
{
    /// <summary>
    /// Page conditions.
    /// </summary>
    private bool isTagPageClick = false, isBlueprintPageClick = false, isItemPageClick = false;
    /// <summary>
    /// Target
    /// </summary>
    private AsylumPlugin _plugin;

    public override void OnInspectorGUI()
    {
        _plugin = GetPlugin();

        SetAccountSecretKeyDrawing();


        if (this._plugin.Account != null)
        {
            DataGetterDrawing();

            DataDrawing();
        }

    }
    /// <summary>
    /// All conditions set false.
    /// </summary>
    private void SetAllButtonFalse()
    {
        isTagPageClick = false;
        isBlueprintPageClick = false;
        isItemPageClick = false;
    }

    /// <summary>
    /// Draw button for interactions with chain. Get Tags | User Items | Blueprints.
    /// </summary>
    private void DataGetterDrawing()
    {
        var h1LabelStyle = new GUIStyle(GUI.skin.label) { alignment = TextAnchor.MiddleCenter, richText = true, fontSize = 18 };
        var textFieldStyle = new GUIStyle(GUI.skin.textField) { alignment = TextAnchor.MiddleCenter };
        var buttonSetSecretKeyStyle = new GUIStyle(GUI.skin.button) { alignment = TextAnchor.MiddleCenter, richText = true };

        GUILayout.BeginVertical();

        GUILayout.Space(10f);

        GUILayout.BeginHorizontal();
        GUILayout.Label("Get Data From Blockchain", h1LabelStyle);
        GUILayout.EndHorizontal();

        GUILayout.Space(10f);

        GUILayout.BeginHorizontal();
        if (GUILayout.Button("Get Tags", buttonSetSecretKeyStyle))
        {
            SetAllButtonFalse();
            isTagPageClick = true;
            this._plugin.GetAllAsylumTags();
        }
        if (GUILayout.Button("Get User Items", buttonSetSecretKeyStyle))
        {
            SetAllButtonFalse();
            isItemPageClick = true;
            this._plugin.GetAllAsylumUserItems();
        }
        if (GUILayout.Button("Get Blueprints", buttonSetSecretKeyStyle))
        {
            SetAllButtonFalse();
            isBlueprintPageClick = true;
            this._plugin.GetAllAsylumBlueprints();
        }
        GUILayout.EndHorizontal();

        GUILayout.EndVertical();


    }

    /// <summary>
    /// Drawing the incoming information. Tags Items or Blueprints.
    /// </summary>
    private void DataDrawing()
    {
        var h1LabelStyle = new GUIStyle(GUI.skin.label) { alignment = TextAnchor.MiddleCenter, richText = true, fontSize = 18 };
        var mintItemLabelStyle = new GUIStyle(GUI.skin.button) { alignment = TextAnchor.MiddleCenter, richText = true, fontSize = 12 };
        var nameLabelStyle = new GUIStyle(GUI.skin.label) { alignment = TextAnchor.MiddleCenter, richText = true, fontSize = 12 };

        if (this.isTagPageClick && this._plugin._asylumTags != null && this._plugin._asylumTags.Count > 0)
        {
            GUILayout.Space(10f);
            GUILayout.Label("Tags", h1LabelStyle);
            GUILayout.Space(10f);

            GUILayout.BeginVertical();
                //maybe not best option. optimize. only for editor inspector.
                SerializedProperty tags = serializedObject.FindProperty("_asylumTags");
                serializedObject.Update();

                for (int i = 0; i < tags.arraySize; i++)
                {
                    string tagName = '"' + this._plugin._asylumTags[i].tag + '"';
                    string tagMetadataContent = this._plugin._asylumTags[i].description != null ? tagName + " - " + this._plugin._asylumTags[i].description : tagName;

                    EditorGUILayout.PropertyField(tags.GetArrayElementAtIndex(i), new GUIContent(tagMetadataContent));
                }

            GUILayout.EndVertical();
        }

        if (this.isItemPageClick && this._plugin._asylumItems != null && this._plugin._asylumItems.Count > 0)
        {
            GUILayout.Space(10f);
            GUILayout.Label("User Items", h1LabelStyle);
            GUILayout.Space(10f);

            GUILayout.BeginVertical();

                SerializedProperty items = serializedObject.FindProperty("_asylumItems");
                serializedObject.Update();

                for (int i = 0; i < items.arraySize; i++)
                {
                    EditorGUILayout.PropertyField(items.GetArrayElementAtIndex(i), new GUIContent("Item "+(i+1)));
                }

            GUILayout.EndVertical();
        }

        if (this.isBlueprintPageClick && this._plugin._asylumBlueprints != null && this._plugin._asylumBlueprints.Count > 0)
        {
            GUILayout.Space(10f);
            GUILayout.Label("Blueprints", h1LabelStyle);
            GUILayout.Space(10f);

            GUILayout.BeginVertical();
               
                SerializedProperty blueprints = serializedObject.FindProperty("_asylumBlueprints");
                serializedObject.Update();

                for (int i = 0; i < blueprints.arraySize; i++)
                {
                    EditorGUILayout.PropertyField(blueprints.GetArrayElementAtIndex(i), new GUIContent(this._plugin._asylumBlueprints[i].name));
                    if (GUILayout.Button("Mint item from blueprint", mintItemLabelStyle))
                    {
                        AsylumItemMetadata itemMetadata = new AsylumItemMetadata();

                        itemMetadata.name = $"{this._plugin._asylumBlueprints[i].name}: NFT Item {this._plugin._asylumBlueprints[i].nftCount}";

                    if (this._plugin._asylumBlueprints[i].description != null)
                        itemMetadata.description = this._plugin._asylumBlueprints[i].description;
                    else
                        itemMetadata.description = $"Metadata of blueprint with name - {this._plugin._asylumBlueprints[i].name} and id - {this._plugin._asylumBlueprints[i].id} not found. Use temp description.";

                        this._plugin.MintItemFromBlueprint(this._plugin._asylumBlueprints[i].id, itemMetadata);
                    }
                }

            GUILayout.EndVertical();
        }
    }

    /// <summary>
    /// Drawing the input for the user secret phrase and connecting to the chain at the push of a button
    /// </summary>
    private void SetAccountSecretKeyDrawing()
    {
        var h1LabelStyle = new GUIStyle(GUI.skin.label) { alignment = TextAnchor.MiddleCenter, richText = true, fontSize = 18 };
        var nameLabelStyle = new GUIStyle(GUI.skin.label) { alignment = TextAnchor.MiddleCenter, richText = true, fontSize = 12, stretchWidth = false };
        var textFieldStyle = new GUIStyle(GUI.skin.textField) { alignment = TextAnchor.MiddleCenter };
        var buttonSetSecretKeyStyle = new GUIStyle(GUI.skin.button) { alignment = TextAnchor.MiddleCenter, richText = true, fontSize = 14, stretchWidth = true, fixedWidth = 200, fixedHeight = 25 };

        GUILayout.BeginVertical();

        GUILayout.Space(20f);

        GUILayout.BeginHorizontal();
        GUILayout.Label("Secret Key", h1LabelStyle);
        GUILayout.EndHorizontal();

        GUILayout.Space(10f);

        GUILayout.BeginHorizontal();
        GUILayout.Label("Your secret key:", nameLabelStyle);
        _plugin.AccountSecretKey = GUILayout.TextField(_plugin.AccountSecretKey, textFieldStyle);
        GUILayout.EndHorizontal();

        GUILayout.Space(5f);

        GUILayout.BeginHorizontal();
        GUILayout.FlexibleSpace();
        if (GUILayout.Button("Set account secret key", buttonSetSecretKeyStyle))
        {
            this._plugin.ClearAccountData();
            this._plugin.Reconnect();
        }
        GUILayout.FlexibleSpace();
        GUILayout.EndHorizontal();

        GUILayout.EndVertical();
    }

    /// <summary>
    /// Get plugin from target.
    /// </summary>
    /// <returns>AsylumPlugin</returns>
    private AsylumPlugin GetPlugin()
    {
        return (AsylumPlugin)this.target;
    }
}

#endif

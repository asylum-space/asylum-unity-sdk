using Cysharp.Threading.Tasks;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;
using UnityEngine.Networking;

namespace Asylum
{
    #region ASSEMBLED CLASSES

    [Serializable]
    public class AsylumAssembledBlueprint
    {
        public string id;
        public string name;
        public string max;
        public string description;
        public string issuer;
        public int nftCount;
        public AsylumInterpretation[] interpretations;

        private string metadata;

        public AsylumAssembledBlueprint()
        {
            this.id = null;
            this.name = null;
            this.max = null;
            this.metadata = null;
            this.issuer = null;
            this.nftCount = -1;
            this.interpretations = null;
            this.metadata = null;
            this.description = null;
        }

        public AsylumAssembledBlueprint(string _id, string _name, string _max, string _metadata, string _issuer, int _nftCount, AsylumInterpretation[] _interpretations,string description)
        {
            this.id = _id;
            this.name = _name;
            this.max = _max;
            this.metadata = _metadata;
            this.issuer = _issuer;
            this.nftCount = _nftCount;
            this.interpretations = _interpretations;
            this.description = description;
        }

        public AsylumAssembledBlueprint(AsylumBlueprint blueprint)
        {
            this.id = blueprint.id;
            this.name = blueprint.name;
            this.max = blueprint.max;
            this.metadata = blueprint.metadata;
            this.issuer = blueprint.issuer;
            this.nftCount = blueprint.nftCount;
            this.interpretations = blueprint.interpretations;
            this.description = null;
        }

        public void SetMetadata(AsylumBlueprintMetadata metadata)
        {
            this.description = metadata.description;
        }

        public async UniTaskVoid LoadBlueprintMetaAsync(string mainPathForDownloading)
        {
            if (mainPathForDownloading != null)
            {
                if (this.metadata != null)
                {
                    string metadataPath = $"{mainPathForDownloading}{this.metadata}";
                    AsylumBlueprintMetadata downloadedMeta = await AsylumAssemblerUtils.LoadMetaAsync<AsylumBlueprintMetadata>(metadataPath);

                    if (downloadedMeta != null)
                    {
                        this.SetMetadata(downloadedMeta);
                    }
                }
            }
        }


        //TODO: convert to generic

        public static AsylumAssembledBlueprint[] ConvertToAssembledBlueprintsArray(AsylumBlueprint[] blueprints)
        {
            List<AsylumAssembledBlueprint> result = new List<AsylumAssembledBlueprint>();

            foreach (var blueprint in blueprints)
            {
                if (blueprint != null)
                    result.Add(new AsylumAssembledBlueprint(blueprint));
            }

            return result.ToArray();
        }

        public static AsylumAssembledBlueprint[] ConvertToAssembledBlueprintsArray(List<AsylumBlueprint> blueprints)
        {
            List<AsylumAssembledBlueprint> result = new List<AsylumAssembledBlueprint>();

            foreach (var blueprint in blueprints)
            {
                if (blueprint != null)
                    result.Add(new AsylumAssembledBlueprint(blueprint));
            }

            return result.ToArray();
        }

        public static List<AsylumAssembledBlueprint> ConvertToAssembledBlueprintsList(AsylumBlueprint[] blueprints)
        {
            List<AsylumAssembledBlueprint> result = new List<AsylumAssembledBlueprint>();

            foreach (var blueprint in blueprints)
            {
                if (blueprint != null)
                    result.Add(new AsylumAssembledBlueprint(blueprint));
            }

            return result;
        }

        public static List<AsylumAssembledBlueprint> ConvertToAssembledBlueprintsList(List<AsylumBlueprint> blueprints)
        {
            List<AsylumAssembledBlueprint> result = new List<AsylumAssembledBlueprint>();

            foreach (var blueprint in blueprints)
            {
                if (blueprint != null)
                    result.Add(new AsylumAssembledBlueprint(blueprint));
            }

            return result;
        }

        public static async UniTaskVoid LoadBlueprintsMetaAsync(List<AsylumAssembledBlueprint> blueprints, string mainPathForDownloading)
        {
            if (mainPathForDownloading != null)
            {
                foreach (var blueprint in blueprints)
                {
                    if (blueprint != null)
                    {
                        if (blueprint.metadata != null)
                        {
                            string metadataPath = $"{mainPathForDownloading}{blueprint.metadata}";
                            AsylumBlueprintMetadata downloadedMeta = await AsylumAssemblerUtils.LoadMetaAsync<AsylumBlueprintMetadata>(metadataPath);

                            if (downloadedMeta != null)
                            {
                                blueprint.SetMetadata(downloadedMeta);
                            }
                        }
                    }
                }
            }
        }

        public static async UniTaskVoid LoadBlueprintsMetaAsync(AsylumAssembledBlueprint[] blueprints, string mainPathForDownloading)
        {
            if (mainPathForDownloading != null)
            {
                foreach (var blueprint in blueprints)
                {
                    if (blueprint != null)
                    {
                        if (blueprint.metadata != null)
                        {
                            string metadataPath = $"{mainPathForDownloading}{blueprint.metadata}";
                            AsylumBlueprintMetadata downloadedMeta = await AsylumAssemblerUtils.LoadMetaAsync<AsylumBlueprintMetadata>(metadataPath);

                            if (downloadedMeta != null)
                            {
                                blueprint.SetMetadata(downloadedMeta);
                            }
                        }
                    }
                }
            }
        }

        public override bool Equals(object obj)
        {
            return obj is AsylumAssembledBlueprint blueprint &&
                   this.id == blueprint.id &&
                   this.name == blueprint.name &&
                   this.max == blueprint.max &&
                   this.description == blueprint.description &&
                   this.issuer == blueprint.issuer &&
                   this.nftCount == blueprint.nftCount &&
                   this.interpretations == blueprint.interpretations &&
                   this.metadata == blueprint.metadata;
        }

        public override int GetHashCode()
        {
            return HashCode.Combine(id, name, max, description, issuer, nftCount,interpretations,metadata);
        }


    }

    [Serializable]
    public class AsylumAssembledTag
    {
        public string tag;
        public string id;
        public string description;
        public AsylumTagMetadataExtensions metadataExtensions;

        private string metadata;

        public AsylumAssembledTag()
        {
            this.tag = null;
            this.id = null;
            this.description = null;
            this.metadataExtensions = null;
            this.metadata = null;
        }

        public AsylumAssembledTag(string tag, string id, string description, AsylumTagMetadataExtensions metadataExtensions, string metadata)
        {
            this.tag = tag;
            this.id = id;
            this.description = description;
            this.metadataExtensions = metadataExtensions;
            this.metadata = metadata;
        }

        public AsylumAssembledTag(AsylumTag tag)
        {
            this.tag = tag.tag;
            this.metadata = tag.metadata;
            this.id = null;
            this.description = null;
            this.metadataExtensions = null;
        }

        public void SetMetadata(AsylumTagMetadata metadata)
        {
            this.id = metadata.id;
            this.description = metadata.description;
            this.metadataExtensions = metadata.metadataExtensions;
        }

        public async UniTaskVoid LoadTagMetaAsync(string mainPathForDownloading)
        {
            if (mainPathForDownloading != null)
            {
                if (this.metadata != null)
                {
                    string metadataPath = $"{mainPathForDownloading}{this.metadata}";
                    AsylumTagMetadata downloadedMeta = await AsylumAssemblerUtils.LoadMetaAsync<AsylumTagMetadata>(metadataPath);

                    if (downloadedMeta != null)
                    {
                        this.SetMetadata(downloadedMeta);
                    }
                }
            }
        }


        //TODO: convert to generic

        public static AsylumAssembledTag[] ConvertToAssembledTagsArray(AsylumTag[] tags)
        {
            List<AsylumAssembledTag> result = new List<AsylumAssembledTag>();

            foreach (var tag in tags)
            {
                if (tag != null)
                    result.Add(new AsylumAssembledTag(tag));
            }

            return result.ToArray();
        }

        public static AsylumAssembledTag[] ConvertToAssembledTagsArray(List<AsylumTag> tags)
        {
            List<AsylumAssembledTag> result = new List<AsylumAssembledTag>();

            foreach (var tag in tags)
            {
                if (tag != null)
                    result.Add(new AsylumAssembledTag(tag));
            }

            return result.ToArray();
        }

        public static List<AsylumAssembledTag> ConvertToAssembledTagsList(AsylumTag[] tags)
        {
            List<AsylumAssembledTag> result = new List<AsylumAssembledTag>();

            foreach (var tag in tags)
            {
                if (tag != null)
                    result.Add(new AsylumAssembledTag(tag));
            }

            return result;
        }

        public static List<AsylumAssembledTag> ConvertToAssembledTagsList(List<AsylumTag> tags)
        {
            List<AsylumAssembledTag> result = new List<AsylumAssembledTag>();

            foreach (var tag in tags)
            {
                if (tag != null)
                    result.Add(new AsylumAssembledTag(tag));
            }

            return result;
        }

        public static async UniTaskVoid LoadTagsMetaAsync(List<AsylumAssembledTag> tags, string mainPathForDownloading)
        {
            if (mainPathForDownloading != null)
            {
                foreach (var tag in tags)
                {
                    if (tag != null)
                    {
                        if (tag.metadata != null)
                        {
                            string metadataPath = $"{mainPathForDownloading}{tag.metadata}";
                            AsylumTagMetadata downloadedMeta = await AsylumAssemblerUtils.LoadMetaAsync<AsylumTagMetadata>(metadataPath);

                            if (downloadedMeta != null)
                            {
                                tag.SetMetadata(downloadedMeta);
                            }
                        }
                    }
                }
            }
        }

        public static async UniTaskVoid LoadTagsMetaAsync(AsylumAssembledTag[] tags, string mainPathForDownloading)
        {
            if (mainPathForDownloading != null)
            {
                foreach (var tag in tags)
                {
                    if (tag != null)
                    {
                        if (tag.metadata != null)
                        {
                            string metadataPath = $"{mainPathForDownloading}{tag.metadata}";
                            AsylumTagMetadata downloadedMeta = await AsylumAssemblerUtils.LoadMetaAsync<AsylumTagMetadata>(metadataPath);

                            if (downloadedMeta != null)
                            {
                                tag.SetMetadata(downloadedMeta);
                            }
                        }
                    }
                }
            }
        }

        public override bool Equals(object obj)
        {
            return obj is AsylumAssembledTag tag &&
                   this.id == tag.id &&
                   this.description == tag.description &&
                   this.metadataExtensions == tag.metadataExtensions &&
                   this.metadata == tag.metadata;
        }

        public override int GetHashCode()
        {
            return HashCode.Combine(id, description, metadataExtensions, metadata);
        }

    }

    #endregion

    #region PARSING CLASSES
    //MB rename classes used in the data parsing to Wrapped* f.e. SpaceMetadata => WrappedSpaceMetadata

    [Serializable]
    public class SpaceMetadata
    {
        public string id;
        public string title;
        public string img;
        public string genre;
        public string shortDescription;
        public string description;
        public string[] gallery;
        public Review[] reviews;

        public SpaceMetadata()
        {

        }

        public SpaceMetadata(string _id,string _title,string _img,string _genre,string _shortDescription,string _description,string[] _gallery,Review[] _reviews)
        {
            this.id = _id;
            this.title = _title;
            this.img = _img;
            this.genre = _genre;
            this.shortDescription = _shortDescription;
            this.description = _description;
            this.gallery = _gallery;
            this.reviews = _reviews;
        }  
    }      

    [Serializable]
    public class Review
    {
        public string id;
        public string name;
        public string text;
        public string date;
        public int rating;
        public string address;

        public Review()
        {

        }

        public Review(string _id,string _name,string _text, string _date,int _rating,string _address)
        {
            this.id = _id;
            this.name = _name;
            this.text = _text;
            this.date = _date;
            this.rating = _rating;
            this.address = _address;
        }

        public override string ToString()
        {
            string result = String.Format("Id : {0}\nName : {1}\nText: {2}\nDate : {3}\nRating : {4}\nAddress : {5}\n",
                                                this.id, this.name, this.text, this.date, this.rating, this.address);
            return result;
        }

    }

    [Serializable]
    public class AsylumBlueprint
    {
        public string id;
        public string name;
        public string max;
        public string metadata;
        public string issuer;
        public int nftCount;
        public AsylumInterpretation[] interpretations;

        public AsylumBlueprint(string _id, string _name, string _max, string _metadata, string _issuer, int _nftCount, AsylumInterpretation[] _interpretations)
        {
            this.id = _id;
            this.name = _name;
            this.max = _max;
            this.metadata = _metadata;
            this.issuer = _issuer;
            this.nftCount = _nftCount;
            this.interpretations = _interpretations;
        }

        public AsylumBlueprint()
        {

        }

        public override string ToString()
        {
            string result = String.Format("Id : {0}\nName : {1}\nMax: {2}\nMetadata : {3}\nIssuer : {4}\nNftCount : {5}\n", 
                                                id, name, max, metadata, issuer, nftCount);

            result += String.Join<AsylumInterpretation>("\n", interpretations);
            return result;
        }
    }

    [Serializable]
    public class AsylumBlueprintMetadata
    {
        public string description;

        public AsylumBlueprintMetadata()
        {

        }

        public AsylumBlueprintMetadata(string _description)
        {
            this.description = _description;
        }
    }

    [Serializable]
    public class Owner
    {
        public string AccountId;

        public Owner()
        {

        }
        public Owner(string owner)
        {
            AccountId = owner;
        }

        public override string ToString()
        {
            return AccountId;
        }

        public override bool Equals(object obj)
        {
            return obj is Owner owner &&
                    AccountId == owner.AccountId;
        }

        public override int GetHashCode()
        {
            return HashCode.Combine(AccountId);
        }
    }

    [Serializable]
    public class UserInfo
    {
        public User User;

        public UserInfo()
        {

        }

        public UserInfo(User _user)
        {
            this.User = _user;
        }

        public override string ToString()
        {
            string result = String.Format("User : {0}\n",
                this.User);

            return result;
        }
    }

    [Serializable]
    public class User
    {
        public string name;
        public string address;

        public User()
        {

        }

        public User(string _name,string _address)
        {
            this.name = _name;
            this.address = _address;
        }

        public override string ToString()
        {
            string result = String.Format("Name : {0}\nAddress : {1}\n",
                this.name,this.address);

            return result;
        }
    }

    /// <summary>
    /// Represent actual NFT item
    /// </summary>
    [Serializable]
    public class AsylumItem
    {
        public string id;
        public string blueprintId;
        public Owner owner;
        public string recipient;
        public string royalty;
        public string metadata;
        public bool equipped;
        public AsylumInterpretation[] interpretations;

        public AsylumItem(string _blueprintId, string _itemId, string _owner, string _royalty, string _metadata, bool _equipped, AsylumInterpretation[] _interpretations)
        {
            id = _itemId;
            blueprintId = _blueprintId;
            owner = new Owner(_owner);
            royalty = _royalty;
            metadata = _metadata;
            equipped = _equipped;
            interpretations = _interpretations;
        }

        public AsylumItem()
        {
        }

        public override string ToString()
        {
            string result = String.Format("BlueprintId : {0}\nItemId : {1}\nOwner : {2}\nRoyalty : {3}\nMetadata : {4}\nEquipped: {5}\n", 
                blueprintId, id, owner, royalty, metadata, equipped);

            result += String.Join<AsylumInterpretation>("\n", interpretations);
            return result;
        }

        public override bool Equals(object obj)
        {
            return obj is AsylumItem item &&
                   id == item.id &&
                   blueprintId == item.blueprintId &&
                   owner.AccountId == item.owner.AccountId &&
                   royalty == item.royalty &&
                   metadata == item.metadata &&
                   equipped == item.equipped;
        }

        public override int GetHashCode()
        {
            return HashCode.Combine(id,blueprintId,owner,royalty,metadata,equipped);
        }
    }

    [Serializable]
    public class AsylumItemMetadata
    {
        public string description;
        public string name;

        public AsylumItemMetadata()
        {

        }

        public AsylumItemMetadata(string _description, string _name)
        {
            this.description = _description;
            this.name = _name;
        }

        public override string ToString()
        {
            string result = String.Format("Description : {0}\nItemName : {1}\n",
                description,name);
            return result;
        }

        public override bool Equals(object obj)
        {
            return obj is AsylumItemMetadata meta &&
                   this.description == meta.description &&
                   this.name == meta.name;
        }

        public override int GetHashCode()
        {
            return HashCode.Combine(description,name);
        }
    }

    [Serializable]
    public class InterpretationInfo
    {
        public string src;
        public string metadata;

        public InterpretationInfo()
        {

        }

        public InterpretationInfo(string _src, string _metadata)
        {
            src = _src;
            metadata = _metadata;
        }

        public override string ToString()
        {
            return String.Format("Src : {0}\nMetadata : {1}", src, metadata);
        }

        public override bool Equals(object obj)
        {
            return obj is InterpretationInfo info &&
                this.src == info.src &&
                this.metadata == info.metadata;
        }

        public override int GetHashCode()
        {
            return HashCode.Combine(src,metadata);
        }

    }

    [Serializable]
    public class AsylumInterpretation
    {
        public string id;
        public string[] tags;
        public InterpretationInfo interpretationInfo;

        public AsylumInterpretation(string _id, string[] _tags, InterpretationInfo _interpretationInfo)
        {
            id = _id;
            tags = _tags;
            interpretationInfo = _interpretationInfo;
        }

        public override string ToString()
        {
            string tags_string = "[" + String.Join(", ", tags) + "]";
            string result = String.Format("Id : {0}\nTags : {1}\n", id, tags_string); 
            result += interpretationInfo.ToString();
            return result;
        }

        public override bool Equals(object obj)
        {
            if (obj is AsylumInterpretation interpretation)
            {

                for (int i = 0; i < this.tags.Length; i++)
                {
                    if (this.tags[i] != interpretation.tags[i])
                        return false;
                }
                return this.id == interpretation.id &&
                    this.interpretationInfo.Equals(interpretation.interpretationInfo);
            }
            else
            {
                return false;
            }
        }

        public override int GetHashCode()
        {
            return HashCode.Combine(id,tags,interpretationInfo);
        }

    }

    [Serializable]
    public class AsylumTag 
    {
        public string tag;
        public string metadata;

        public AsylumTag(string _tag, string _metadata) {
            tag = _tag;
            metadata = _metadata;
        }

        public override string ToString() {
            return String.Format("Tag: {0}\n Metadata: {1}", tag, metadata);
        }
    }

    [Serializable]
    public class AsylumTagMetadata
    {
        public string id;
        public string description;
        public AsylumTagMetadataExtensions metadataExtensions;


        AsylumTagMetadata()
        {

        }

        AsylumTagMetadata(string _id,string _description,AsylumTagMetadataExtensions _metadataExtensions)
        {
            this.id = _id;
            this.description = _description;
            this.metadataExtensions = _metadataExtensions;
        }

        public override string ToString()
        {
            string result = String.Format("Id : {0}\nDescription : {1}\n",
                id,description);
            return result;
        }
    }

    [Serializable]
    public class AsylumTagMetadataExtensions
    {
        public string[] fields;

        public override string ToString()
        {
            string fields_string = "[" + String.Join(", ", fields) + "]";
            string result = String.Format("Fields : {0}\n", fields_string);
            return result;
        }
        public override bool Equals(object obj)
        {
            if(obj is AsylumTagMetadataExtensions metaExtensions)
            {
                for (int i = 0; i < this.fields.Length; i++)
                {
                    if (this.fields[i] != metaExtensions.fields[i])
                        return false;
                }
                return true;
            }
            else
            {
                return false;
            }
        }

        public override int GetHashCode()
        {
            return HashCode.Combine(fields);
        }
    }

    #endregion

    /// <summary>
    /// Define full "real" id for interpretation
    /// Which contains <see cref="ItemCombineID"/> as <see cref="AsylumItem"/> Id and interpretation id
    /// </summary>
    public class InterpretationCombineID
    {
        public ItemCombineID itemCombineID;
        public string interpretationID; //Can dublicate in differnt blueprints

        public override bool Equals(object obj)
        {
            return obj is InterpretationCombineID iD &&
                   itemCombineID ==  iD.itemCombineID &&
                   interpretationID == iD.interpretationID;
        }

        public override int GetHashCode()
        {
            return HashCode.Combine(itemCombineID, interpretationID);
        }

        public override string ToString()
        {
            return $"{itemCombineID.ToString()} & {interpretationID}";
        }
    }

    /// <summary>
    /// Define full "real" item id
    /// Contains blupring id and item id
    /// </summary>
    [Serializable]
    public class ItemCombineID
    {
        public string blueprintID;
        
        /// <summary>
        /// ID of the actual NFT item. Can dublicate in the different plueprints
        /// </summary>
        public string itemID;

        public override bool Equals(object obj)
        {
            return obj is ItemCombineID iD &&
                   blueprintID == iD.blueprintID &&
                   itemID == iD.itemID;
        }

        public override int GetHashCode()
        {
            return HashCode.Combine(blueprintID, itemID);
        }

        public override string ToString()
        {
            return $"{blueprintID}_{itemID}";
        }

        public ItemCombineID(string tID = "-1", string iID = "-1")
        {
            blueprintID = tID;
            itemID = iID;
        }

        public static bool operator == (ItemCombineID first, ItemCombineID second)
        {
            if(first.itemID == second.itemID
                && first.blueprintID == second.blueprintID)
            {
                return true;
            }

            return false;
        }

        public static bool operator != (ItemCombineID first, ItemCombineID second)
        {
            if(first.itemID != second.itemID
                || first.blueprintID != second.blueprintID)
            {
                return true;
            }

            return false;
        }
    }

    /// <summary>
    /// Service entity for storing data as object and it's type
    /// </summary>
    public class TypedData
    {
        public Type dataType;
        public object dataObject;

        public TypedData(Type type, object obj)
        {
            dataType = type;
            dataObject = obj;
        }
    }
}

#if !UNITY_WEBGL
using Ajuna.NetApi.Model.Types;
#endif

using Cysharp.Threading.Tasks;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Runtime.InteropServices;
using System.Threading.Tasks;
using UnityEngine;

namespace Asylum
{
    /// <summary>
    /// Controller for chain-unity connection
    /// </summary>
    public class AsylumDigitalObjectsController : MonoBehaviour
    {
        public string AccountSecretPhrase;
        public Action OnPauseRequestedAction;
        public Action<UserInfo> OnUserInfoReceived;
        public Action<SpaceMetadata> OnSpaceMetadataReceived;
        public Action<AsylumItemAsyncHandler> OnItemWasLoaded;
        public List<AsylumItemAsyncHandler> ItemHandlers => _itemHandlers;
        public UserInfo UserInfo => _userInfo;
        public SpaceMetadata SpaceMetadata => _spaceMetadata;
        
        [SerializeField] private string path = @"http://127.0.0.1:8080/ipfs/";
        private IAsylumDigitalObjectsClient _digitalObjectsClient;
        private List<AsylumItemAsyncHandler> _itemHandlers = new();
        private UserInfo _userInfo;
        private SpaceMetadata _spaceMetadata;

        public bool isAutoRequestItemsOnStart = true;

        public AsylumItemAsyncHandler GetItemHandler(AsylumItem asylumItem) => 
            _itemHandlers.Find(item => item.AsylumItem.id == asylumItem.id && item.AsylumItem.blueprintId == asylumItem.blueprintId);

        public AsylumItemAsyncHandler GetItemHandler(string blueprintID, string itemID) =>
            _itemHandlers.Find(item => item.AsylumItem.blueprintId == blueprintID && item.AsylumItem.id == itemID);

        public List<AsylumItemAsyncHandler> GetItemHandlers(string blueprintID) =>
            _itemHandlers.FindAll(item => item.AsylumItem.blueprintId == blueprintID);
        //MB implement posibility to get item by tag?

        private void Awake()
        {
#if UNITY_EDITOR

            Debug.unityLogger.logEnabled = true;

#else

            Debug.unityLogger.logEnabled = false;

#endif

            DontDestroyOnLoad(this);
        }

        void Start()
        {
#if UNITY_EDITOR

            //MB create EditorClient?

            _digitalObjectsClient = new StandaloneClient();
            (_digitalObjectsClient as StandaloneClient).SetSecretPhrase(this.AccountSecretPhrase);

#elif UNITY_WEBGL
         
            // Here we have our ReactClient

            // Cause React lib working through GameObject.SendMessage logic, 
            // we actually need to have our React* script on the scene 

            _digitalObjectsClient = FindObjectOfType<ReactClient>();

            // MB it's a good idea to use singletone with static instance field
            // or|and a-la proxy which will work with ScriptableObject/"Regular" c# class
#else

            //StandaloneClient
            
            _digitalObjectsClient = new StandaloneClient();
            (_digitalObjectsClient as StandaloneClient).SetSecretPhrase(this.AccountSecretPhrase);
#endif

            ConnectToChain();

            if(isAutoRequestItemsOnStart)
                RequestAsylumUserItems();
        }

        private void OnDestroy()
        {
            _digitalObjectsClient.OnDisconnected();
        }

        //Mb put this to the single #if state?
        public void RequestExit()
        {
#if UNITY_EDITOR

            Application.Quit();

#elif UNITY_WEBGL

            (_digitalObjectsClient as ReactClient).SendSpaceCloseRequest();
#else

            Application.Quit();
#endif
        }

        public void ConnectToChain()
        {
            this.Subscribe();
            _digitalObjectsClient.OnConnected();
        }

        public void DisconnectFromChain()
        {
            _digitalObjectsClient.OnDisconnected();
        }

        public void RequestAsylumUserInfo()
        {
            _digitalObjectsClient.SendUserInfoRequest();
        }
        
        public void RequestAsylumSpaceMetadata()
        {
            _digitalObjectsClient.SendSpaceMetadataRequest();
        }

        public void RequestAsylumUserItems()
        {
            _digitalObjectsClient.SendUserItemsRequest();
        }

        public void RequestMintAsylumItem(int blueprintId, AsylumItemMetadata metadata)
        {
            this._digitalObjectsClient.SendMintItemRequest(blueprintId, metadata);
        }

        private void UserInfoReceive(UserInfo userInfo)
        {
            this._userInfo = userInfo;

            OnUserInfoReceived?.Invoke(_userInfo);
        }

        private void SpaceMetadataReceive(SpaceMetadata spaceMetadata)
        {
            this._spaceMetadata = spaceMetadata;

            OnSpaceMetadataReceived?.Invoke(_spaceMetadata);
        }

        private void Subscribe()
        {
            if (_digitalObjectsClient != null)
            {
                _digitalObjectsClient.OnUserItemsRecieved += StartAsyncItemLoading;
                _digitalObjectsClient.OnMintedItemsRecieved += StartAsyncItemLoading;
                _digitalObjectsClient.OnSpaceMetadataRecieved += SpaceMetadataReceive;
                _digitalObjectsClient.OnUserInfoRecieved += UserInfoReceive;

                if (this._digitalObjectsClient is ReactClient reactClient)
                {
                    reactClient.OnPauseRequestedAction += ()=>this.OnPauseRequestedAction?.Invoke();

                    UnityEngine.Debug.LogWarning($"_digitalObjectsClient is ReactClient! Subscribe OnPauseRequestAction!");
                }
                else
                {
                    UnityEngine.Debug.LogWarning($"_digitalObjectsClient is not ReactClient! Can't subscribe OnPauseRequestAction!");
                }
            }
            else
            {
                UnityEngine.Debug.LogError($"_digitalObjectsClient is null! Can't subscribe!");
            }
        }

        private void UnSubscribe()
        {
            if (_digitalObjectsClient != null)
            {
                _digitalObjectsClient.OnUserItemsRecieved -= StartAsyncItemLoading;
                _digitalObjectsClient.OnMintedItemsRecieved -= StartAsyncItemLoading;
            }
            else
            {
                UnityEngine.Debug.LogError($"_digitalObjectsClient is null! Can't UN Subscribe!");
            }
        }

        private void StartAsyncItemLoading(AsylumItem[] items)
        {
            UnityEngine.Debug.LogWarning($"Controller : StartAsyncItemLoading Items count {items.Length}");

            foreach (var item in items)
            {
                StartAsyncLoadSingleItem(new AsylumItemAsyncHandler(item, path)).Forget();
            }

            UnityEngine.Debug.LogWarning($"Controller : StartAsyncItemLoading all loading started");
        }

        public void Mocked_StartAsyncItemLoad(AsylumItem item)
        {
            StartAsyncItemLoading(new[] { item });
        }

        public void Mocked_UserInfoReceive(UserInfo info)
        {
            UserInfoReceive(info);
        }

        public void Mocked_SpaceMetadataReceive(SpaceMetadata metadata)
        {
            SpaceMetadataReceive(metadata);
        }

        private async UniTaskVoid StartAsyncLoadSingleItem(AsylumItemAsyncHandler itemHandler)
        {
            var result = await itemHandler.LoadAllItemDataAsync();

            if (result)
            {
                await UniTask.SwitchToMainThread(); // Just for sure

                if (_itemHandlers == null)
                {
                    _itemHandlers = new();
                }

                _itemHandlers.Add(itemHandler);

                OnItemWasLoaded?.Invoke(itemHandler);

                UnityEngine.Debug.LogWarning($"Controller : item{itemHandler.Name} id:{itemHandler.AsylumItem.id}|bp{itemHandler.AsylumItem.blueprintId}" +
                    $" loaded successfully. Loaded Items handlers count {_itemHandlers.Count}");
            }
            else
            {
                UnityEngine.Debug.LogWarning($"Controller : item{itemHandler?.Name ?? "NO_NAME"} id:{itemHandler.AsylumItem.id}|bp{itemHandler.AsylumItem.blueprintId} load FAILED");
            }
        }
    }
}

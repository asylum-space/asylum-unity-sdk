﻿#if !UNITY_WEBGL || UNITY_EDITOR

using Cysharp.Threading.Tasks;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Threading.Tasks;
using System.Threading;
using System.Text;
using System.Security;
using Ipfs.Http;
using Ipfs;
using Newtonsoft.Json.Linq;

using Ajuna.NetApi.Model.Types;
using AjunaExample.NetApiExt.Generated;
using Ajuna.NetApi;
using Ajuna.NetApi.Model.Types.Primitive;
using AjunaExample.NetApiExt.Generated.Model.rmrk_traits.nft;
using AjunaExample.NetApiExt.Generated.Model.asylum_traits.types;
using AjunaExample.NetApiExt.Generated.Model.rmrk_traits.resource;
using Ajuna.NetApi.Model.Meta;
using AjunaExample.NetApiExt.Generated.Model.sp_core.crypto;
using AjunaExample.NetApiExt.Generated.Model.rmrk_traits.collection;
using AjunaExample.NetApiExt.Generated.Model.frame_support.storage.bounded_vec;
using Ajuna.NetApi.Model.Extrinsics;
using AjunaExample.NetApiExt.Generated.Storage;
using Ajuna.NetApi.Model.Rpc;
using Ajuna.NetApi.Model.Types.Base;

namespace Asylum
{
    //MB use regular Task instead of UniTask

    //TODO : Refactor, clean up, optimize

    /// <summary>
    /// Implementation of the <see cref="IAsylumDigitalObjectsClient"/> interface for the chain-unity connection by Ajuna
    /// </summary>
    public class StandaloneClient : IAsylumDigitalObjectsClient
#if UNITY_EDITOR
        , IAsylumEditorClient
#endif
    {
        /// <summary>
        /// Client that connects and interacts with the chain.
        /// </summary>
        public SubstrateClientExt Client => _client;
        public Action<AsylumItem[]> OnUserItemsRecieved { get; set; }
        public Action<UserInfo> OnUserInfoRecieved { get; set; }
        public Action<SpaceMetadata> OnSpaceMetadataRecieved { get; set; }
        public Action<AsylumItem[]> OnMintedItemsRecieved { get; set; }
        public List<AsylumItem> AsylumItems { get; set; }

        /// <summary>
        /// Task which monitors the connection status to the chain.
        /// </summary>
        private Task _connectTask;
        /// <summary>
        /// Client that connects and interacts with the chain.
        /// </summary>
        private SubstrateClientExt _client;
        /// <summary>
        /// URL for connect to chain.
        /// </summary>
        private const string NodeUrl = "ws://127.0.0.1:9944";
        private List<Task<string>> _mintingSubscriptionIds = new();
        private Account _account;
        private IpfsClient _ipfs;

        //mb put this into ctor.
        public void SetSecretPhrase(string secretPhrase)
        {
            this._account = Mnemonic.GetAccountFromMnemonic(secretPhrase, "", KeyType.Sr25519);
            if (this._account != null)
                Debug.LogWarning($"Account : {this._account.Value}");
            else
                Debug.LogError("In StandaloneClient:SetSecretPhrasse - Can`t get user public key.");
        }

        public void OnConnected()
        {
            CreateIpfsClientAndConnect();
            ConnectToChainAsync().Forget();
        }

        public void OnDisconnected()
        {
            CloseConnectionToChainAsync().Forget();
        }

        public void SendUserItemsRequest()
        {
            //1
            //ReturnUserItemsAsync().Forget();

            //OR 2
            if(this._account == null)
                (AsyncLoaderCallbackCaller(RequestUserItemsAsync(), (items)=> { this.AsylumItems = new(items); OnUserItemsRecieved?.Invoke(items); })).Forget();
            else
                (AsyncLoaderCallbackCaller(RequestUserItemsAsync(this._account), (items) => { this.AsylumItems = new(items); OnUserItemsRecieved?.Invoke(items); })).Forget();
        }

        public void SendUserInfoRequest()
        {
            //TODO :Сan't implement because we don't have this information stored on the blockchain.
            //This information used to be provided by React.
            //Implements as soon as the necessary information to work with is available and stored on the blockchain.

            UnityEngine.Debug.LogError($"METHOD StandaloneClient:SendUserInfoRequest isn't implemented yet");
        }

        public void SendSpaceMetadataRequest()
        {
            //TODO :Сan't implement because we don't have this information stored on the blockchain.
            //This information used to be provided by React.
            //Implements as soon as the necessary information to work with is available and stored on the blockchain.

            UnityEngine.Debug.LogError($"METHOD StandaloneClient:SendSpaceMetadataRequest isn't implemented yet");
        }

        public void SendMintItemRequest(int blueprintId, AsylumItemMetadata metadata)
        {
            if (_account != null)
            {
                RequestMintItem(_account, blueprintId, metadata).Forget();
            }
            else
            {
                UnityEngine.Debug.LogError($"Can't mint item cause account was not setuped!");
            }
            
        }

        private void CreateIpfsClientAndConnect()
        {
            _ipfs = new IpfsClient();
            Task.Run(()=>_ipfs.IdAsync()).ContinueWith((result)=>Debug.LogWarning($"IPFS ID - {result.Result.Id}"));
        }

        private async UniTaskVoid ReturnUserItemsAsync()
        {
            var items = await RequestUserItemsAsync();

            this.AsylumItems = new List<AsylumItem>(items);
            OnUserItemsRecieved?.Invoke(items);
        }

        private async UniTaskVoid AsyncLoaderCallbackCaller<T>(UniTask<T> asyncTask, Action callback)
        {
            T data = await asyncTask;

            callback();
        }

        private async UniTaskVoid AsyncLoaderCallbackCaller<T>(UniTask<T> asyncTask, Action<T> onEndCallback)
        {
            T data = await asyncTask;

            onEndCallback?.Invoke(data);
        }

        /// <summary>
        /// Connecting to the chain. Waiting for connection.
        /// </summary>
        /// <returns>Connection status.</returns>
        private async UniTask<bool> ConnectToChainAsync()
        {
            if (this._client != null && this._client.IsConnected)
                return true;

            if (this._client == null)
            {
                this._client = new SubstrateClientExt(new Uri(NodeUrl));
            }

            if (this._connectTask == null)
            {
                this._connectTask = this._client.ConnectAsync();
            }

            await this._connectTask;

            if (this._connectTask!=null && (!this._connectTask.IsCompleted || this._connectTask.IsFaulted || this._connectTask.IsCanceled))
            {
                this._connectTask = null;

                return false;
            }
            else if (this._client.IsConnected)
            {
                this._connectTask = null;

                Debug.LogWarning("Connected to blockchain standalone.");
                return true;
            }

            return false;

        }

        /// <summary>
        /// Disconnecting from the chain.
        /// </summary>
        /// <returns>Task</returns>
        private async UniTaskVoid CloseConnectionToChainAsync()
        {
            if (this._client != null)
            {
                if (this._client.IsConnected)
                {
                    await this._client.CloseAsync();
                }
            }
        }

        private async UniTask<AsylumItem[]> RequestUserItemsAsync()
        {
            if (!await this.ConnectToChainAsync())
                return null;

            List<AsylumItem> items = await QueryItems();

            if (items != null)
            {
                this.AsylumItems = items;
                return items.ToArray();
            }

            return null;
        }

        private async UniTask<AsylumItem[]> RequestUserItemsAsync(Account owner)
        {
            if (!await this.ConnectToChainAsync())
                return null;

            List<AsylumItem> items = await QueryItems(owner.Value);

            if (items != null)
            {
                this.AsylumItems = items;
                return items.ToArray();
            }

            return null;
        }

        private async UniTask<AsylumItem[]> RequestUserItemsAsync(string ownerPublicKey_ss58)
        {
            if (!await this.ConnectToChainAsync())
                return null;

            List<AsylumItem> items = await QueryItems(ownerPublicKey_ss58);

            if (items != null)
            {
                this.AsylumItems = items;
                return items.ToArray();
            }

            return null;
        }

        //TODO : Optimize \/
        private async UniTask<List<AsylumItem>> QueryItems()
        {

            //UniTask.WhenAll
            JArray items = await _client.State.GetPairsAsync(RequestGenerator.GetStorageKeyBytesHash("RmrkCore", "Nfts"));
            JArray itemsInterpretations = await _client.State.GetPairsAsync(RequestGenerator.GetStorageKeyBytesHash("RmrkCore", "Resources"));
            JArray itemsInterpretationsTags = await _client.State.GetPairsAsync(RequestGenerator.GetStorageKeyBytesHash("AsylumBlueprints", "ItemInterpretationTags"));
            
            return AsylumStandaloneUtils.DecodeItems(items, itemsInterpretations, itemsInterpretationsTags);
        }
      
        private async UniTask<List<AsylumItem>> QueryItems(string ownerId)
        {
            return (await QueryItems()).FindAll(element => element.owner.AccountId == ownerId);
        }

        private async UniTask<List<AsylumItem>> QueryItems(Owner ownerId)
        {
            return await QueryItems(ownerId.AccountId);
        }

        private async UniTask<List<AsylumItem>> QueryItems(AccountId32 ownerId)
        {
            string public_key_ss58 = Ajuna.NetApi.Utils.GetAddressFrom(ownerId.Value.Encode());

            return await QueryItems(public_key_ss58);
        }

        private async Task<List<AsylumBlueprint>> QueryBlueprints()
        {
            JArray blueprintsInfos = await _client.State.GetPairsAsync(RequestGenerator.GetStorageKeyBytesHash("RmrkCore", "Collections"));
            JArray blueprintsInterpretations = await _client.State.GetPairsAsync(RequestGenerator.GetStorageKeyBytesHash("AsylumBlueprints", "BlueprintIntepretations"));
            return AsylumStandaloneUtils.DecodeBlueprints(blueprintsInfos, blueprintsInterpretations);
        }

        private async Task<List<AsylumTag>> QueryTags()
        {
            JArray tags = await _client.State.GetPairsAsync(RequestGenerator.GetStorageKeyBytesHash("AsylumBlueprints", "Tags"));
            return AsylumStandaloneUtils.DecodeTags(tags);
        }

        private async UniTask<bool> RequestMintItem(Account owner,int id,AsylumItemMetadata metadata)
        {
            AccountId32 accountId32 = new AccountId32();
            accountId32.Create(owner.Bytes);

            ChargeAssetTxPayment chargeAssetTx = new ChargeAssetTxPayment(0, 0);

            BaseCom<U32> blueprintId = new BaseCom<U32>();
   
            blueprintId.Create((CompactInteger)id);

            IFileSystemNode ipfsResult = await SendMetaToIpfs(metadata);

            if (ipfsResult != null && ipfsResult.Id != null)
            {
                Debug.Log($"IPFS CID - {ipfsResult.Id.ToString()}");

                BoundedVecT1 itemMetadata = AsylumStandaloneUtils.ToBoundedVec<BoundedVecT1>(ipfsResult.Id);
                Method extrinsicMethod = AsylumBlueprintsCalls.MintItem(accountId32, blueprintId, itemMetadata);

                Task<string> subscriptionTask = Client.Author.SubmitAndWatchExtrinsicAsync(OnExtrinsicStateUpdateEvent, extrinsicMethod, owner, chargeAssetTx, 0, CancellationToken.None);

                AddMintingSubscriptionIdTask(subscriptionTask);

                await subscriptionTask;

                return true;
            }
            else
            {
                Debug.LogError("Can`t save data to ipfs. Stoping mining item.");
            }

            return false;
        }

        private async UniTask<IFileSystemNode> SendMetaToIpfs(AsylumItemMetadata metadata)
        {
            string metadataJson = JsonUtility.ToJson(metadata);

            if (this._ipfs == null)
                this._ipfs = new IpfsClient();

            return await this._ipfs.FileSystem.AddTextAsync(metadataJson);
        }

        private async UniTask<IFileSystemNode> SendMetaToIpfs(string metadataJson)
        {
            if (this._ipfs == null)
                this._ipfs = new IpfsClient();

            return await this._ipfs.FileSystem.AddTextAsync(metadataJson);
        }

        private async UniTaskVoid GetNewMintedItem(string owner)
        {

            //TODO: improve with union/sets
            if (this.AsylumItems == null)
                await RequestUserItemsAsync();

            var oldItems = this.AsylumItems;
            if (owner != null)
            {
                this.AsylumItems = await QueryItems(owner);

                List<AsylumItem> newMintedItems = new List<AsylumItem>();

                //TODO : Check for removed items also
                foreach (var item in this.AsylumItems)
                {
                    if (!oldItems.Contains(item))
                        newMintedItems.Add(item);
                }

                OnMintedItemsRecieved?.Invoke(newMintedItems.ToArray());
            }
            else
            {
                Debug.LogError("In StandaloneClient:GetNewMintedItem - Can`t get user item. User secret phrase is not set.");
            }

        }

        private async UniTaskVoid GetNewMintedItem()
        {

            //TODO: improve with union/sets
            if (this.AsylumItems == null)
                await RequestUserItemsAsync();

            var oldItems = this.AsylumItems;

            if (this._account != null)
            {
                this.AsylumItems = await QueryItems();

                List<AsylumItem> newMintedItems = new List<AsylumItem>();

                //TODO : Check for removed items also
                foreach (var item in this.AsylumItems)
                {
                    if (!oldItems.Contains(item))
                        newMintedItems.Add(item);
                }

                OnMintedItemsRecieved?.Invoke(newMintedItems.ToArray());
            }
            else
            {
                Debug.LogError("In StandaloneClient:GetNewMintedItem - Can`t get user item. User secret phrase is not set.");
            }

        }


        private async UniTaskVoid CheckMintedItemsAsync(string subscriptionId)
        {
            await UniTask.RunOnThreadPool(() => CheckMintedItems(subscriptionId));
        }

        private void CheckMintedItems(string subscriptionId)
        {
            if (this._mintingSubscriptionIds != null)
            {
                Task<string> tempResult = null;

                foreach (var subscriptionIdTask in _mintingSubscriptionIds)
                {
                    if (!subscriptionIdTask.IsCompleted)
                        subscriptionIdTask.Wait();

                    if (subscriptionIdTask.Result == subscriptionId)
                    {
                        tempResult = subscriptionIdTask;
                        GetNewMintedItem().Forget();
                    }
                }

                this._mintingSubscriptionIds.Remove(tempResult);

            }
            else
            {
                Debug.LogError("In StandaloneClient:CheckMintedItems - Array is null, can`t check.");
            }

        }

        private void AddMintingSubscriptionIdTask(Task<string> subscriptionTask)
        {
            if (subscriptionTask != null)
            {
                if (this._mintingSubscriptionIds == null)
                {
                    this._mintingSubscriptionIds = new();
                }

                this._mintingSubscriptionIds.Add(subscriptionTask);
            }
            else
                Debug.LogError("In StandaloneClient:AddMintingSubscriptionIdTask - subscriptionTask is null, can`t add in array.");

        }

        //TODO: optimize switch
        private void OnExtrinsicStateUpdateEvent(string subscriptionId, ExtrinsicStatus extrinsicStatus)
        {
            string state = "Unknown";
            int value = 0;
            switch (extrinsicStatus.ExtrinsicState)
            {
                case ExtrinsicState.None:
                    if (extrinsicStatus.InBlock?.Value.Length > 0)
                    {
                        state = "InBlock";
                        value = 5;
                    }
                    else if (extrinsicStatus.Finalized?.Value.Length > 0)
                    {
                        state = "Finalized";
                        value = 6;
                    }
                    else
                    {
                        state = "None";
                        value = 0;
                    }
                    break;

                case ExtrinsicState.Future:
                    state = "Future";
                    break;

                case ExtrinsicState.Ready:
                    state = "Ready";
                    value = 2;
                    break;

                case ExtrinsicState.Dropped:
                    state = "Dropped";
                    value = 0;
                    break;

                case ExtrinsicState.Invalid:
                    state = "Invalid";
                    value = 0;
                    break;
            }

            Debug.Log($"State is {state} || Value is - {value}");

            if (value == 5)
                CheckMintedItemsAsync(subscriptionId).Forget();
        }
            #region Unity Editor Region
#if UNITY_EDITOR
        //TODO : Optimize editor methods for better architecture
        public async Task<bool> RequestMintItemEditorAsync(Account owner, int id, AsylumItemMetadata metadata)
        {
            if (!await this.ConnectToChainAsync())
                return false;

            await RequestUserItemsEditorAsync(owner.Value);

            return await RequestMintItem(owner, id, metadata);
        }
        public async Task<AsylumTag[]> RequestTagsEditorAsync()
        {
            if (!await this.ConnectToChainAsync())
                return null;

            List<AsylumTag> tags = await QueryTags();

            if (tags != null)
                return tags.ToArray();

            return null;
        }
        public async Task<AsylumBlueprint[]> RequestUserBlueprintsEditorAsync()
        {
            if (!await this.ConnectToChainAsync())
                return null;

            List<AsylumBlueprint> blueprints = await QueryBlueprints();

            if (blueprints != null)
                return blueprints.ToArray();

            return null;
        }
        //WARNING : Not sure that is legit
        public async Task<AsylumItem[]> RequestUserItemsEditorAsync(string accountString)
        {
            this.AsylumItems = new List<AsylumItem>(await this.RequestUserItemsAsync(accountString));
            return this.AsylumItems.ToArray();
        }
        //This using for recconnect from plugin
        public async Task ReconnectEditorAsync(Action refreshLogicCallback)
        {
            //TODO : Error handling

            await CloseConnectionEditorAsync();

            refreshLogicCallback?.Invoke();

            this.CreateIpfsClientAndConnect();

            await ConnectToChainAsync();

        }

        public async Task ReconnectEditorAsync()
        {
            //TODO : Error handling

            await CloseConnectionEditorAsync();

            this.CreateIpfsClientAndConnect();

            await ConnectToChainAsync();

        }
        //HACK : Can't wait UniTaskVoid nor async void
        public async Task CloseConnectionEditorAsync()
        {
            if (this._client != null)
            {
                if (this._client.IsConnected)
                {
                    await this._client.CloseAsync();
                }
            }
        }
        //--------------------------------------
#endif

    }
}
            #endregion
#endif
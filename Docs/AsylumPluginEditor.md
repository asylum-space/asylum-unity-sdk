## Using Asylum Editor Plugin

1. The first thing you need to do is to download our [example](https://gitlab.com/asylum-space/asylum-unity-sdk-example) or add the [AsylumSDK Plugin](./) to the project.

2. Then you will need to create empty `GameObject` in the scene and then add to it the AsylumPlugin script component.

![](Docs/img/EditorPlugin_CreateObj.png)

![](Docs/img/EditorPlugin_SetComponent.png)

> Note: If you have downloaded our [example](https://gitlab.com/asylum-space/asylum-unity-sdk-example), then you can find this `GameObject` named "EditorPlugin" in the scene hierarchy.

3. Make sure you enter the user's passphrase in the [AsylumPlugin](AsylumSDK/AsylumPlugin.cs) component field.

![](Docs/img/EditorPlugin_SetPhrase.png)

4. After that, press the button to set the secret phrase. Then you should see the logs about the correct connection and generation of the correct **PulicKey**.

![](Docs/img/EditorPlugin_ClickButtonSet.png)

![](Docs/img/EditorPlugin_ConnectLogs.png)

5. You will have three new buttons with which you can request information and display it.

![](Docs/img/EditorPlugin_AllButtons.png)

- This is what a **Tags** request looks like.

    ![](Docs/img/EditorPlugin_TagButton.png)
- This is what a **Blueprints** request looks like.

    ![](Docs/img/EditorPlugin_BlueprintButton.png)

- This is what a **User Items** request looks like.

    ![](Docs/img/EditorPlugin_ItemButton.png)

6. Also in the **Blueprint** tab you can mint an item by clicking on the **Mint Item button**.

![](Docs/img/EditorPlugin_MintItem.png)

- And then you can find new item in **User Items** tab
    
    ![](Docs/img/EditorPlugin_ItemIsMinted.png)


## Run in Unity Play Mode
1. After you have loaded the [example](https://gitlab.com/asylum-space/asylum-unity-sdk-example) or added the [AsylumSDK Plugin](./) to the project and created an GameObject with component [AsylumDigitalObjectsController](AsylumSDK/AsylumDigitalObjectsController.cs), make sure you enter the user's passphrase in the [AsylumDigitalObjectsController](AsylumSDK/AsylumDigitalObjectsController.cs) component field.

![](img/set_secret_phrase.png)

2. [Download](https://docs.docker.com/get-docker/), install and run Docker.

3. Follow the steps to [Run Creator Studio (Docker setup)](https://gitlab.com/asylum-space/asylum-ui/-/tree/main/packages/game-developers-console#run-game-developers-console-docker-setup) and run the following command in terminal:

```
docker compose up
```
> Note: Since you are not using WebGL it is not necessary to run Creator Studio. You can limit yourself to running IPFS and AsylumNode.

4. And now you can start Unity Play Mode.

![](img/playmode_start.png)

5. The final result should be this. In the console you should get the **Public Key** that was generated from the secret phrase. And you should also see a message about the correct connection to the blockchain.

![](img/playmode_correctConnect.png)

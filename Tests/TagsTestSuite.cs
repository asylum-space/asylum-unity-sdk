using Asylum;
using NUnit.Framework;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.TestTools;

public class TagsTestSuite
{

    private bool IsEqualByPixels(Texture2D first, Texture2D second)
    {
        var firstPixels = first.GetPixels();
        var secondPixels = second.GetPixels();

        if (firstPixels.Length != secondPixels.Length)
            return false;

        for(int i = 0; i < firstPixels.Length; ++i)
        {
            if (firstPixels[i] != secondPixels[i])
                return false;
        }

        return true;
    }

    private bool IsEqualByPixels(Sprite[] first, Sprite[] second)
    {
        if (first.Length != second.Length)
            return false;

        for(int i=0; i < first.Length; ++i)
        {
            if (!IsEqualByPixels(first[i].texture, second[i].texture))
                return false;
        }

        return true;
    }

    [Test]
    [TestCase("default-view", 
        "DefaultViewInterpretation Sprite cant be loaded! Something wrong with texture! Return NULL!")]
    [TestCase("inventory-view",
        "InventoryViewTag Sprite cant be loaded! Something wrong with texture! Return NULL!")]
    public void SpriteTagsComparingTest(string tagString, string errorMessage)
    {
        var tagObject = TagController.Instance.GetTagBaseByString(tagString);

        UnityEngine.Assertions.Assert.IsNotNull(tagObject);

        Sprite mockedSprite = Resources.Load<Sprite>("TestingSprite");

        var mockedTexture = mockedSprite.texture;

        UnityEngine.Assertions.Assert.IsNotNull(mockedSprite);

        byte[] rawData = mockedSprite.texture.EncodeToPNG();

        var result = tagObject.AdaptData(rawData, null) as Sprite;
        UnityEngine.Assertions.Assert.IsNotNull(result);

        Assert.IsTrue(IsEqualByPixels(mockedSprite.texture, result.texture));

        byte[] nonValidRawNull = null;
        byte[] nonValidRawEmpty = System.Array.Empty<byte>();

        var resultFromNull = tagObject.AdaptData(nonValidRawNull, null);
        var resultFromEmpty = tagObject.AdaptData(nonValidRawEmpty, null);

        UnityEngine.Assertions.Assert.IsNull(resultFromNull);
        UnityEngine.Assertions.Assert.IsNull(resultFromEmpty);

        byte[] nonValid = new byte[] { 0x20, 0x25 };

        var resultFullyNonValid = tagObject.AdaptData(nonValid, null);

        LogAssert.Expect(LogType.Error, errorMessage);
    }

    [Test]
    public void TagTypeTest()
    {
        var tagObjectByString = TagController.Instance.GetTagBaseByString("default-view");

        Assert.AreEqual(typeof(Sprite), tagObjectByString.DataType);

        var tagObjectByType = TagController.Instance.GetTagBaseByType(typeof(Sprite));

        Assert.AreEqual(typeof(Sprite), tagObjectByType.DataType);
    }

    [Test]
    public void SpriteAtlasTagTest()
    {
        //Load atlas as array of sprites
        Sprite[] mockedSprites = Resources.LoadAll<Sprite>("TestingSpriteAtlas");
        UnityEngine.Assertions.Assert.IsNotNull(mockedSprites);

        //Load atlas as single sprite for getting bytes from it
        Sprite mockedSingleSprite = Resources.Load<Sprite>("TestingSpriteAtlas");
        UnityEngine.Assertions.Assert.IsNotNull(mockedSingleSprite);

        byte[] rawData = mockedSingleSprite.texture.EncodeToPNG();

        var atlasTagObject = TagController.Instance.GetTagBaseByString("2d-sprite-atlas");

        UnityEngine.Assertions.Assert.IsNotNull(atlasTagObject);

        Dictionary<string, object> mockedMetadataDictionary = new();
        mockedMetadataDictionary.Add("rowCount", 2);
        mockedMetadataDictionary.Add("columnCount", 2);
        mockedMetadataDictionary.Add("pivotX", 0.0f);
        mockedMetadataDictionary.Add("pivotY", 0.0f); 
        mockedMetadataDictionary.Add("tileWidth", 130); 
        mockedMetadataDictionary.Add("tileHeight", 130);


        var result = atlasTagObject.AdaptData(rawData, mockedMetadataDictionary) as Sprite[];
        UnityEngine.Assertions.Assert.IsNotNull(result);

        Assert.IsTrue(IsEqualByPixels(mockedSprites, result));

        byte[] nonValidRawNull = null;
        byte[] nonValidRawEmpty = System.Array.Empty<byte>();

        var resultFromNull = atlasTagObject.AdaptData(nonValidRawNull, mockedMetadataDictionary);
        var resultFromEmpty = atlasTagObject.AdaptData(nonValidRawEmpty, mockedMetadataDictionary);

        UnityEngine.Assertions.Assert.IsNull(resultFromNull);
        UnityEngine.Assertions.Assert.IsNull(resultFromEmpty);

        var resultWithoutMeta = atlasTagObject.AdaptData(rawData, null);

        UnityEngine.Assertions.Assert.IsNull(resultWithoutMeta);

        byte[] nonValid = new byte[] { 0x20, 0x25 };

        string errorMessage = "SpriteAtlas2DTag sprite isn't an atlas or rawData cannot be adapt as sprite-atlas! Return null!";

        var resultFullyNonValid = atlasTagObject.AdaptData(nonValid, mockedMetadataDictionary);

        LogAssert.Expect(LogType.Error, errorMessage);
    }
}

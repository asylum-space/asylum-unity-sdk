using Asylum;
using Cysharp.Threading.Tasks;
using NUnit.Framework;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.TestTools;

public class DataLoadAsyncTestSuite
{
    private bool _isTested = false;
    private byte[] _cachedData = null;
    private string _savedPath = "https://civiconcepts.com/wp-content/uploads/2020/08/Pile-Load-Test-Method.jpg";

    [SetUp]
    public void Setup()
    {
        if (DataLoader.instance == null)
        {
            new GameObject().AddComponent<DataLoader>();
        }
    }

    private async UniTaskVoid DataLoadAsync(string path)
    {
        var result = await DataLoader.instance.GetDataAsync(path);

        Assert.IsTrue(result.state);

        Assert.Greater(result.data.Length, 0);
    }

    [Test]
    [TestCase("https://civiconcepts.com/wp-content/uploads/2020/08/Pile-Load-Test-Method.jpg")] //Valid path
    [TestCase("non-valid-path")]
    public void SingleLoadingAsync(string path)
    {
        DataLoadAsync(path).Forget();
    }

    [UnityTest]
    public IEnumerator MultiplyLoadingAsync()
    {
        FirstAsyncLoading().Forget();
        SecondAsyncLoading().Forget();

        yield return WaitingCoroutine();
    }

    private async UniTaskVoid FirstAsyncLoading()
    {
        var result = await DataLoader.instance.GetDataAsync(_savedPath);

        UnityEngine.Debug.Log("FirstAsyncLoading load complete");

        Assert.IsTrue(result.state);
        Assert.Greater(result.data.Length, 0);

        _cachedData = result.data;
    }

    private async UniTaskVoid SecondAsyncLoading()
    {
        var result = await DataLoader.instance.GetDataAsync(_savedPath);

        UnityEngine.Debug.Log("SecondAsyncLoading load complete");

        Assert.IsTrue(result.state);
        Assert.Greater(result.data.Length, 0);

        Assert.AreEqual(result.data.Length, _cachedData.Length);

        for (int i = 0; i < result.data.Length; ++i)
        {
            Assert.AreEqual(result.data[i], _cachedData[i]);
        }

        _isTested = true;
    }

    private IEnumerator WaitingCoroutine()
    {
        while (!_isTested)
        {
            yield return null;
        }
    }


    [TearDown]
    public void TearDown()
    {
        _isTested = false;
        _cachedData = null;

        //Object.Destroy(DataLoader.instance.gameObject);
    }
}

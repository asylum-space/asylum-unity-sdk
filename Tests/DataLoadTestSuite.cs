using System.Collections;
using System.Collections.Generic;
using Asylum;
using Newtonsoft.Json.Linq;
using NUnit.Framework;
using UnityEngine;
using UnityEngine.TestTools;
using Cysharp.Threading.Tasks;

public class DataLoadTestSuite
{
    private bool _isTested = false;

    [SetUp]
    public void Setup()
    {
        //Arrange

        if(DataLoader.instance == null)
        {
            new GameObject().AddComponent<DataLoader>();
        }
    }

    private void DataLoadTestCallback(string path)
    {
        //string loadingPath = "https://civiconcepts.com/wp-content/uploads/2020/08/Pile-Load-Test-Method.jpg";

        //Act
        DataLoader.instance.LoadDataByCallback(path,
                (bytes) =>
                {
                    //Assert
                    Assert.Greater(bytes.Length, 0); 
                }
            );
    }

    [Test]
    [TestCase("https://civiconcepts.com/wp-content/uploads/2020/08/Pile-Load-Test-Method.jpg")] //Valid path
    [TestCase("non-valid-path")]
    public void SingleLoadingByCallbackTest(string path)
    {
        DataLoadTestCallback(path);
    }

    private byte[] _cachedData = null;
    private string _savedPath = "https://civiconcepts.com/wp-content/uploads/2020/08/Pile-Load-Test-Method.jpg";


    [UnityTest]
    public IEnumerator MultiplyLoadingByCallbackTest()
    {
        DataLoader.instance.LoadDataByCallback(_savedPath, OnFirstLoadedCallback);

        yield return WaitingCoroutine();
    }

    private void OnFirstLoadedCallback(byte[] rawData)
    {
        UnityEngine.Debug.Log("OnFirstLoadedCallback");

        Assert.Greater(rawData.Length, 0);

        _cachedData = rawData;

        DataLoader.instance.LoadDataByCallback(_savedPath, OnSecondLoadedCallback);
    }

    private void OnSecondLoadedCallback(byte[] rawData)
    {
        UnityEngine.Debug.Log("OnSecondLoadedCallback");

        Assert.Greater(rawData.Length, 0);

        Assert.AreEqual(rawData.Length, _cachedData.Length);

        for(int i = 0; i < rawData.Length; ++i)
        {
            Assert.AreEqual(rawData[i], _cachedData[i]);
        }

        _isTested = true;
    }

    [UnityTest]
    public IEnumerator MultiplyLoadingMixedCallbackFirst()
    {
        DataLoader.instance.LoadDataByCallback(_savedPath, MixedCallback);

        yield return WaitingCoroutine(); //Wait while first loading is ended

        MixedAsync().Forget();

        yield return WaitingCoroutine(); //Wait while second loading is ended
    }

    [UnityTest]
    public IEnumerator MultiplyLoadingMixedAsyncFirst()
    {
        MixedAsync().Forget();

        yield return WaitingCoroutine(); //Wait while first loading is ended

        DataLoader.instance.LoadDataByCallback(_savedPath, MixedCallback);

        yield return WaitingCoroutine(); //Wait while second loading is ended
    }

    private void MixedCallback(byte[] rawData)
    {
        UnityEngine.Debug.Log("MixedCallback");

        Assert.Greater(rawData.Length, 0);

        _cachedData = rawData;

        _isTested = true;
    }

    private async UniTaskVoid MixedAsync()
    {
        var result = await DataLoader.instance.GetDataAsync(_savedPath);
        UnityEngine.Debug.Log("MixedAsync");

        Assert.IsTrue(result.state);
        Assert.Greater(result.data.Length, 0);

        _isTested = true;
    }


    private IEnumerator WaitingCoroutine()
    {
        while (!_isTested)
        {
            yield return null;
        }
    }

    [TearDown]
    public void TearDown()
    {
        _isTested = false;
        _cachedData = null;

       // Object.Destroy(DataLoader.instance.gameObject);
    }
}

using Asylum;
using NUnit.Framework;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.TestTools;

public class AsylumDigitalObjectsControllerTestSuite
{
    private AsylumDigitalObjectsController _asylumDigitalObjectControllerInstance;

    private bool _isUserInfoReady = false;
    private bool _isSpaceMetadataReady = false;
    private bool _isUserItemReady = false;

    protected bool IsTested => _isUserItemReady && _isSpaceMetadataReady && _isUserInfoReady; 

    UserInfo _mockedInfo = new UserInfo(new User("John", "0x2032121"));

    SpaceMetadata _mockedSpaceMetadata = new("stringID", "stringTitle", "stringImg", "stringGenre", "stringShordDescription",
            "stringDescription",
            new string[] { "FisrtGalleryElement", "SecondGalleryElement" },
            new Review[] { new Review("reviewId1","reviewName1", "reviewText1", "reviewDate1", 1, "reviewAddress1"),
                           new Review("reviewId1","reviewName1", "reviewText1", "reviewDate1", 1, "reviewAddress1")});

    AsylumItem _mockedItem;

    [SetUp]
    public void Setup()
    {
        if (DataLoader.instance == null)
        {
            new GameObject().AddComponent<DataLoader>();
        }

        _asylumDigitalObjectControllerInstance = new GameObject().AddComponent<AsylumDigitalObjectsController>();
        _asylumDigitalObjectControllerInstance.AccountSecretPhrase = "eternal danger cherry radar exit damage slam hip say relief awesome middle";
        _asylumDigitalObjectControllerInstance.isAutoRequestItemsOnStart = false;

        UnityEngine.Assertions.Assert.IsNotNull(DataLoader.instance);
        UnityEngine.Assertions.Assert.IsNotNull(_asylumDigitalObjectControllerInstance);

        Assert.IsFalse(_asylumDigitalObjectControllerInstance.isAutoRequestItemsOnStart);
    }

    protected IEnumerator WaitingCoroutine()
    {
        yield return new WaitForSecondsRealtime(0.5f);

        while (!IsTested)
        {
            //waiting for not delete this object while it's not tested yet
            yield return null;
        }
    }

    private void UserInfoCallback(UserInfo userInfo)
    {
        Assert.IsTrue(ReactClientTestSuite.AreUserInfosEqual(userInfo, _mockedInfo));
        
        _asylumDigitalObjectControllerInstance.OnUserInfoReceived -= UserInfoCallback;

        _isUserInfoReady = true;
    }

    private void SpaceMetadataCallback(SpaceMetadata spaceMetadata)
    {
        Assert.IsTrue(ReactClientTestSuite.AreSpacemetadatasEqual(spaceMetadata, _mockedSpaceMetadata));

        _asylumDigitalObjectControllerInstance.OnSpaceMetadataReceived -= SpaceMetadataCallback;

        _isSpaceMetadataReady = true;
    }

    private void UserItemHandlerCallback(AsylumItemAsyncHandler itemHandler)
    {
        Assert.AreEqual(itemHandler.AsylumItem, _mockedItem);

        _asylumDigitalObjectControllerInstance.OnItemWasLoaded -= UserItemHandlerCallback;

        var handlerByItem = _asylumDigitalObjectControllerInstance.GetItemHandler(_mockedItem);
        Assert.AreEqual(handlerByItem.AsylumItem, _mockedItem);

        var handlerByIDs = _asylumDigitalObjectControllerInstance.GetItemHandler(_mockedItem.blueprintId, _mockedItem.id);
        Assert.AreEqual(handlerByIDs.AsylumItem, _mockedItem);

        var handlersList = _asylumDigitalObjectControllerInstance.GetItemHandlers(_mockedItem.blueprintId);
        Assert.AreEqual(handlersList.Count, 1);
        Assert.AreEqual(handlersList[0].AsylumItem, _mockedItem);
        
        _isUserItemReady = true;
    }

    [UnityTest]
    public IEnumerator UserDataTestCoroutine()
    {
        _mockedItem = AbstractAsylumItemAsyncHandlerTestSuite.MockAsylumItem();

        _asylumDigitalObjectControllerInstance.OnItemWasLoaded += UserItemHandlerCallback;
        _asylumDigitalObjectControllerInstance.Mocked_StartAsyncItemLoad(_mockedItem);

        _asylumDigitalObjectControllerInstance.OnSpaceMetadataReceived += SpaceMetadataCallback;
        _asylumDigitalObjectControllerInstance.Mocked_SpaceMetadataReceive(_mockedSpaceMetadata);

        _asylumDigitalObjectControllerInstance.OnUserInfoReceived += UserInfoCallback;
        _asylumDigitalObjectControllerInstance.Mocked_UserInfoReceive(_mockedInfo);

        yield return WaitingCoroutine();
    }

    [TearDown]
    public void TearDown()
    {
        Object.Destroy(DataLoader.instance);

        Object.Destroy(_asylumDigitalObjectControllerInstance.gameObject);
    }
}

using Asylum;
using Cysharp.Threading.Tasks;
using NUnit.Framework;
using System.Collections;
using System.Collections.Generic;
using System.Threading.Tasks;
using UnityEngine;
using UnityEngine.TestTools;

public class AsylumAssemblerUtilsTestSuite
{
    private StandaloneClient _standClient;

    private string _mainPath = @"http://127.0.0.1:8080/ipfs/";

    private bool _isTested = false;


    [SetUp]
    public void Setup()
    {
        _standClient = new();
        _standClient.SetSecretPhrase("eternal danger cherry radar exit damage slam hip say relief awesome middle");
        _standClient.OnConnected();

        UnityEngine.Assertions.Assert.IsNotNull(_standClient);
    }

    private async Task RequestEditorBlueprintsAsync()
    {
        List <AsylumAssembledBlueprint> asylumBlueprints = null;

        var res = await Task.Run(() => asylumBlueprints = AsylumAssembledBlueprint.ConvertToAssembledBlueprintsList(_standClient.RequestUserBlueprintsEditorAsync().Result))
            .ContinueWith((state) => AsylumAssembledBlueprint.LoadBlueprintsMetaAsync(asylumBlueprints, this._mainPath));

        UnityEngine.Assertions.Assert.IsNotNull(asylumBlueprints);

        Assert.Greater(asylumBlueprints.Count, 0);

        _isTested = true;
    }

    [UnityTest]
    public IEnumerator AsylumAssemblerUtilsTestCoroutine()
    {
        Task.Run(() => RequestEditorBlueprintsAsync());

        yield return WaitingCoroutine();
    }

    private IEnumerator WaitingCoroutine()
    {
        while (!_isTested)
        {
            yield return null;
        }
    }
}

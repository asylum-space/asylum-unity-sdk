using NUnit.Framework;
using System.Collections;
using UnityEngine;
using UnityEngine.TestTools;

public class GetAdaptedTestSuite : AbstractAsylumItemAsyncHandlerTestSuite
{
    private void AllGetTest()
    {
        GetAdaptedByStringValid();
        GetAdaptedByStringNonValid();
        GetAdaptedByTypeOnlyValid();
        GetAdaptedByTypeOnlyNonValid();
        GetAdaptedByTypeAndTagValid();
        GetAdaptedByTypeAndTagNonValid();
        GetAllAdapted();

        _isTested = true;
    }

    private void GetAdaptedByStringValid()
    {
        var adaptedObj = _itemHandler.GetAdapted("default-view");

        UnityEngine.Assertions.Assert.IsNotNull(adaptedObj);

        foreach (var singleAdapted in adaptedObj)
        {
            Assert.IsTrue(singleAdapted is Sprite adaptedSprite);
        }
    }

    private void GetAdaptedByStringNonValid()
    {
        var adaptedObj = _itemHandler.GetAdapted("non-valid-tag");
        UnityEngine.Assertions.Assert.IsNotNull(adaptedObj);

        Assert.AreEqual(adaptedObj.Length, 0);

        _isTested = true;
    }

    private void GetAdaptedByTypeOnlyValid()
    {
        var adaptedObj = _itemHandler.GetAdapted(typeof(Sprite));

        UnityEngine.Assertions.Assert.IsNotNull(adaptedObj);

        foreach (var singleAdapted in adaptedObj)
        {
            Assert.IsTrue(singleAdapted is Sprite adaptedSprite);
        }

        _isTested = true;
    }

    private void GetAdaptedByTypeOnlyNonValid()
    {
        var adaptedObj = _itemHandler.GetAdapted(typeof(Transform));
        UnityEngine.Assertions.Assert.IsNotNull(adaptedObj);

        Assert.AreEqual(adaptedObj.Length, 0);

        _isTested = true;
    }

    private void GetAdaptedByTypeAndTagValid()
    {
        var adaptedObj = _itemHandler.GetAdapted(typeof(Sprite), "default-view");

        UnityEngine.Assertions.Assert.IsNotNull(adaptedObj);

        foreach (var singleAdapted in adaptedObj)
        {
            Assert.IsTrue(singleAdapted is Sprite adaptedSprite);
        }

        _isTested = true;
    }

    private void GetAdaptedByTypeAndTagNonValid()
    {
        var adaptedObjectsByTagString = _itemHandler.GetAdapted(typeof(Transform), "non-valid-tag");

        UnityEngine.Assertions.Assert.IsNotNull(adaptedObjectsByTagString);

        Assert.AreEqual(adaptedObjectsByTagString.Length, 0);

        _isTested = true;
    }

    private void GetAllAdapted()
    {
        var adaptedObj = _itemHandler.GetAllAdapted();

        UnityEngine.Assertions.Assert.IsNotNull(adaptedObj);

        Assert.AreNotEqual(adaptedObj, 0);

        foreach (var singleAdapted in adaptedObj)
        {
            UnityEngine.Assertions.Assert.IsNotNull(singleAdapted);

            UnityEngine.Assertions.Assert.IsNotNull(singleAdapted.dataObject);
        }
    }

    [UnityTest]
    public IEnumerator TagInterpretationTestCoroutine()
    {
        yield return WaitingCoroutine(AllGetTest);
    }
}

using Asylum;
using Cysharp.Threading.Tasks;
using NUnit.Framework;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.TestTools;

public class StandaloneClientIntegrationTestSuit
{
    StandaloneClient _standClient;

    private bool _tagAsyncloaded = false, _bluprintAsyncLoaded = false, _mainLoading = false;

    private bool IsTested => _mainLoading && _tagAsyncloaded && _bluprintAsyncLoaded;

    [SetUp]
    public void Setup()
    {
        _standClient = new();
        _standClient.SetSecretPhrase("eternal danger cherry radar exit damage slam hip say relief awesome middle");
        _standClient.OnConnected();

        UnityEngine.Assertions.Assert.IsNotNull(_standClient);

        _standClient.OnUserItemsRecieved += OnItemsReceived;

        _standClient.SendUserItemsRequest();
    }

    private async UniTaskVoid RequestTagsEditor()
    {
        var result = await _standClient.RequestTagsEditorAsync();

        UnityEngine.Assertions.Assert.IsNotNull(result);
        Assert.Greater(result.Length, 0);

        _tagAsyncloaded = true;
    }

    private async UniTaskVoid RequestBlueprintsEditor()
    {
        var result = await _standClient.RequestUserBlueprintsEditorAsync();

        UnityEngine.Assertions.Assert.IsNotNull(result);
        Assert.Greater(result.Length, 0);

        _bluprintAsyncLoaded = true;
    }

    [UnityTest]
    public IEnumerator StandaloneTestCoroutine()
    {
        RequestTagsEditor().Forget();
        RequestBlueprintsEditor().Forget();

        yield return WaitingCoroutine();
    }

    private void OnItemsReceived(AsylumItem[] items)
    {
        _standClient.OnUserItemsRecieved -= OnItemsReceived;

        var manuallyGetItem = _standClient.AsylumItems;

        Assert.AreEqual(items.Length, manuallyGetItem.Count);

        for(int i = 0; i < items.Length; ++i)
        {
            Assert.AreEqual(items[i], manuallyGetItem[i]);
        }

        RequestItemMint();
    }

    private void RequestItemMint()
    {
        _standClient.OnMintedItemsRecieved += OnMintedItems;

        _standClient.SendMintItemRequest(0, new AsylumItemMetadata("description string", "name string"));
    }

    private void OnMintedItems(AsylumItem[] items)
    {
        Assert.Greater(items.Length, 0);

        Assert.AreEqual(items[0].blueprintId, "0");

        _standClient.OnMintedItemsRecieved -= OnMintedItems;

        _mainLoading = true;
    }

    private IEnumerator WaitingCoroutine()
    {
        while(!IsTested)
        {
            yield return null;
        }
    }
}

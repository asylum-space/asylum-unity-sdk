using NUnit.Framework;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.TestTools;

public class TagInterpretationTestSuite : AbstractAsylumItemAsyncHandlerTestSuite
{
    private void TagInterpretationCheck()
    {
        Assert.IsTrue(_itemHandler.IsTagInterpretated("default-view")); //valid

        Assert.IsFalse(_itemHandler.IsTagInterpretated("non-valid-tag")); //nonvalid

        _isTested = true;
    }

    [UnityTest]
    public IEnumerator TagInterpretationTestCoroutine()
    {
        yield return WaitingCoroutine(TagInterpretationCheck);
    }
}

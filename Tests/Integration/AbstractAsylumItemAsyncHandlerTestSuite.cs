using Asylum;
using Cysharp.Threading.Tasks;
using NUnit.Framework;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class AbstractAsylumItemAsyncHandlerTestSuite
{
    public static AsylumItem MockAsylumItem()
    {
        AsylumItem mockItem = new AsylumItem(
                _blueprintId: "1",
                _itemId: "1",
                _owner: "5ELYXe9CdJupDtjymmk5aK1i1XkisWi1X9V6UHHjVSudHmCn",
                _royalty: "",
                _metadata: "QmQKV8egp8uFMWhYm5ixKeDW7Me7AAoz4ioBoEiBtAX4Qg",
                _equipped: true,
                _interpretations: new AsylumInterpretation[] {
                        MockAsylumInterpretation()
                    }
                );


        return mockItem;
    }

    public static AsylumInterpretation MockAsylumInterpretation()
    {
        AsylumInterpretation mockInterpretation =
            new AsylumInterpretation(
            _id: "0", //a define full 0:0:0?
            _tags: new string[] {
                   "default-view",
                   "png"
                },
            _interpretationInfo: new InterpretationInfo( //mock also?
                    _src: "QmRJAxL53ThPggqK1YrgDyrAc7dk73JZFPaCxf4fQdaZVP",
                    _metadata: "QmVeVRWBzg1ybhJfbUc7dxvCf544BKcTtA8vSoCP8Xdw98"
                )
            );

        return mockInterpretation;
    }

    protected AsylumItemAsyncHandler _itemHandler;
    protected bool _isTested = false;

    [SetUp]
    public void Setup()
    {
        if (DataLoader.instance == null)
        {
            new GameObject().AddComponent<DataLoader>();
        }

        _itemHandler = new AsylumItemAsyncHandler(
                MockAsylumItem(),
                "http://127.0.0.1:8080/ipfs/" //mb get from controller?
            );
    }

    private async UniTaskVoid LoadItemDataAsync(Action callback)
    {
        var itemLoadingDataState = await _itemHandler.LoadAllItemDataAsync();

        Assert.IsTrue(itemLoadingDataState);

        callback?.Invoke();
    }

    protected IEnumerator WaitingCoroutine(Action callback)
    {
        LoadItemDataAsync(callback).Forget();

        while (!_isTested)
        {
            //waiting for not delete this object while it's not tested yet
            yield return null;
        }
    }

    [TearDown]
    public void TearDown()
    {
        _itemHandler = null;
        _isTested = false;

        //UnityEngine.Object.Destroy(DataLoader.instance.gameObject);
    }
}

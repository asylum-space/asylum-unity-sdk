using NUnit.Framework;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.TestTools;

public class NameAndDescriptionTestSuite : AbstractAsylumItemAsyncHandlerTestSuite
{
    private void NameAndDescriptionCheck()
    {
        var name = _itemHandler.Name;
        var description = _itemHandler.Description;

        Assert.AreNotEqual(name, "NO_NAME_LOADED");
        Assert.AreNotEqual(description, "NO_DESCRIPTION_LOADED");

        _isTested = true;
    }

    [UnityTest]
    public IEnumerator LoadingNameAndDescriptionTestCoroutine()
    {
        yield return WaitingCoroutine(NameAndDescriptionCheck);
    }
}

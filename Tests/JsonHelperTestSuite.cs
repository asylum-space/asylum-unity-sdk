using Asylum;
using NUnit.Framework;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class JsonHelperTestSuite
{
    [Serializable]
    public class TestJsonClass
    {
        public int intValue;
        public string strValue;

        public TestJsonClass()
        {

        }

        public TestJsonClass(int a, string b)
        {
            intValue = a;
            strValue = b;
        }

        public override bool Equals(object obj)
        {
            return obj is TestJsonClass testObj && testObj.intValue == this.intValue && testObj.strValue == this.strValue;
        }

        public override int GetHashCode()
        {
            return base.GetHashCode();
        }
    }

    [Test]
    public void ParseFromJsonValid()
    {
        string valid = "{ \"Items\": [{ \"intValue\": 1, \"strValue\":\"first!\"}]}";
        TestJsonClass[] mocked = { new TestJsonClass(1, "first!") };

        TestJsonClass[] parsed = JsonHelper.FromJson<TestJsonClass>(valid);
        
        Assert.AreEqual(parsed, mocked);
    }

    [Test]
    public void ParseFromJsonUnvalid()
    {
        string nonvalid = "{ \"Items\": [{ \"intValue\": \"2\", \"strValue\":\"second!\"}]}";
        TestJsonClass[] mocked = { new TestJsonClass(1, "first!") };

        TestJsonClass[] parsed = JsonHelper.FromJson<TestJsonClass>(nonvalid);

        Assert.AreNotEqual(parsed, mocked);
    }

    [Test]
    public void ConvertToJson()
    {
        string mockedValid = "{\"Items\":[{\"intValue\":1,\"strValue\":\"first!\"}]}";

        TestJsonClass[] testArray =
            {
                new(1, "first!")
            };

        string jsonString = JsonHelper.ToJson<TestJsonClass>(testArray);

        Assert.AreEqual(jsonString, mockedValid);
    }
}

using Asylum;
using NUnit.Framework;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.TestTools;

public class ReactClientTestSuite
{
    ReactClient _reactClientInstance;

    [SetUp]
    public void Setup()
    {
        //Copy init from controller

        _reactClientInstance = new GameObject().AddComponent<ReactClient>();
    }

    public static bool AreUserInfosEqual(UserInfo first, UserInfo second)
    {
        return AreUsersEqual(first.User, second.User);
    }

    public static bool AreUsersEqual(User first, User second)
    {
        return first.name == second.name && first.address == second.address;
    }

    [Test]
    public void UserInfoTest()
    {
        UserInfo mockedInfo = new UserInfo(new User("John", "0x2032121"));

        _reactClientInstance.OnUserInfoRecieved += (info) =>
        {
            Assert.IsTrue(AreUserInfosEqual(info, mockedInfo));
        };

        string json = JsonUtility.ToJson(mockedInfo);

        _reactClientInstance.ParseUserInfo(json);
    }

    [Test]
    public void ItemMintedTest()
    {
        var mockedItem = AbstractAsylumItemAsyncHandlerTestSuite.MockAsylumItem();

        _reactClientInstance.OnMintedItemsRecieved += (mintedItems) =>
        {
            Assert.AreEqual(mintedItems.Length, 1);

            Assert.AreEqual(mintedItems[0], mockedItem);
        };

        string json = JsonHelper.ToJson(new AsylumItem[] { mockedItem });

        _reactClientInstance.ItemsMinted(json);
    }

    [Test]
    public void ItemReceivedTest()
    {
        var mockedItem = AbstractAsylumItemAsyncHandlerTestSuite.MockAsylumItem();

        _reactClientInstance.OnUserItemsRecieved += (userItems) =>
        {
            Assert.AreEqual(userItems.Length, 1);

            Assert.AreEqual(userItems[0], mockedItem);
        };

        string json = JsonHelper.ToJson(new AsylumItem[] { mockedItem });
        
        _reactClientInstance.ParseItems(json);
    }

    [Test]
    public void InterpretationReceivedTest()
    {
        var mockedInterpretation = AbstractAsylumItemAsyncHandlerTestSuite.MockAsylumInterpretation();

        _reactClientInstance.OnInterpretationReceived += (interpretations) =>
        {
            Assert.AreEqual(interpretations.Length, 1);

            Assert.AreEqual(interpretations[0], mockedInterpretation);
        };

        string json = JsonHelper.ToJson(new AsylumInterpretation[] { mockedInterpretation });

        _reactClientInstance.ParseInterpretations(json);
    }

    [Test]
    public void BlueprintsReceivedTest()
    {
        AsylumBlueprint mockedBlueprint = new AsylumBlueprint("0", "mocked_name", "100", "metadatahash", "issuerhash", 10,
                                            new[] { AbstractAsylumItemAsyncHandlerTestSuite.MockAsylumInterpretation() });

        _reactClientInstance.OnBlueprintsReceived += (blueprints) =>
        {
            Assert.AreEqual(blueprints.Length, 1);

            Assert.AreEqual(blueprints[0].id, mockedBlueprint.id);
        };

        string json = JsonHelper.ToJson(new AsylumBlueprint[] { mockedBlueprint });

        _reactClientInstance.ParseBlueprints(json);
    }

    [UnityTest]
    public IEnumerator ReactFunctionCallsCoroutine()
    {
        _reactClientInstance.OnConnected();
        yield return null;

        _reactClientInstance.SendUserItemsRequest();
        yield return null;

        _reactClientInstance.SendUserInfoRequest();
        yield return null;

        _reactClientInstance.SendSpaceMetadataRequest();
        yield return null;

        _reactClientInstance.SendMintItemRequest(0, new AsylumItemMetadata("description", "name"));
        yield return null;

        _reactClientInstance.SendSpaceCloseRequest();
        yield return null;

        _reactClientInstance.OnDisconnected();
    }

    public static bool AreSpacemetadatasEqual(SpaceMetadata first, SpaceMetadata second)
    {
        if (first.reviews.Length != second.reviews.Length)
            return false;
            

        for(int i = 0; i < first.reviews.Length; ++i)
        {
            if (!AreReviewsEqual(first.reviews[i], second.reviews[i]))
                return false;        
        }

        for(int i = 0; i < first.gallery.Length; ++i)
        {
            if (first.gallery[i] != second.gallery[i])
                return false;
        }

        return first.genre == second.genre
            && first.description == second.description
            && first.id == second.id
            && first.img == second.img
            && first.shortDescription == second.shortDescription
            && first.title == second.title;
    }

    public static bool AreReviewsEqual(Review first, Review second)
    {
        return first.address == second.address
            && first.date == second.date
            && first.id == second.id
            && first.name == second.name
            && first.rating == second.rating
            && first.text == second.text;
    }

    [Test]
    public void SpaceMetadataTest()
    {
        SpaceMetadata mocked = new("stringID", "stringTitle", "stringImg", "stringGenre", "stringShordDescription",
            "stringDescription",
            new string[] { "FisrtGalleryElement", "SecondGalleryElement" },
            new Review[] { new Review("reviewId1","reviewName1", "reviewText1", "reviewDate1", 1, "reviewAddress1"),
                           new Review("reviewId1","reviewName1", "reviewText1", "reviewDate1", 1, "reviewAddress1")});

        string mockedJson = JsonUtility.ToJson(mocked);

        _reactClientInstance.OnSpaceMetadataRecieved += (spaceMetadata) =>
        {
            Assert.IsTrue(AreSpacemetadatasEqual(mocked, spaceMetadata));
        };

        _reactClientInstance.ParseSpaceMetadata(mockedJson);
    }
}

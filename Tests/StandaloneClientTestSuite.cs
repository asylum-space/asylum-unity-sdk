using Asylum;
using NUnit.Framework;
using Newtonsoft.Json.Linq;
using System.Collections;
using System.Linq;
using System.Collections.Generic;
using UnityEngine;

public class StandaloneClientTestSuite
{
    public static JArray MockItems()
    {
        string mock = @"
        [
            [
                '0x5bef2c5471aa9e955551dc810f5abb39e8d49389c2e23e152fdd6364daadd2cc5153cb1f00942ff4010000005153cb1f00942ff401000000',
                '0x00648cfe08cacd22da41a17f1cf8d6e5e05288dbd11932912efe4a3dd3eba855620034746573745f6974656d5f325f30000001'
            ],
            [
                '0x5bef2c5471aa9e955551dc810f5abb39e8d49389c2e23e152fdd6364daadd2cc5153cb1f00942ff401000000b4def25cfda6ef3a00000000',
                '0x00648cfe08cacd22da41a17f1cf8d6e5e05288dbd11932912efe4a3dd3eba855620034746573745f6974656d5f315f30000001'
            ],
            [
                '0x5bef2c5471aa9e955551dc810f5abb39e8d49389c2e23e152fdd6364daadd2ccb4def25cfda6ef3a00000000b4def25cfda6ef3a00000000',
                '0x00648cfe08cacd22da41a17f1cf8d6e5e05288dbd11932912efe4a3dd3eba855620034746573745f6974656d5f305f30000001'
            ]
        ]
        ";
        return JArray.Parse(mock);
    }

    public static JArray MockItemInterpretations()
    {
        string mock = @"
        [
            [
                '0x5bef2c5471aa9e955551dc810f5abb392111e0df19de9563b58301e5f7e0074311d2df4e979aa105cf552e9544ebd2b50000000011d2df4e979aa105cf552e9544ebd2b50000000011d2df4e979aa105cf552e9544ebd2b500000000',
                '0x000000000001b8516d524a41784c35335468506767714b31597267447972416337646b37334a5a46506143786634665164615a565001b8516d5665565257427a67317962684a66625563376478764366353434424b635474413876536f435038586477393800000000'
            ],
            [
                '0x5bef2c5471aa9e955551dc810f5abb392111e0df19de9563b58301e5f7e0074311d2df4e979aa105cf552e9544ebd2b50000000011d2df4e979aa105cf552e9544ebd2b500000000754faa9acf0378f8c3543d9f132d85bc02000000',
                '0x020000000001b8516d624237436938656e77643173437154536f67584d336768513441715043677a4b4b31524e694851325667503301b8516d595157766d47426f74586e41777977366f6e4465486e457442577739774a315a6858355463556f676f45324d00000000'
            ],
            [
                '0x5bef2c5471aa9e955551dc810f5abb392111e0df19de9563b58301e5f7e0074311d2df4e979aa105cf552e9544ebd2b50000000011d2df4e979aa105cf552e9544ebd2b500000000d82c12285b5d4551f88e8f6e7eb52b8101000000',
                '0x010000000001b8516d547076454571556437725644616531436766786169436d65326a37387276376d756873455235726263567a5401b8516d514531333133736832386d64587254764c7a546546456a704d4259675a787774643764595938396e78326d4c00000000'
            ],
            [
                '0x5bef2c5471aa9e955551dc810f5abb392111e0df19de9563b58301e5f7e0074311d2df4e979aa105cf552e9544ebd2b50000000011d2df4e979aa105cf552e9544ebd2b500000000de18007c0afadc771c45bf719bc7fe5103000000',
                '0x030000000001b8516d63464533363447655732375956556b4242737757557972583379706170435337533962746e486f76466e345a01b8516d524754697747594a65434e35355a393455665a7973763464587662524a34647154466559487476317464596e00000000'
            ],
            [
                '0x5bef2c5471aa9e955551dc810f5abb392111e0df19de9563b58301e5f7e00743d82c12285b5d4551f88e8f6e7eb52b810100000011d2df4e979aa105cf552e9544ebd2b50000000011d2df4e979aa105cf552e9544ebd2b500000000',
                '0x000000000001b8516d53566157647538514a457871535a7668694c4c577472344266555548764873796d54354d7353624e32754c7801b8516d554776616f427079584839635746416d466335513237715435544766536874487a526341674a48466e67364500000000'
            ],
            [
                '0x5bef2c5471aa9e955551dc810f5abb392111e0df19de9563b58301e5f7e00743d82c12285b5d4551f88e8f6e7eb52b810100000011d2df4e979aa105cf552e9544ebd2b500000000754faa9acf0378f8c3543d9f132d85bc02000000',
                '0x020000000001b8516d54654665687466313155683471704577674453667a506d31374a7a326f5533523755456e6870534a6845737801b8516d50696d786f3652733166486f69587062564a62464a6435747471595a55694656436b71716a4178734a50314d00000000'
            ],
            [
                '0x5bef2c5471aa9e955551dc810f5abb392111e0df19de9563b58301e5f7e00743d82c12285b5d4551f88e8f6e7eb52b810100000011d2df4e979aa105cf552e9544ebd2b500000000d82c12285b5d4551f88e8f6e7eb52b8101000000',
                '0x010000000001b8516d576135517172516b706e4c51787243554b6e42556474525a70644e4268364b3178714e556a354553444c556401b8516d565673657776524846475379465741334552476364703779316d45726971457638587055537a445878514d4300000000'
            ],
            [
                '0x5bef2c5471aa9e955551dc810f5abb392111e0df19de9563b58301e5f7e00743d82c12285b5d4551f88e8f6e7eb52b8101000000d82c12285b5d4551f88e8f6e7eb52b810100000011d2df4e979aa105cf552e9544ebd2b500000000',
                '0x000000000001b8516d53566157647538514a457871535a7668694c4c577472344266555548764873796d54354d7353624e32754c7801b8516d554776616f427079584839635746416d466335513237715435544766536874487a526341674a48466e67364500000000'
            ],
            [
                '0x5bef2c5471aa9e955551dc810f5abb392111e0df19de9563b58301e5f7e00743d82c12285b5d4551f88e8f6e7eb52b8101000000d82c12285b5d4551f88e8f6e7eb52b8101000000754faa9acf0378f8c3543d9f132d85bc02000000',
                '0x020000000001b8516d54654665687466313155683471704577674453667a506d31374a7a326f5533523755456e6870534a6845737801b8516d50696d786f3652733166486f69587062564a62464a6435747471595a55694656436b71716a4178734a50314d00000000'
            ],
            [
                '0x5bef2c5471aa9e955551dc810f5abb392111e0df19de9563b58301e5f7e00743d82c12285b5d4551f88e8f6e7eb52b8101000000d82c12285b5d4551f88e8f6e7eb52b8101000000d82c12285b5d4551f88e8f6e7eb52b8101000000',
                '0x010000000001b8516d576135517172516b706e4c51787243554b6e42556474525a70644e4268364b3178714e556a354553444c556401b8516d565673657776524846475379465741334552476364703779316d45726971457638587055537a445878514d4300000000'
            ]
        ]
        ";
        return JArray.Parse(mock);
    }
    
    public static JArray MockItemInterpretationTags()
    {        
        string mock = @"
        [
            [
                '0xdfc15e6b4ac77b1b389535be70b0f0d7d184bfa8a3eff9ef286767e49095ea4a5153cb1f00942ff4010000005153cb1f00942ff4010000005153cb1f00942ff401000000',
                '0x103467616d652d6d656368616e696338696e76656e746f72792d766965773c706173736976652d6561726e696e670c706e670000'
            ],
            [
                '0xdfc15e6b4ac77b1b389535be70b0f0d7d184bfa8a3eff9ef286767e49095ea4a5153cb1f00942ff4010000005153cb1f00942ff4010000009eb2dcce60f37a2702000000',
                '0x0c2033642d6d6f64656c3467616d652d6d656368616e69633c706173736976652d6561726e696e670000'
            ],
            [
                '0xdfc15e6b4ac77b1b389535be70b0f0d7d184bfa8a3eff9ef286767e49095ea4a5153cb1f00942ff4010000005153cb1f00942ff401000000b4def25cfda6ef3a00000000',
                '0x083064656661756c742d766965770c706e670000'
            ],
            [
                '0xdfc15e6b4ac77b1b389535be70b0f0d7d184bfa8a3eff9ef286767e49095ea4a5153cb1f00942ff401000000b4def25cfda6ef3a000000005153cb1f00942ff401000000',
                '0x103467616d652d6d656368616e696338696e76656e746f72792d766965773c706173736976652d6561726e696e670c706e670000'
            ],
            [
                '0xdfc15e6b4ac77b1b389535be70b0f0d7d184bfa8a3eff9ef286767e49095ea4a5153cb1f00942ff401000000b4def25cfda6ef3a000000009eb2dcce60f37a2702000000',
                '0x0c2033642d6d6f64656c3467616d652d6d656368616e69633c706173736976652d6561726e696e670000'
            ],
            [
                '0xdfc15e6b4ac77b1b389535be70b0f0d7d184bfa8a3eff9ef286767e49095ea4a5153cb1f00942ff401000000b4def25cfda6ef3a00000000b4def25cfda6ef3a00000000',
                '0x083064656661756c742d766965770c706e670000'
            ],
            [
                '0xdfc15e6b4ac77b1b389535be70b0f0d7d184bfa8a3eff9ef286767e49095ea4ab4def25cfda6ef3a00000000b4def25cfda6ef3a000000005153cb1f00942ff401000000',
                '0x0838696e76656e746f72792d766965770c706e670000'
            ],
            [
                '0xdfc15e6b4ac77b1b389535be70b0f0d7d184bfa8a3eff9ef286767e49095ea4ab4def25cfda6ef3a00000000b4def25cfda6ef3a000000009eb2dcce60f37a2702000000',
                '0x143c32642d7370726974652d61746c617324616e696d6174696f6e0c706e6710736b696e30776561706f6e2d73776f72640000'
            ],
            [
                '0xdfc15e6b4ac77b1b389535be70b0f0d7d184bfa8a3eff9ef286767e49095ea4ab4def25cfda6ef3a00000000b4def25cfda6ef3a00000000b4def25cfda6ef3a00000000',
                '0x083064656661756c742d766965770c706e670000'
            ],
            [
                '0xdfc15e6b4ac77b1b389535be70b0f0d7d184bfa8a3eff9ef286767e49095ea4ab4def25cfda6ef3a00000000b4def25cfda6ef3a00000000bfb27f1eaef06bb903000000',
                '0x082033642d6d6f64656c30776561706f6e2d73776f72640000'
            ]
        ]
        ";
        return JArray.Parse(mock);
    }
    
    public static JArray MockBlueprintInfos()
    {
        string mock = @"
        [
            [
                '0x5bef2c5471aa9e955551dc810f5abb399200647b8c99af7b8b52752114831bdb5153cb1f00942ff401000000',
                '0x648cfe08cacd22da41a17f1cf8d6e5e05288dbd11932912efe4a3dd3eba85562b8516d547435584868586a4c4356734b624b4153774c485945673748426e6f6f47315948313650774675545152586b01640000002c436c6f766572206c65616602000000'
            ],
            [
                '0x5bef2c5471aa9e955551dc810f5abb399200647b8c99af7b8b52752114831bdb9eb2dcce60f37a2702000000',
                '0x648cfe08cacd22da41a17f1cf8d6e5e05288dbd11932912efe4a3dd3eba85562b8516d5a7334744c3641553574754162636843384e7966575376577a4563626f4242685065584d44767172466973630164000000344b65726f73656e65206c616d7000000000'
            ],
            [
                '0x5bef2c5471aa9e955551dc810f5abb399200647b8c99af7b8b52752114831bdbb4def25cfda6ef3a00000000',
                '0x648cfe08cacd22da41a17f1cf8d6e5e05288dbd11932912efe4a3dd3eba85562b8516d65616d6b6269654a3450614479465544774b696e5a73575873484d4a50786e6d52425532447065444e624e6a0164000000404f6c64206375727665642073776f726401000000'
            ]
        ]";
        return JArray.Parse(mock);
    }

    public static JArray MockBlueprintInterpretations()
    {
        string mock = @"
        [
            [
                '0xdfc15e6b4ac77b1b389535be70b0f0d73501b07f805a75a25e88a8945752c99611d2df4e979aa105cf552e9544ebd2b50000000011d2df4e979aa105cf552e9544ebd2b500000000',
                '0x083064656661756c742d766965770c706e6701b8516d524a41784c35335468506767714b31597267447972416337646b37334a5a46506143786634665164615a565001b8516d5665565257427a67317962684a66625563376478764366353434424b635474413876536f43503858647739380000'
            ],
            [
                '0xdfc15e6b4ac77b1b389535be70b0f0d73501b07f805a75a25e88a8945752c99611d2df4e979aa105cf552e9544ebd2b500000000754faa9acf0378f8c3543d9f132d85bc02000000',
                '0x143c32642d7370726974652d61746c617324616e696d6174696f6e0c706e6710736b696e30776561706f6e2d73776f726401b8516d624237436938656e77643173437154536f67584d336768513441715043677a4b4b31524e694851325667503301b8516d595157766d47426f74586e41777977366f6e4465486e457442577739774a315a6858355463556f676f45324d0000'
            ],
            [
                '0xdfc15e6b4ac77b1b389535be70b0f0d73501b07f805a75a25e88a8945752c99611d2df4e979aa105cf552e9544ebd2b500000000d82c12285b5d4551f88e8f6e7eb52b8101000000',
                '0x0838696e76656e746f72792d766965770c706e6701b8516d547076454571556437725644616531436766786169436d65326a37387276376d756873455235726263567a5401b8516d514531333133736832386d64587254764c7a546546456a704d4259675a787774643764595938396e78326d4c0000'
            ],
            [
                '0xdfc15e6b4ac77b1b389535be70b0f0d73501b07f805a75a25e88a8945752c99611d2df4e979aa105cf552e9544ebd2b500000000de18007c0afadc771c45bf719bc7fe5103000000',
                '0x082033642d6d6f64656c30776561706f6e2d73776f726401b8516d63464533363447655732375956556b4242737757557972583379706170435337533962746e486f76466e345a01b8516d524754697747594a65434e35355a393455665a7973763464587662524a34647154466559487476317464596e0000'
            ],
            [
                '0xdfc15e6b4ac77b1b389535be70b0f0d73501b07f805a75a25e88a8945752c996754faa9acf0378f8c3543d9f132d85bc0200000011d2df4e979aa105cf552e9544ebd2b500000000',
                '0x083064656661756c742d766965770c706e6701b8516d58516f76643971763538385443434b466a4e3972416855677a6a59346a5163536e776f4648596f55426b776e01b8516d5665565257427a67317962684a66625563376478764366353434424b635474413876536f43503858647739380000'
            ],
            [
                '0xdfc15e6b4ac77b1b389535be70b0f0d73501b07f805a75a25e88a8945752c996754faa9acf0378f8c3543d9f132d85bc02000000754faa9acf0378f8c3543d9f132d85bc02000000',
                '0x0c2033642d6d6f64656c3467616d652d6d656368616e69633c776f726c642d6578706c6f72696e6701b8516d5534377743726f463664766b73433759393268725050754d6871685364794d4b6d787334396936334a46364c01b8516d524754697747594a65434e35355a393455665a7973763464587662524a34647154466559487476317464596e0000'
            ],
            [
                '0xdfc15e6b4ac77b1b389535be70b0f0d73501b07f805a75a25e88a8945752c996754faa9acf0378f8c3543d9f132d85bc02000000d82c12285b5d4551f88e8f6e7eb52b8101000000',
                '0x103467616d652d6d656368616e696338696e76656e746f72792d766965770c706e673c776f726c642d6578706c6f72696e6701b8516d515a507347634a545a696a4e476b515655593132796832444a73624c4b44736b7262364d6850566544634c6901b8516d59674a4b4853435a6854654d3244396e6a4d524a66643151457343706f62434666504b7772366869486457310000'
            ],
            [
                '0xdfc15e6b4ac77b1b389535be70b0f0d73501b07f805a75a25e88a8945752c996d82c12285b5d4551f88e8f6e7eb52b810100000011d2df4e979aa105cf552e9544ebd2b500000000',
                '0x083064656661756c742d766965770c706e6701b8516d53566157647538514a457871535a7668694c4c577472344266555548764873796d54354d7353624e32754c7801b8516d554776616f427079584839635746416d466335513237715435544766536874487a526341674a48466e6736450000'
            ],
            [
                '0xdfc15e6b4ac77b1b389535be70b0f0d73501b07f805a75a25e88a8945752c996d82c12285b5d4551f88e8f6e7eb52b8101000000754faa9acf0378f8c3543d9f132d85bc02000000',
                '0x0c2033642d6d6f64656c3467616d652d6d656368616e69633c706173736976652d6561726e696e6701b8516d54654665687466313155683471704577674453667a506d31374a7a326f5533523755456e6870534a6845737801b8516d50696d786f3652733166486f69587062564a62464a6435747471595a55694656436b71716a4178734a50314d0000'
            ],
            [
                '0xdfc15e6b4ac77b1b389535be70b0f0d73501b07f805a75a25e88a8945752c996d82c12285b5d4551f88e8f6e7eb52b8101000000d82c12285b5d4551f88e8f6e7eb52b8101000000',
                '0x103467616d652d6d656368616e696338696e76656e746f72792d766965773c706173736976652d6561726e696e670c706e6701b8516d576135517172516b706e4c51787243554b6e42556474525a70644e4268364b3178714e556a354553444c556401b8516d565673657776524846475379465741334552476364703779316d45726971457638587055537a445878514d430000'
            ]
        ]";
        return JArray.Parse(mock);
    }

    public static JArray MockTags()
    {
        string mock = @"
        [
            [
                '0xdfc15e6b4ac77b1b389535be70b0f0d73749eab28926caabb8aeabf2684b2ece0526504ecb8a7cefb1caa606a3165ecf2c666972652d656666656374',
                '0xb8516d6543737545626155423834443156343447384a59766e695369314641666a6b71464b5072765957567a4a546301'
            ],
            [
                '0xdfc15e6b4ac77b1b389535be70b0f0d73749eab28926caabb8aeabf2684b2ece2358165d931721159a620a5b1809deb92033642d6d6f64656c',
                '0xb8516d653252554a6b39314a5075636e627177546343744c6162677a5872336a626f7a613975455a7762653168633301'
            ]
        ]";
        return JArray.Parse(mock);
    }

    [SetUp]
    public void Setup()
    {
        //Copy init from controller
    }

    [Test]
    public void StandAloneTagsDecodingTest()
    {
        // Emulate GetPairs functionality
        JArray tags = MockTags();
        List<AsylumTag> asylumTags = AsylumStandaloneUtils.DecodeTags(tags);

        Assert.IsTrue(asylumTags.Count == 2);
        
        Assert.IsTrue(asylumTags[0].tag == "fire-effect");
        Assert.IsTrue(asylumTags[0].metadata == "QmeCsuEbaUB84D1V44G8JYvniSi1FAfjkqFKPrvYWVzJTc");
        Assert.IsTrue(asylumTags[1].tag == "3d-model");
        Assert.IsTrue(asylumTags[1].metadata == "Qme2RUJk91JPucnbqwTcCtLabgzXr3jboza9uEZwbe1hc3");
    }

    [Test]
    public void StandAloneItemsDecodingTest()
    {
        // Emulate GetPairs functionality
        JArray items = MockItems();
        JArray itemsInterpretations = MockItemInterpretations();
        JArray itemsInterpretationsTags = MockItemInterpretationTags();
        List<AsylumItem> asylumItems = AsylumStandaloneUtils.DecodeItems(items, itemsInterpretations, itemsInterpretationsTags);

        // Expected intepretations sets
        AsylumInterpretation[] cloverLeaf = new AsylumInterpretation[3];
        cloverLeaf[0] = new AsylumInterpretation("0", new string[] {"default-view", "png"}, new InterpretationInfo("QmSVaWdu8QJExqSZvhiLLWtr4BfUUHvHsymT5MsSbN2uLx", "QmUGvaoBpyXH9cWFAmFc5Q27qT5TGfShtHzRcAgJHFng6E"));
        cloverLeaf[1] = new AsylumInterpretation("2", new string[] {"3d-model", "game-mechanic", "passive-earning"}, new InterpretationInfo("QmTeFehtf11Uh4qpEwgDSfzPm17Jz2oU3R7UEnhpSJhEsx", "QmPimxo6Rs1fHoiXpbVJbFJd5ttqYZUiFVCkqqjAxsJP1M"));
        cloverLeaf[2] = new AsylumInterpretation("1", new string[]  {"game-mechanic", "inventory-view", "passive-earning", "png"}, new InterpretationInfo("QmWa5QqrQkpnLQxrCUKnBUdtRZpdNBh6K1xqNUj5ESDLUd", "QmVVsewvRHFGSyFWA3ERGcdp7y1mEriqEv8XpUSzDXxQMC"));
        
        AsylumInterpretation[] sword = new AsylumInterpretation[4];
        sword[0] = new AsylumInterpretation("0", new string[] {"default-view", "png"}, new InterpretationInfo("QmRJAxL53ThPggqK1YrgDyrAc7dk73JZFPaCxf4fQdaZVP", "QmVeVRWBzg1ybhJfbUc7dxvCf544BKcTtA8vSoCP8Xdw98"));
        sword[1] = new AsylumInterpretation("2", new string[] {"2d-sprite-atlas", "animation", "png", "skin", "weapon-sword"}, new InterpretationInfo("QmbB7Ci8enwd1sCqTSogXM3ghQ4AqPCgzKK1RNiHQ2VgP3", "QmYQWvmGBotXnAwyw6onDeHnEtBWw9wJ1ZhX5TcUogoE2M"));
        sword[2] = new AsylumInterpretation("1", new string[]  {"inventory-view","png"}, new InterpretationInfo("QmTpvEEqUd7rVDae1CgfxaiCme2j78rv7muhsER5rbcVzT", "QmQE1313sh28mdXrTvLzTeFEjpMBYgZxwtd7dYY89nx2mL"));
        sword[3] = new AsylumInterpretation("3", new string[]  {"3d-model", "weapon-sword"}, new InterpretationInfo("QmcFE364GeW27YVUkBBswWUyrX3ypapCS7S9btnHovFn4Z", "QmRGTiwGYJeCN55Z94UfZysv4dXvbRJ4dqTFeYHtv1tdYn"));

        Assert.IsTrue(asylumItems.Count == 3);

        Assert.IsTrue(asylumItems[0].blueprintId == "1");
        Assert.IsTrue(asylumItems[0].id == "1");
        Assert.IsTrue(asylumItems[0].metadata == "test_item_2_0");
        Assert.IsTrue(asylumItems[0].owner.AccountId == "5ELYXe9CdJupDtjymmk5aK1i1XkisWi1X9V6UHHjVSudHmCn");
        Assert.IsTrue(Enumerable.SequenceEqual(asylumItems[0].interpretations, cloverLeaf));

        Assert.IsTrue(asylumItems[1].blueprintId == "1");
        Assert.IsTrue(asylumItems[1].id == "0");
        Assert.IsTrue(asylumItems[1].metadata == "test_item_1_0");
        Assert.IsTrue(asylumItems[1].owner.AccountId == "5ELYXe9CdJupDtjymmk5aK1i1XkisWi1X9V6UHHjVSudHmCn");
        Assert.IsTrue(Enumerable.SequenceEqual(asylumItems[1].interpretations, cloverLeaf));

        Assert.IsTrue(asylumItems[2].blueprintId == "0");
        Assert.IsTrue(asylumItems[2].id =="0");
        Assert.IsTrue(asylumItems[2].metadata == "test_item_0_0");
        Assert.IsTrue(asylumItems[2].owner.AccountId == "5ELYXe9CdJupDtjymmk5aK1i1XkisWi1X9V6UHHjVSudHmCn");
        Assert.IsTrue(Enumerable.SequenceEqual(asylumItems[2].interpretations, sword));
    }

    [Test]
    public void StandAloneBlueprintsDecodingTest()
    {
        // Emulate GetPairs functionality
        JArray blueprints = MockBlueprintInfos();
        JArray blueprintsInterpretations = MockBlueprintInterpretations();
        List<AsylumBlueprint> asylumBlueprints = AsylumStandaloneUtils.DecodeBlueprints(blueprints, blueprintsInterpretations);

        // Expected intepretations sets
        // Sword
        AsylumInterpretation[] sword = new AsylumInterpretation[4];
        sword[0] = new AsylumInterpretation("0", new string[] {"default-view", "png"}, new InterpretationInfo("QmRJAxL53ThPggqK1YrgDyrAc7dk73JZFPaCxf4fQdaZVP", "QmVeVRWBzg1ybhJfbUc7dxvCf544BKcTtA8vSoCP8Xdw98"));
        sword[1] = new AsylumInterpretation("2", new string[] {"2d-sprite-atlas", "animation", "png", "skin", "weapon-sword"}, new InterpretationInfo("QmbB7Ci8enwd1sCqTSogXM3ghQ4AqPCgzKK1RNiHQ2VgP3", "QmYQWvmGBotXnAwyw6onDeHnEtBWw9wJ1ZhX5TcUogoE2M"));
        sword[2] = new AsylumInterpretation("1", new string[]  {"inventory-view","png"}, new InterpretationInfo("QmTpvEEqUd7rVDae1CgfxaiCme2j78rv7muhsER5rbcVzT", "QmQE1313sh28mdXrTvLzTeFEjpMBYgZxwtd7dYY89nx2mL"));
        sword[3] = new AsylumInterpretation("3", new string[]  {"3d-model", "weapon-sword"}, new InterpretationInfo("QmcFE364GeW27YVUkBBswWUyrX3ypapCS7S9btnHovFn4Z", "QmRGTiwGYJeCN55Z94UfZysv4dXvbRJ4dqTFeYHtv1tdYn"));
        // Clover leaf
        AsylumInterpretation[] cloverLeaf = new AsylumInterpretation[3];
        cloverLeaf[0] = new AsylumInterpretation("0", new string[] {"default-view", "png"}, new InterpretationInfo("QmSVaWdu8QJExqSZvhiLLWtr4BfUUHvHsymT5MsSbN2uLx", "QmUGvaoBpyXH9cWFAmFc5Q27qT5TGfShtHzRcAgJHFng6E"));
        cloverLeaf[1] = new AsylumInterpretation("2", new string[] {"3d-model", "game-mechanic", "passive-earning"}, new InterpretationInfo("QmTeFehtf11Uh4qpEwgDSfzPm17Jz2oU3R7UEnhpSJhEsx", "QmPimxo6Rs1fHoiXpbVJbFJd5ttqYZUiFVCkqqjAxsJP1M"));
        cloverLeaf[2] = new AsylumInterpretation("1", new string[]  {"game-mechanic", "inventory-view", "passive-earning", "png"}, new InterpretationInfo("QmWa5QqrQkpnLQxrCUKnBUdtRZpdNBh6K1xqNUj5ESDLUd", "QmVVsewvRHFGSyFWA3ERGcdp7y1mEriqEv8XpUSzDXxQMC"));
        // Lamp
        AsylumInterpretation[] lamp = new AsylumInterpretation[3];
        lamp[0] = new AsylumInterpretation("0", new string[] {"default-view", "png"}, new InterpretationInfo("QmXQovd9qv588TCCKFjN9rAhUgzjY4jQcSnwoFHYoUBkwn", "QmVeVRWBzg1ybhJfbUc7dxvCf544BKcTtA8vSoCP8Xdw98"));
        lamp[1] = new AsylumInterpretation("2", new string[] {"3d-model", "game-mechanic", "world-exploring"}, new InterpretationInfo("QmU47wCroF6dvksC7Y92hrPPuMhqhSdyMKmxs49i63JF6L", "QmRGTiwGYJeCN55Z94UfZysv4dXvbRJ4dqTFeYHtv1tdYn"));
        lamp[2] = new AsylumInterpretation("1", new string[]  {"game-mechanic", "inventory-view", "png", "world-exploring"}, new InterpretationInfo("QmQZPsGcJTZijNGkQVUY12yh2DJsbLKDskrb6MhPVeDcLi", "QmYgJKHSCZhTeM2D9njMRJfd1QEsCpobCFfPKwr6hiHdW1"));
        
        Assert.IsTrue(asylumBlueprints.Count == 3);

        Assert.IsTrue(asylumBlueprints[0].id == "1");
        Assert.IsTrue(asylumBlueprints[0].issuer == "5ELYXe9CdJupDtjymmk5aK1i1XkisWi1X9V6UHHjVSudHmCn");
        Assert.IsTrue(asylumBlueprints[0].max == "100");
        Assert.IsTrue(asylumBlueprints[0].metadata == "QmTt5XHhXjLCVsKbKASwLHYEg7HBnooG1YH16PwFuTQRXk");
        Assert.IsTrue(asylumBlueprints[0].name == "Clover leaf");
        Assert.IsTrue(asylumBlueprints[0].nftCount == 2);
        Assert.IsTrue(Enumerable.SequenceEqual(asylumBlueprints[0].interpretations, cloverLeaf));

        Assert.IsTrue(asylumBlueprints[1].id == "2");
        Assert.IsTrue(asylumBlueprints[1].issuer == "5ELYXe9CdJupDtjymmk5aK1i1XkisWi1X9V6UHHjVSudHmCn");
        Assert.IsTrue(asylumBlueprints[1].max == "100");
        Assert.IsTrue(asylumBlueprints[1].metadata == "QmZs4tL6AU5tuAbchC8NyfWSvWzEcboBBhPeXMDvqrFisc");
        Assert.IsTrue(asylumBlueprints[1].name == "Kerosene lamp");
        Assert.IsTrue(asylumBlueprints[1].nftCount == 0);
        Assert.IsTrue(Enumerable.SequenceEqual(asylumBlueprints[1].interpretations, lamp));

        Assert.IsTrue(asylumBlueprints[2].id == "0");
        Assert.IsTrue(asylumBlueprints[2].issuer == "5ELYXe9CdJupDtjymmk5aK1i1XkisWi1X9V6UHHjVSudHmCn");
        Assert.IsTrue(asylumBlueprints[2].max == "100");
        Assert.IsTrue(asylumBlueprints[2].metadata == "QmeamkbieJ4PaDyFUDwKinZsWXsHMJPxnmRBU2DpeDNbNj");
        Assert.IsTrue(asylumBlueprints[2].name == "Old curved sword");
        Assert.IsTrue(asylumBlueprints[2].nftCount == 1);
        Assert.IsTrue(Enumerable.SequenceEqual(asylumBlueprints[2].interpretations, sword));
    }
}
